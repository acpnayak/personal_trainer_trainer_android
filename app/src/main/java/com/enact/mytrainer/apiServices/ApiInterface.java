package com.enact.mytrainer.apiServices;

import com.enact.mytrainer.model.AvailabilityResponse;
import com.enact.mytrainer.model.ClientDeatilsResponse;
import com.enact.mytrainer.model.ClientDetailResponse;
import com.enact.mytrainer.model.ClientResponse;
import com.enact.mytrainer.model.CommonResponse;
import com.enact.mytrainer.model.GymsResponse;
import com.enact.mytrainer.model.LoginResponse;
import com.enact.mytrainer.model.PunchCardHistoryResponse;
import com.enact.mytrainer.model.PunchCardResponse;
import com.enact.mytrainer.model.SessionResponse;
import com.enact.mytrainer.model.StripeConnectCheck.StripeConnectCheck;
import com.enact.mytrainer.model.StripeResponse;
import com.enact.mytrainer.model.StripeSecondStepResponse;
import com.enact.mytrainer.model.TrainingResponse;
import com.enact.mytrainer.model.chat.Message;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("trainer/login")
    Call<LoginResponse> loginUser(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("social_id") String social_id,
                                  @Field("type") String type);

    @FormUrlEncoded
    @POST("trainer/register")
    Call<LoginResponse> registerUser(@Field("first_name") String first_name,
                                     @Field("last_name") String last_name,
                                     @Field("email") String email,
                                     @Field("password") String password,
                                     @Field("social_id") String social_id,
                                     @Field("type") String type);

    @FormUrlEncoded
    @POST("trainer/reset_password")
    Call<CommonResponse> resetPass(@Field("email") String email);

    @POST("trainings")
    Call<TrainingResponse> fetchTrainings();

    @POST("trainer/my_trainings")
    Call<CommonResponse> saveTrainings(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("trainer/change_password")
    Call<CommonResponse> changePass(@Field("trainer_id") String trainer_id,
                                    @Field("old_password") String old_password,
                                    @Field("new_password") String new_password);

    @FormUrlEncoded
    @POST("trainer/push_settings")
    Call<CommonResponse> on_off_push(@Field("trainer_id") String trainer_id,
                                     @Field("status") String status);

    @FormUrlEncoded
    @POST("trainer/gyms")
    Call<GymsResponse> fetchGyms(@Field("trainer_id") String trainer_id);

    @FormUrlEncoded
    @POST("trainer/delete_gym")
    Call<CommonResponse> deleteGym(@Field("trainer_id") String trainer_id,
                                   @Field("gym_id") String gym_id);

    @FormUrlEncoded
    @POST("trainer/availability_delete")
    Call<CommonResponse> deleteAvailability(@Field("trainer_id") String trainer_id,
                                   @Field("availability_id") String availability_id);

    @FormUrlEncoded
    @POST("trainer/clients")
    Call<ClientResponse> fetchClients(@Field("trainer_id") String trainer_id);


    @FormUrlEncoded
    @POST("trainer/client_details")
    Call<ClientDeatilsResponse> fetchClientDetail(@Field("trainer_id") String trainer_id,
                                                 @Field("client_id") String client_id);



    @Multipart
    @POST("trainer/add_gym")
    Call<CommonResponse> addGym(@PartMap HashMap<String, RequestBody> hashMap);

    @Multipart
    @POST("trainer/update_profile")
    Call<LoginResponse> updateProfile(@PartMap HashMap<String, RequestBody> hashMap);


    @FormUrlEncoded
    @POST("trainer/punch_cards")
    Call<PunchCardResponse> fetchPunchCards(@Field("trainer_id") String trainer_id);

    @FormUrlEncoded
    @POST("trainer/punch_card_purchase_history")
    Call<PunchCardHistoryResponse> fetchPunchCardHistory(@Field("trainer_id") String trainer_id,
                                                         @Field("punch_card_id") String punch_card_id);


    @FormUrlEncoded
    @POST("trainer/add_punch_card")
    Call<CommonResponse> addPunchCard(@Field("trainer_id") String trainer_id,
                                      @Field("sessions") String sessions,
                                      @Field("price") String price,
                                      @Field("name") String name);

    @FormUrlEncoded
    @POST("trainer/delete_punch_card")
    Call<CommonResponse> deletePunchCard(@Field("trainer_id") String trainer_id,
                                         @Field("punch_card_id") String punch_card_id);

    @FormUrlEncoded
    @POST("trainer/appointments")
    Call<SessionResponse> fetchAppoitments(@Field("trainer_id") String trainer_id,
                                           @Field("type") String type,
                                           @Field("keyword") String keyword);

    @FormUrlEncoded
    @POST("trainer/payment_history")
    Call<SessionResponse> fetchPaymentHistory(@Field("trainer_id") String trainer_id);

    @FormUrlEncoded
    @POST("trainer/dashboard")
    Call<SessionResponse> fetchDashboardSessions(@Field("trainer_id") String trainer_id,
                                           @Field("year") String year,
                                           @Field("month") String month,
                                           @Field("day") String day);

    @FormUrlEncoded
    @POST("trainer/availabilities_trainer")
    Call<AvailabilityResponse> fetchAvailabilities(@Field("trainer_id") String trainer_id,
                                                      @Field("year") String year,
                                                      @Field("month") String month,
                                                      @Field("day") String day);

    @POST("trainer/set_availability")
    Call<CommonResponse> setAvailability(@Body JsonObject jsonObject);

    @POST("trainer/update_availability")
    Call<CommonResponse> updateAvailability(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @Headers("Authorization:Bearer sk_test_1tqRe2GJZqKbCChbIZvB4GKs")
    @POST("accounts")
    Call<StripeResponse> stripeFirstStep(@Field("type") String type);


    @FormUrlEncoded
   // @Headers("Authorization:Bearer sk_test_1tqRe2GJZqKbCChbIZvB4GKs")
    @Headers("Authorization:Bearer sk_test_1tqRe2GJZqKbCChbIZvB4GKs")

    @POST("account_links")
    Call<StripeSecondStepResponse> stripeSecondStep(@Field("account") String account,
                                                    @Field("refresh_url") String refresh_url,
                                                    @Field("return_url") String return_url,
                                                    @Field("type") String type);


    @Headers("Authorization:Bearer sk_test_1tqRe2GJZqKbCChbIZvB4GKs")
    @GET("accounts/{account_id}")
    Call<StripeConnectCheck> checkifDetailsSubmited(@Path("account_id") String account_id);

    @POST("/fcm/send")
    Call<Message> sendMessage(@Header("Authorization") String token, @Body Message message);


    @FormUrlEncoded
    @POST("trainer/stripe_connect")
    Call<CommonResponse>stripe_connect(@Field("trainer_id") String user_id,
                                                 @Field("stripe_connect_id") String client_id);


    @FormUrlEncoded
    @POST("trainer/report_user")
    Call<CommonResponse> report_client(@Field("trainer_id") String trainer_id,
                                       @Field("type") String type,
                                       @Field("user_id") String user_id);

}
