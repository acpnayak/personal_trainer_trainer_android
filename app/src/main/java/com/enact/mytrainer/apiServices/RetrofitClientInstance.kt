package com.enact.mytrainer.apiServices;



import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClientInstance {

    private val BASE_URL = "http://54.183.206.219:8088/api/"
    private var api: ApiInterface? = null
    private val STRIPE_BASE_URL = "https://api.stripe.com/v1/"
    private var api2: ApiInterface? = null

    fun getAPI(): ApiInterface? {
        if (api == null) {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(500, TimeUnit.SECONDS)
                .readTimeout(500, TimeUnit.SECONDS)
                .addInterceptor(logging).build()



            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
            api = retrofit.create(ApiInterface::class.java)

        }
        return api
    }


    fun getStripeAPI(): ApiInterface? {
        if (api2 == null) {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY




            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(500, TimeUnit.SECONDS)
                .readTimeout(500, TimeUnit.SECONDS)
                .addInterceptor(logging).build()



            val retrofit = Retrofit.Builder()
                .baseUrl(STRIPE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
            api2 = retrofit.create(ApiInterface::class.java)

        }
        return api2
    }



    // This method  converts String to RequestBody
    fun toRequestBody(value: String): RequestBody {
        return RequestBody.create("text/plain".toMediaTypeOrNull(), value)
    }

}