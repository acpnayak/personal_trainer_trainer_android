package com.enact.mytrainer

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationItemView

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_gyms, R.id.navigation_inbox,R.id.navigation_clients,R.id.navigation_account
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }


    @SuppressLint("RestrictedApi")
    fun setImageDotIcon(yes: Boolean)
    {

        if(yes)
        {

            val menuItem: BottomNavigationItemView = findViewById<View>(R.id.navigation_inbox) as BottomNavigationItemView
            menuItem.setIcon(resources.getDrawable(R.drawable.chat_active))


        }
        else{
            val menuItem: BottomNavigationItemView = findViewById<View>(R.id.navigation_inbox) as BottomNavigationItemView
            menuItem.setIcon(resources.getDrawable(R.drawable.chat))

        }
    }
}