package com.enact.mytrainer.model

import java.io.Serializable

class ClientResponse(var success:Boolean,var message:String,var data:ArrayList<ClientObj>):Serializable

data class ClientObj(var user_id:String,var first_name:String,var last_name:String,var image:String,var status:String,var email:String,var social:String,var type:String,var goal:String,var address:String,var avg_rating:String,var trainings:ArrayList<TrainingObj>,



                     var next_session: SessionInfo,
                     var last_session: SessionInfo,


):Serializable