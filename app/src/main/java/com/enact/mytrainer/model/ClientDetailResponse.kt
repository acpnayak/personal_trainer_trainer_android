package com.enact.mytrainer.model

import java.io.Serializable

data class ClientDetailResponse (var success:Boolean,var message:String,var data:ClientDetailObj):Serializable

data class ClientDetailObj(var user_id:String,var first_name:String,var last_name:String,var image:String,var status:String,var email:String,var social:String,var type:String,var goal:String,var trainings:ArrayList<TrainingObj>,var address:String,var avg_rating:String,var next_session:NextSessionObj,var last_session:NextSessionObj):Serializable

data class NextSessionObj(var _id:String,var user_id:String,var price:Int,var trainer_id:String,var slot_id:String,var date:String,var time:String,var status:Int,var schedule_time:Long,var transaction_id:String,var payment_method:String,var day:String,var month:String,var year:String,var punch_card_id:String,var gym_name:String):Serializable