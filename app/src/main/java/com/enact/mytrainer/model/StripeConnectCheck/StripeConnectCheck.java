
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StripeConnectCheck implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("object")
    @Expose
    private String object;
    @SerializedName("business_profile")
    @Expose
    private BusinessProfile businessProfile;
    @SerializedName("capabilities")
    @Expose
    private Capabilities capabilities;
    @SerializedName("charges_enabled")
    @Expose
    private Boolean chargesEnabled;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("default_currency")
    @Expose
    private String defaultCurrency;
    @SerializedName("details_submitted")
    @Expose
    private Boolean detailsSubmitted;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("payouts_enabled")
    @Expose
    private Boolean payoutsEnabled;
    @SerializedName("requirements")
    @Expose
    private Requirements requirements;
    @SerializedName("settings")
    @Expose
    private Settings settings;
    @SerializedName("type")
    @Expose
    private String type;
    private final static long serialVersionUID = 2166387202376621247L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public BusinessProfile getBusinessProfile() {
        return businessProfile;
    }

    public void setBusinessProfile(BusinessProfile businessProfile) {
        this.businessProfile = businessProfile;
    }

    public Capabilities getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(Capabilities capabilities) {
        this.capabilities = capabilities;
    }

    public Boolean getChargesEnabled() {
        return chargesEnabled;
    }

    public void setChargesEnabled(Boolean chargesEnabled) {
        this.chargesEnabled = chargesEnabled;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Boolean getDetailsSubmitted() {
        return detailsSubmitted;
    }

    public void setDetailsSubmitted(Boolean detailsSubmitted) {
        this.detailsSubmitted = detailsSubmitted;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Boolean getPayoutsEnabled() {
        return payoutsEnabled;
    }

    public void setPayoutsEnabled(Boolean payoutsEnabled) {
        this.payoutsEnabled = payoutsEnabled;
    }

    public Requirements getRequirements() {
        return requirements;
    }

    public void setRequirements(Requirements requirements) {
        this.requirements = requirements;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
