package com.enact.mytrainer.model.chat

import com.google.firebase.Timestamp
import java.io.Serializable

 data class ChatData (var roomId: String="",
                     var name: String = "",
                     var imageUrl: String = "",
                     var message: String = "",
                     var timestamp:Timestamp = Timestamp.now(),
                     var userLastDeletedMsgId: String = "",
                     var gymLastDeletedMsgId: String = "",
                     var uid: String = "",
                     var gid: String = "",
                     var sid: String = "",
                     var unreadCount: String = "",
                     var unreadMessage :Boolean= true):Serializable