package com.enact.mytrainer.model

import java.io.Serializable

data class CommonResponse (var success:Boolean,var message:String):Serializable