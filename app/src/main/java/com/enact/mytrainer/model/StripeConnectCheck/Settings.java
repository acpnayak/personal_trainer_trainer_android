
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Settings implements Serializable
{

    @SerializedName("branding")
    @Expose
    private Branding branding;
    @SerializedName("card_payments")
    @Expose
    private CardPayments cardPayments;
    @SerializedName("dashboard")
    @Expose
    private Dashboard dashboard;
    @SerializedName("payments")
    @Expose
    private Payments payments;
    private final static long serialVersionUID = -7700108995640829596L;

    public Branding getBranding() {
        return branding;
    }

    public void setBranding(Branding branding) {
        this.branding = branding;
    }

    public CardPayments getCardPayments() {
        return cardPayments;
    }

    public void setCardPayments(CardPayments cardPayments) {
        this.cardPayments = cardPayments;
    }

    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    public Payments getPayments() {
        return payments;
    }

    public void setPayments(Payments payments) {
        this.payments = payments;
    }

}
