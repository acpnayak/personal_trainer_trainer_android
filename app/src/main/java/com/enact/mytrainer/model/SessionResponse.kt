package com.enact.mytrainer.model

import java.io.Serializable

data  class SessionResponse(var success:Boolean,var message:String,var data:ArrayList<SessionData>):Serializable

data class SessionData(val first_name:String="", var last_name:String="",  var _id:String="",var client_id:String="",var review:String="",var rating:String="",var status:String="",var payment_type:String="",var price:String="",var trainer_id:String="",var name:String="",var image:String="",var schedule_time:String="",var time:TimeObj,var type:Int=-1,var header:String=""):Serializable

data class TimeObj(var from:String,var date:String,var day:String,var month:String,var year:String,var utc:String,var slot_id:String,var gym_name:String):Serializable