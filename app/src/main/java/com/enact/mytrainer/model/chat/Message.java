package com.enact.mytrainer.model.chat;

import java.util.HashMap;

public class Message {

    private String to;
    private String collapseKey;
    private HashMap<String, Object> notification;
    private HashMap<String, Object> data;

    public Message(String to, String collapseKey, HashMap<String, Object> notification, HashMap<String, Object> data) {
        this.to = to;
        this.collapseKey = collapseKey;
        this.notification = notification;
        this.data = data;
    }
}
