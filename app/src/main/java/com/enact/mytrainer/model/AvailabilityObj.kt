package com.enact.mytrainer.model

import java.io.Serializable

data class AvailabilityObj (var from:String,var to:String,var sessions:String):Serializable