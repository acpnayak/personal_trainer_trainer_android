package com.enact.mytrainer.model

import java.io.Serializable

data class AccountResponse(var success: Boolean, var message: String, var data: AccountData) :
    Serializable

data class AccountData(
    var balance: String,
    var last_payment: Int,
    var history: ArrayList<AccountHistoryData>
) : Serializable

data class AccountHistoryData(
    var id: String,
    var first_name: String,
    var last_name: String,
    var notes: String,
    var amount: String,
    var date: Int
) : Serializable


data class ClientDeatilsResponse(
    var success: Boolean,
    var message: String,
    var data: ClientObj
) : Serializable




//data class Payment_Info(var bank_name:String, var card_holder_name:String, var customer_id:String, var cvv:String
//                        , var mm:String, var name_on_account:String
//                        , var number:String, var yy:String
//                        , var zip:String,var   primary : Int
//                        , var user_id:String,var   type : String
//                        , var id:String, var account_type:String,
//                        var account_number:String,
//                        var routing_number:String
//
//
//
//):Serializable


data class TrainingInfo(
    var _id: String, var name: String


) : Serializable


data class SessionInfo(

    var _id: String,
    var user_id: String,
    var price: String,
    var trainer_id: String,
    var slot_id: String,
    var date: String,
    var time: String,
    var status: String,
    var schedule_time: String,
    var transaction_id: String,
    var payment_method: String,
    var day: String,
    var month: String,
    var year: String,
    var punch_card_id: String,
    var gym_name: String,


    ) : Serializable
