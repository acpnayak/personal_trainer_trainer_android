package com.enact.mytrainer.model

import java.io.Serializable

data class PunchCardHistoryResponse (var success:Boolean,var message:String,var data:ArrayList<PunchCardHistoryObj>):Serializable

data class PunchCardHistoryObj(var _id:String,var punch_id:String,var trainer_id:String,var client_id:String,var sessions_available:String,var total_sessions:String,var price:String,var transaction_id:String,var client_name:String,var updated_at:Long,var purchased_on:String):Serializable