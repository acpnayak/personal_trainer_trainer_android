
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Payments implements Serializable
{

    @SerializedName("statement_descriptor")
    @Expose
    private Object statementDescriptor;
    @SerializedName("statement_descriptor_kana")
    @Expose
    private Object statementDescriptorKana;
    @SerializedName("statement_descriptor_kanji")
    @Expose
    private Object statementDescriptorKanji;
    private final static long serialVersionUID = -6273528419636507135L;

    public Object getStatementDescriptor() {
        return statementDescriptor;
    }

    public void setStatementDescriptor(Object statementDescriptor) {
        this.statementDescriptor = statementDescriptor;
    }

    public Object getStatementDescriptorKana() {
        return statementDescriptorKana;
    }

    public void setStatementDescriptorKana(Object statementDescriptorKana) {
        this.statementDescriptorKana = statementDescriptorKana;
    }

    public Object getStatementDescriptorKanji() {
        return statementDescriptorKanji;
    }

    public void setStatementDescriptorKanji(Object statementDescriptorKanji) {
        this.statementDescriptorKanji = statementDescriptorKanji;
    }

}
