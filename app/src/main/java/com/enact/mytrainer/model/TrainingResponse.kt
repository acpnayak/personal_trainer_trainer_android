package com.enact.mytrainer.model

import java.io.Serializable

data class TrainingResponse (var success:Boolean,var message:String,var data:ArrayList<TrainingObj>):Serializable