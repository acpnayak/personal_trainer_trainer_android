package com.enact.mytrainer.model.chat

import com.google.firebase.Timestamp

class ChatMessage {

    // MARK:- Properties
    var type: String=""
    var roomId: String=""
    var id: String =""
    var messageId: Long = Long.MIN_VALUE
    var from: String =""
    var to: String =""
    var message: String =""
    var messageType: String =""// MessageType = MessageType.Text
    var timestamp = Timestamp.now()
    var senderName: String =""
    var sessionId:String=""
    var deleted:Boolean = false

    var sessionStartTime=  Timestamp.now()
    var deleteMsgTimestamp=  Timestamp.now()

    /// Only for local use: UIImage instance for Image message
    var localImage: String =""
}