package com.enact.mytrainer.model


import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class ClientimportRes (val success: Boolean,val message: String,val data:ArrayList<clientData> ): Serializable

data class clientData(var first_name:String="", var last_name:String="",
                      var phone:Any, var address:String="",
                      var image:String="", var user_id:String="",
                      var id:String="",
                      var push_token:ArrayList<String>,
                      var isSignup: Any ,
                      var isBlocked: Any,
                      var isPayment: Any ,
                      var current_trainer: String = ""


):Serializable









