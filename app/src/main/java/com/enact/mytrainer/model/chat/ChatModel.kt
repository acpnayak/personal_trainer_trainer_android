package com.enact.mytrainer.model.chat

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi
import com.google.firebase.Timestamp

class ChatModel() : Parcelable
{

    var roomId: String=""
    var name: String = ""
    var imageUrl: String = ""
    var client_name:String=""
    var client_imageUrl:String=""
    var message: String = ""
    var timestamp = Timestamp.now()
    var userLastDeletedMsgId: String = ""
    var gymLastDeletedMsgId: String = ""
    var uid: String = ""
    var client_id: String = ""
    var trainer_id: String = ""
    var unreadCount: String = ""
    var unreadMessage = true

    @RequiresApi(Build.VERSION_CODES.Q)
    constructor(parcel: Parcel) : this() {
        roomId = parcel.readString()!!
        name = parcel.readString()!!
        imageUrl = parcel.readString()!!
        client_name= parcel.readString()!!
        client_imageUrl= parcel.readString()!!
        message = parcel.readString()!!
        timestamp = parcel.readParcelable(Timestamp::class.java.classLoader)!!
        userLastDeletedMsgId = parcel.readString()!!
        gymLastDeletedMsgId = parcel.readString()!!
        uid = parcel.readString()!!
        client_id = parcel.readString()!!
        trainer_id = parcel.readString()!!
        unreadMessage = parcel.readBoolean()!!
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(roomId)
        parcel.writeString(name)
        parcel.writeString(imageUrl)
        parcel.writeString(client_name)
        parcel.writeString(client_imageUrl)

        parcel.writeString(message)
        parcel.writeParcelable(timestamp, flags)
        parcel.writeString(userLastDeletedMsgId)
        parcel.writeString(gymLastDeletedMsgId)
        parcel.writeString(uid)
        parcel.writeString(client_id)
        parcel.writeString(trainer_id)
        parcel.writeBoolean(unreadMessage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChatModel> {
        @RequiresApi(Build.VERSION_CODES.Q)
        override fun createFromParcel(parcel: Parcel): ChatModel {
            return ChatModel(parcel)
        }

        override fun newArray(size: Int): Array<ChatModel?> {
            return arrayOfNulls(size)
        }
    }
}