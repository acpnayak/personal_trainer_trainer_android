package com.enact.mytrainer.model

import java.io.Serializable

data class LoginResponse (var success:Boolean,var message:String,var data:LoginUserData):Serializable

data class LoginUserData(var id:String,var first_name:String,var last_name:String,var image:String,var status:String,var email:String,var social_id:String,var bio:String,var price:String,var notifications:String,var latitude:String,var longitude:String,var address:String,var stripe_connect_id:String,var stripe_connect_email:String,var trainings:ArrayList<TrainingObj>):Serializable

data class TrainingObj(var _id:String="",var name:String,var isSelected:Boolean):Serializable