package com.enact.mytrainer.model

import java.io.Serializable

data class PunchCardResponse (var success:Boolean,var message:String,var data:ArrayList<PunchCardObj>):Serializable

data class PunchCardObj(var _id:String,var status:String,var trainer_id:String,var sessions:String,var price:String):Serializable