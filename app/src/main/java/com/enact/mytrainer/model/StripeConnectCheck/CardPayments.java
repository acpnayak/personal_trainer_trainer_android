
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardPayments implements Serializable
{

    @SerializedName("statement_descriptor_prefix")
    @Expose
    private Object statementDescriptorPrefix;
    private final static long serialVersionUID = -7151721233024231393L;

    public Object getStatementDescriptorPrefix() {
        return statementDescriptorPrefix;
    }

    public void setStatementDescriptorPrefix(Object statementDescriptorPrefix) {
        this.statementDescriptorPrefix = statementDescriptorPrefix;
    }

}
