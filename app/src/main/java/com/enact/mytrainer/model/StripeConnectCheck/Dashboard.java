
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dashboard implements Serializable
{

    @SerializedName("display_name")
    @Expose
    private Object displayName;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    private final static long serialVersionUID = 8928413665130245009L;

    public Object getDisplayName() {
        return displayName;
    }

    public void setDisplayName(Object displayName) {
        this.displayName = displayName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

}
