package com.enact.mytrainer.model;

import java.io.Serializable;

public class StripeResponse implements Serializable {

    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}