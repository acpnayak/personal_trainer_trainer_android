package com.enact.mytrainer.model

import java.io.Serializable

data class AvailabilityResponse (var success:Boolean,var message:String,var data:ArrayList<AvailabilityData>):Serializable

data class AvailabilityData(var _id:String="",var month:String="",var to_utc:Long=-6346346,var date_utc:Long=-5765757,var to:String="",var day:String="",var year:String="",var from:String="",var length:String="",var from_utc:Long=-767687,var trainer_id:String="",var created_at:Long=-8677687,var updated_at:Long=-6767567,var isConver:Boolean=true):Serializable