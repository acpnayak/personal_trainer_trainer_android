package com.enact.mytrainer.model

import java.io.Serializable

data class HistoryObj (var date:String,var type:Int,var header:String):Serializable