
package com.enact.mytrainer.model.StripeConnectCheck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessProfile implements Serializable
{

    @SerializedName("mcc")
    @Expose
    private Object mcc;
    @SerializedName("name")
    @Expose
    private Object name;
    @SerializedName("support_address")
    @Expose
    private Object supportAddress;
    @SerializedName("support_email")
    @Expose
    private Object supportEmail;
    @SerializedName("support_phone")
    @Expose
    private Object supportPhone;
    @SerializedName("support_url")
    @Expose
    private Object supportUrl;
    @SerializedName("url")
    @Expose
    private Object url;
    private final static long serialVersionUID = 1780616845175676879L;

    public Object getMcc() {
        return mcc;
    }

    public void setMcc(Object mcc) {
        this.mcc = mcc;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getSupportAddress() {
        return supportAddress;
    }

    public void setSupportAddress(Object supportAddress) {
        this.supportAddress = supportAddress;
    }

    public Object getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(Object supportEmail) {
        this.supportEmail = supportEmail;
    }

    public Object getSupportPhone() {
        return supportPhone;
    }

    public void setSupportPhone(Object supportPhone) {
        this.supportPhone = supportPhone;
    }

    public Object getSupportUrl() {
        return supportUrl;
    }

    public void setSupportUrl(Object supportUrl) {
        this.supportUrl = supportUrl;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

}
