package com.enact.mytrainer.model

import java.io.Serializable

data class GymsResponse (var success:Boolean,var message:String,var data:ArrayList<GymObject>):Serializable

data class GymObject(var trainer_id:String,var trainer_main_id:String,var name:String,var address:String,var latitude:Double,var longitude:Double,var image:String,var google_place_id:String,var id:String):Serializable