package com.enact.mytrainer.ui.changePass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityAddGymBinding
import com.enact.mytrainer.databinding.ActivityChangePassBinding
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginResponse
import com.enact.mytrainer.model.LoginUserData
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePassActivity : BaseActivity() {
    private lateinit var binding: ActivityChangePassBinding
    lateinit var loginUserData: LoginUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePassBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData=Paper.book().read(Constants.USERDATA)
        setListeners()
    }

    fun setListeners(){
        binding.changePass.setOnClickListener(){
            if(binding.editOld.text.toString().trim().isBlank())
            {
                binding.editOld.error = getString(R.string.please_enter_old_password)
                binding.editOld.requestFocus()
            }

           else if(binding.editNew.text.toString().trim().isBlank())
            {
                binding.editNew.error = getString(R.string.please_enter_new_password)
                binding.editNew.requestFocus()
            }
            else if(binding.editCnfrm.text.toString().trim().isBlank())
            {
                binding.editCnfrm.error = getString(R.string.please_enter_confirm_password)
                binding.editCnfrm.requestFocus()
            }

            else if(!binding.editNew.text.toString().equals(binding.editCnfrm.text.toString()))
            {
                binding.editCnfrm.error = getString(R.string.please_not_match)
                binding.editCnfrm.requestFocus()
            }
            else{
                if (isNetworkAvailable()){
                    changePassCall()
                }
                else{
                    singleButtonDialog(
                        getString(R.string.netError),
                        "Ok", {

                        }
                    )


                }
            }
        }

        binding.backbtn.setOnClickListener(){
            finish()
        }
    }



    fun changePassCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.changePass(
                loginUserData.id,
                binding.editOld.text.toString(),
                binding.editNew.text.toString()
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok", {
                                finish()
                                })
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


}