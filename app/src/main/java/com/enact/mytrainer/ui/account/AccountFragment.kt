package com.enact.mytrainer.ui.account

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.loadUrl
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.AccountFragmentBinding
import com.enact.mytrainer.databinding.FragmentGymsBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.addAvailability.AddAvailabilityActivity
import com.enact.mytrainer.ui.changePass.ChangePassActivity
import com.enact.mytrainer.ui.editProfile.EditProfileActivity
import com.enact.mytrainer.ui.gyms.GymViewModel
import com.enact.mytrainer.ui.login.LoginActivity
import com.enact.mytrainer.ui.paymentHIstory.PaymentHistoryActivity
import com.enact.mytrainer.ui.punchCard.PunchCardsActivity
import com.enact.mytrainer.ui.stripeConnect.StripeConnectActivity
import com.enact.mytrainer.ui.terms.TermsActivity
import com.enact.mytrainer.ui.viewAvailability.ViewAvailabilityActivity
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountFragment : Fragment() {
    var push_status=""
    private var _binding: AccountFragmentBinding? = null
    private lateinit var viewModel: AccountViewModel
    private val binding get() = _binding!!
    lateinit var loginUserData: LoginUserData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this).get(AccountViewModel::class.java)

        _binding = AccountFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        loginUserData = Paper.book().read(Constants.USERDATA)
        setLiteners()
        setData()
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun setLiteners(){

        binding.connect.setOnClickListener(){
            if ((context as MainActivity).isNetworkAvailable()) {
                stripeFirstStep()
            } else {
                (context as MainActivity).msgAlert(
                    getString(R.string.netError),
                    requireContext()
                )
            }
        }

        binding.labelViewProfile.setOnClickListener() {
            val intent = Intent(context, EditProfileActivity::class.java)
            startActivity(intent)
        }

        binding.tvChangePass.setOnClickListener(){
            val intent = Intent(context, ChangePassActivity::class.java)
            startActivity(intent)
        }

        binding.tvPunch.setOnClickListener(){
            val intent = Intent(context, PunchCardsActivity::class.java)
            startActivity(intent)
        }

        binding.tvAvailability.setOnClickListener(){
            val intent = Intent(context, AddAvailabilityActivity::class.java)
            startActivity(intent)
        }

        binding.tvGetHelp.setOnClickListener(){
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "developer.enact@gmail.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)))
        }

        binding.tvTerms.setOnClickListener(){
            val intent = Intent(context, TermsActivity::class.java)
            startActivity(intent)
        }

        binding.tvHistory.setOnClickListener(){
            val intent = Intent(context, PaymentHistoryActivity::class.java)
            startActivity(intent)
        }

        binding.toggle.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(
                buttonView: CompoundButton?,
                isChecked: Boolean
            ) {
                if (isChecked) {
                    push_status="1"
                    updatePushCall(push_status)

                } else {
                    push_status="0"
                    updatePushCall(push_status)
                }
            }
        })


        binding.tvLogout.setOnClickListener(){
            (context as MainActivity).doubleButtonDialog("Are you sure you want to logout", "Yes", "No", {
                Paper.book().delete(Constants.USERDATA)
                Paper.book().delete(Constants.ISLOGIN)
                val intent =
                    Intent(context, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                requireActivity().finish()
            }, {

            })

        }

    }

    fun setData(){
        binding.tvUsername.text=loginUserData.first_name+" "+loginUserData.last_name
        binding.tvEmail.text=loginUserData.email
        binding.profile.loadUrl(Constants.IMAGE_BASE_URL+loginUserData.image)

        if (!loginUserData.notifications.isNullOrEmpty()){
            binding.toggle.isChecked = loginUserData.notifications.equals("1")
        }
        else{
            binding.toggle.isChecked=false
        }
    }

    override fun onResume() {
        super.onResume()
        loginUserData = Paper.book().read(Constants.USERDATA)
        setData()
    }

    fun updatePushCall(status:String) {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.on_off_push(
               loginUserData.id,
                status
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    (context as MainActivity). hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity). hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                val loginUser: LoginUserData
                                loginUser = loginUserData
                                loginUser.notifications = status
                                Paper.book().write(Constants.USERDATA, loginUser)

                            } else {
                                (context as MainActivity).singleButtonDialog(userpojo.message, "Ok") {

                                }
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok") {

                            }
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    }
                }
            })
    }


    fun stripeFirstStep() {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getStripeAPI()
            ?.stripeFirstStep("standard")
            ?.enqueue(object : Callback<StripeResponse> {
                override fun onFailure(call: Call<StripeResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<StripeResponse>?,
                    response: Response<StripeResponse>?
                ) {
                    (context as MainActivity).hideProgress()
                    val responsePojo = response!!.body()

                    if (responsePojo != null) {
                        if (response!!.isSuccessful) {
                            stripeSecondStep(responsePojo.id)
                        }
                    } else {
                        (context as MainActivity).msgAlert(
                            getString(R.string.pleasetryagain),
                            context!!
                        )

                    }


                }
            })
    }


    fun stripeSecondStep(code: String) {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getStripeAPI()
            ?.stripeSecondStep(
                code,
                "https://enact.com/reauth",
                "https://enact.com/return",
                "account_onboarding"
            )
            ?.enqueue(object : Callback<StripeSecondStepResponse> {
                override fun onFailure(call: Call<StripeSecondStepResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<StripeSecondStepResponse>?,
                    response: Response<StripeSecondStepResponse>?
                ) {
                    (context as MainActivity).hideProgress()
                    val responsePojo = response!!.body()

                    if (responsePojo != null) {
                        if (response!!.isSuccessful) {
                            val intent =
                                Intent(context, StripeConnectActivity::class.java)
                            intent.putExtra("url", responsePojo.url)
                            intent.putExtra("connect_id", code)
                            startActivity(intent)
                        }

                    } else {
                        (context as MainActivity).msgAlert(
                            getString(R.string.pleasetryagain),
                            context!!
                        )

                    }


                }
            })
    }





}