package com.enact.mytrainer.ui.gyms

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.FragmentGymsBinding
import com.enact.mytrainer.model.GymObject
import com.enact.mytrainer.model.GymsResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.model.TrainingResponse
import com.enact.mytrainer.ui.addGym.AddGymActivity
import com.enact.mytrainer.ui.clients.adapter.ClientsAdapter
import com.enact.mytrainer.ui.editProfile.EditProfileActivity
import com.enact.mytrainer.ui.gyms.adapter.GymsAdapter
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GymFragment : Fragment() {

    private lateinit var dashboardViewModel: GymViewModel
    private var _binding: FragmentGymsBinding? = null
    lateinit var list: ArrayList<String>
    lateinit var loginUserData: LoginUserData

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(GymViewModel::class.java)

        _binding = FragmentGymsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        loginUserData = Paper.book().read(Constants.USERDATA)

        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
        })

        binding.add.setOnClickListener() {
            val intent = Intent(context, AddGymActivity::class.java)
            startActivity(intent)
        }

        binding.plus.setOnClickListener() {
            val intent = Intent(context, AddGymActivity::class.java)
            startActivity(intent)
        }


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun setAdapter(list: ArrayList<GymObject>) {
        val layoutManager = LinearLayoutManager(context)
        binding.gymList.layoutManager = layoutManager
        val adapter = GymsAdapter(list, requireContext())
        binding.gymList!!.adapter = adapter
    }


    fun fetchAllGyms() {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchGyms(loginUserData.id)
            ?.enqueue(object : Callback<GymsResponse> {
                override fun onFailure(call: Call<GymsResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<GymsResponse>?,
                    response: Response<GymsResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity).hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                if (userpojo.data.size > 0) {
                                    binding.nodata.visibility = View.GONE
                                    setAdapter(response.body().data)

                                } else {
                                    binding.nodata.visibility = View.VISIBLE

                                }


                            } else {
                                (context as MainActivity).singleButtonDialog(
                                    userpojo.message,
                                    "Ok",
                                    {

                                    })
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        fetchAllGyms()
    }

}