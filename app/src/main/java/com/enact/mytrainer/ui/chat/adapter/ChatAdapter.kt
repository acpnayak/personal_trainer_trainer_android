package com.enact.mytrainer.ui.chat.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.ChatPojo
import com.enact.mytrainer.model.HistoryObj
import java.util.*

class ChatAdapter(
    var results: ArrayList<ChatPojo>,
    private val context: Context

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        if (viewType == 1) {
            return RightViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.left_chat_bubble,
                    parent,
                    false
                )
            )

        } else {
            return LeftViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.right_chat_bubble,
                    parent,
                    false
                )
            )

        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (results[position].type == 1) {
            1
        } else {
            2
        }


    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {

        if (holder is ChatAdapter.RightViewHolder) {
            holder.msg.text = results[position].msg

        } else {
            (holder as ChatAdapter.LeftViewHolder).msg.text = results[position].msg

        }
    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }


    inner class LeftViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var msg: TextView


        init {
            msg = itemView.findViewById(R.id.msg)


        }
    }

    inner class RightViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var msg: TextView

        init {
            msg = itemView.findViewById(R.id.msg)

        }
    }


}