package com.enact.mytrainer.ui.login

import android.content.Intent
import android.os.Bundle
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityLoginBinding
import com.enact.mytrainer.model.LoginResponse
import com.enact.mytrainer.ui.forgotpassword.ForgotPasswordActivity
import com.enact.mytrainer.ui.signup.SignupActivity
import com.enact.mytrainer.ui.trainingoffer.TrainingOfferActivity
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.loginBtn.setOnClickListener {
            if (binding.editEmailtext.text.toString().trim().isBlank()) {
                binding.editEmailtext.error = getString(R.string.please_enter_email)
                binding.editEmailtext.requestFocus()
            } else if (!isValidEmail(binding.editEmailtext.text.toString().trim())) {
                binding.editEmailtext.error = getString(R.string.please_enter_valid_email)
                binding.editEmailtext.requestFocus()
            } else if (binding.editPassword.text.toString().trim().isBlank()) {
                binding.editPassword.error = getString(R.string.please_enter_password)
                binding.editPassword.requestFocus()
            } else {
                if (isNetworkAvailable()){
                    loginCall()

                }
                else{
                    singleButtonDialog(
                        getString(R.string.netError),
                        "Ok", {

                        }
                    )

                }
            }
        }



        binding.forgotpassword.setOnClickListener {
            goToNextScreen(ForgotPasswordActivity::class.java)

        }
        binding.forgotpassword.setOnClickListener {
            goToNextScreen(ForgotPasswordActivity::class.java)

        }
        binding.signUpClick.setOnClickListener {
            goToNextScreen(SignupActivity::class.java)

        }
    }


    fun loginCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.loginUser(
                binding.editEmailtext.text.toString().trim(),
                binding.editPassword.text.toString(),
                "",
                "0"
            )
            ?.enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<LoginResponse>?,
                    response: Response<LoginResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                Paper.book().write(Constants.USERDATA,response.body().data)
                                Paper.book().write(Constants.ISLOGIN,true)
                                finishAll(MainActivity::class.java)
                            } else {
                                singleButtonDialog(userpojo.message, "Ok") {

                                }
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok") {

                            }
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    }
                }
            })
    }
}