package com.enact.mytrainer.ui.punchCard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityChangePassBinding
import com.enact.mytrainer.databinding.ActivityPunchCardsBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.addPunchCard.AddPunchActivity
import com.enact.mytrainer.ui.punchCard.adapter.PunchCardsAdapter
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PunchCardsActivity : BaseActivity() {
    private lateinit var binding: ActivityPunchCardsBinding
    lateinit var list: ArrayList<String>
    lateinit var loginUserData: LoginUserData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPunchCardsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        setListeners()

    }

    fun setAdapter(list: ArrayList<PunchCardObj>) {
        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
        val adapter = PunchCardsAdapter(list, this)
        binding.rv!!.adapter = adapter
    }


    fun setListeners() {
        binding.backbtn.setOnClickListener() {
            finish()
        }
        binding.add.setOnClickListener() {
            val intent = Intent(this, AddPunchActivity::class.java)
            startActivity(intent)
        }

    }


    fun fetchAllPunchCards() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchPunchCards(loginUserData.id)
            ?.enqueue(object : Callback<PunchCardResponse> {
                override fun onFailure(call: Call<PunchCardResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<PunchCardResponse>?,
                    response: Response<PunchCardResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                setAdapter(response.body().data)

                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        fetchAllPunchCards()
    }


}