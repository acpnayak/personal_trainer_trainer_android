package com.enact.mytrainer.ui.trainingoffer.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.GymOffer
import com.enact.mytrainer.model.TrainingObj
import com.google.android.material.button.MaterialButton

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HashTagAdapter(
    var results: ArrayList<TrainingObj>,
    private val context: Context,
    private val isShowCross:Boolean

) : RecyclerView.Adapter<HashTagAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.hashitem_with_cross,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        holder.txt.text = results[position].name
        if(results[position].isSelected){
            holder.txt.setTextColor(context.getColor(R.color.white))
            holder.txt.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.red));
        }else
        {
            holder.txt.setTextColor(context.getColor(R.color.black))
            holder.txt.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.white));

        }

        holder.txt.setOnClickListener {
            results[position].isSelected= !results[position].isSelected
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }

    fun addData(listItems: TrainingObj) {
        this.results.add(listItems)
        notifyDataSetChanged()

    }

    fun remove(position: Int){
        results.removeAt(position)
        notifyDataSetChanged()
    }

    fun getList():ArrayList<TrainingObj>{
        return results;
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txt: MaterialButton

        init {
            txt = itemView.findViewById(R.id.txt)
        }
    }

    fun select(layout:LinearLayout){
      //  layout.setBackgroundDrawable(context.resources.getDrawable(R.drawable.empty_green_bg))
    }
    fun unSelect(layout:LinearLayout){
        layout.setBackgroundDrawable(context.resources.getDrawable(R.drawable.white_bg))
    }
}