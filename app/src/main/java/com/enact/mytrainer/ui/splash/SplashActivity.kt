package com.enact.mytrainer.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.ui.login.LoginActivity
import io.paperdb.Paper

class SplashActivity : BaseActivity() {
    var islogin = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        islogin = Paper.book().read(Constants.ISLOGIN, false)

        Handler().postDelayed({
            if (islogin) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }

        }, 500)


    }
}