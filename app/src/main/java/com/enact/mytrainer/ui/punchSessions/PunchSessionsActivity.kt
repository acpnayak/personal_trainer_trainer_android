package com.enact.mytrainer.ui.punchSessions

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityPunchCardsBinding
import com.enact.mytrainer.databinding.ActivityPunchSessionsBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.addPunchCard.AddPunchActivity
import com.enact.mytrainer.ui.punchCard.adapter.PunchCardsAdapter
import com.enact.mytrainer.ui.punchSessions.adapter.PunchSessionsAdapter
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PunchSessionsActivity : BaseActivity() {
    private lateinit var binding: ActivityPunchSessionsBinding
    lateinit var list: ArrayList<String>
    lateinit var loginUserData: LoginUserData
    lateinit var punchCardObj: PunchCardObj
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPunchSessionsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        punchCardObj= intent.getSerializableExtra("obj") as PunchCardObj
        binding.label.text=punchCardObj.sessions+" x Sessions"
        setListeners()
        fetchAllPunchCardHIstory()
    }


    fun setAdapter(list: ArrayList<PunchCardHistoryObj>) {
        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
        val adapter = PunchSessionsAdapter(list, this)
        binding.rv!!.adapter = adapter
    }


    fun setListeners() {
        binding.backbtn.setOnClickListener() {
            finish()
        }

    }

    fun fetchAllPunchCardHIstory() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?. fetchPunchCardHistory(loginUserData.id,punchCardObj._id)
            ?.enqueue(object : Callback<PunchCardHistoryResponse> {
                override fun onFailure(call: Call<PunchCardHistoryResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<PunchCardHistoryResponse>?,
                    response: Response<PunchCardHistoryResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                setAdapter(response.body().data)

                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }




}