package com.enact.mytrainer.ui.clients.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.loadUrl
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.ClientObj
import com.enact.mytrainer.model.GymOffer
import com.enact.mytrainer.ui.chat.ChatActivity
import com.enact.mytrainer.ui.clientDetail.ClientDetailActivity
import com.enact.mytrainer.ui.trainingoffer.TrainingOfferActivity
import com.google.android.material.button.MaterialButton

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ClientsAdapter(
    var results: ArrayList<ClientObj>,
    private val context: Context

) : RecyclerView.Adapter<ClientsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.client_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        holder.tv_username.text=results[position].first_name+" "+results[position].last_name
        holder.goal.text=results[position].goal
        holder.location.text=results[position].address
        holder.img.loadUrl(Constants.IMAGE_BASE_URL+results[position].image)

        if (!results[position].avg_rating.isNullOrBlank()){
            holder.ratingBar.rating=results[position].avg_rating.toFloat()
        }
        else{
            holder.ratingBar.rating= 0.0F

        }

        holder.itemView.setOnClickListener(){
            val intent= Intent(context,ClientDetailActivity::class.java)
            intent.putExtra("clientID",results[position].user_id)
            context.startActivity(intent)
        }


        holder.chat_img.setOnClickListener(){

            val intent = Intent(context, ChatActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(Constants.selectedClient, results[position])
            intent.putExtras(bundle)


            (context as Activity).startActivityForResult(intent, 201)
            context.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)



        }





    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }



    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tv_username: TextView
        var goal: TextView
        var location: TextView
        var img: ImageView
        var ratingBar: RatingBar
        var chat_img: AppCompatImageView

        init {
            tv_username = itemView.findViewById(R.id.tv_username)
            goal = itemView.findViewById(R.id.desc)
            location = itemView.findViewById(R.id.tvLocation)
            img = itemView.findViewById(R.id.profile)
            ratingBar = itemView.findViewById(R.id.ratingBar1)
            chat_img= itemView.findViewById(R.id.chat_img)
        }
    }

}