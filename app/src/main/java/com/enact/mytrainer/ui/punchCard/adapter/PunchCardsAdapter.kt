package com.enact.mytrainer.ui.punchCard.adapter

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.punchCard.PunchCardsActivity
import com.enact.mytrainer.ui.punchSessions.PunchSessionsActivity
import com.google.android.material.button.MaterialButton
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PunchCardsAdapter(
    var results: ArrayList<PunchCardObj>,
    private val context: Context

) : RecyclerView.Adapter<PunchCardsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.punch_card_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        var loginUserData: LoginUserData
        loginUserData= Paper.book().read(Constants.USERDATA)
        holder.number.text=results[position].sessions
        holder.price.text="$"+results[position].price

        holder.mainLayout.setOnClickListener(){
            val intent=Intent(context,PunchSessionsActivity::class.java)
            intent.putExtra("obj",results[position])
            context.startActivity(intent)
        }

        holder.deleteLay.setOnClickListener(){
            deletePunchCard(loginUserData.id,results[position]._id,position)
        }



    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }

    fun deleteItem(pos:Int){
        results.removeAt(pos)
        notifyDataSetChanged()
    }




    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var number: TextView
        var price: TextView
        var deleteLay: LinearLayout
        var mainLayout: ConstraintLayout

        init {
            number = itemView.findViewById(R.id.tv_number)
            price = itemView.findViewById(R.id.tvPrice)
            deleteLay = itemView.findViewById(R.id.deletelay)
            mainLayout = itemView.findViewById(R.id.mainLayout)

        }
    }



    fun deletePunchCard(trainerId:String,punchCardID:String,pod:Int) {
        (context as PunchCardsActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.deletePunchCard(
                trainerId,
                punchCardID
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    (context as PunchCardsActivity). hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as PunchCardsActivity). hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                deleteItem(pod)

                            } else {
                                (context as PunchCardsActivity).singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            (context as PunchCardsActivity).singleButtonDialog(
                                context.getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            (context as PunchCardsActivity).singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            (context as PunchCardsActivity).singleButtonDialog(
                                context.getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }



}