package com.enact.mytrainer.ui.stripeConnect

import android.app.ProgressDialog
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.model.StripeConnectCheck.StripeConnectCheck
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StripeConnectActivity : BaseActivity() {
    var webView: WebView? = null
    var url = ""
    var connect_id = ""
    lateinit var loginUserPojo: LoginUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stripe_connect)
        webView = findViewById(R.id.webview)
        loginUserPojo = Paper.book().read(Constants.USERDATA)
        val intent = intent
        url = intent.getStringExtra("url").toString()
        connect_id = intent.getStringExtra("connect_id").toString()
        webView!!.loadUrl(url)
        webView!!.settings.javaScriptEnabled = true



        webView!!.setWebChromeClient(object : WebChromeClient() {
            var dialog: ProgressDialog? = null
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                if (dialog == null) {
                    dialog = ProgressDialog.show(this@StripeConnectActivity, null, "loading...")
                }
                if (newProgress >= 100 && dialog != null) {
                    dialog!!.dismiss()
                    dialog = null
                }
                super.onProgressChanged(view, newProgress)
            }
        })
        webView!!.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean {
                Log.e("StripeURLoverride", url)
                if (url.equals("https://enact.com/return", ignoreCase = true)) {
                    checkSubmitDetails(connect_id)

                }
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.e("StripeURLfinished", url)
                if (url.equals("https://enact.com/return", ignoreCase = true)) {
                  //  checkSubmitDetails(connect_id)

                }
            }

            override fun onLoadResource(view: WebView, url: String) {
                Log.e("StripeURLonLoad", url)
                super.onLoadResource(view, url)
                if (url.equals("https://enact.com/return", ignoreCase = true)) {
                   // checkSubmitDetails(connect_id)

                }
            }

            override fun onPageStarted(
                view: WebView,
                url: String,
                favicon: Bitmap?
            ) {
                Log.e("StripeURLstarted", url)
                if (url.equals("https://enact.com/return", ignoreCase = true)) {
                   // checkSubmitDetails(connect_id)

                }
            }
        })
    }

    fun checkSubmitDetails(account_id: String) {
        showProgressDialog()
        RetrofitClientInstance.getStripeAPI()
            ?.checkifDetailsSubmited(account_id)
            ?.enqueue(object : Callback<StripeConnectCheck> {
                override fun onFailure(call: Call<StripeConnectCheck>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<StripeConnectCheck>?,
                    response: Response<StripeConnectCheck>?
                ) {
                    hideProgress()
                    if (response != null) {
                        if (response!!.isSuccessful) {
                            val isSubmitted = response.body().detailsSubmitted
                            if (isSubmitted) {
                                saveConnectToServer(connect_id,response.body().email.toString())
                            } else {

                            }
                        }

                    } else {
                        msgAlert(
                            getString(R.string.pleasetryagain),
                            this@StripeConnectActivity
                        )

                    }


                }
            })
    }



    fun saveConnectToServer(id: String,email:String) {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.stripe_connect(
                loginUserPojo.id ,
                id
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {

                    if (response!!.isSuccessful) {

                        val userpojo = response?.body()
                        hideProgress()
                        if (response != null && response.body()?.success!!) {
                            if (userpojo != null) {

                                if (userpojo.success) {
                                    singleButtonDialog(
                                        userpojo.message,
                                        "Ok"
                                    ) {
                                        finish()

                                    }


                                } else {
                                    singleButtonDialog(
                                        userpojo.message,
                                        "Ok"
                                    ) {

                                    }
                                }

                            } else {
                                msgAlert(
                                    getString(R.string.pleasetryagain),
                                    this@StripeConnectActivity
                                )
                            }
                        } else {
                            if (userpojo != null) {
                                msgAlert(
                                    userpojo.message,
                                    this@StripeConnectActivity
                                )
                            } else {
                                msgAlert(
                                    getString(R.string.pleasetryagain),
                                    this@StripeConnectActivity
                                )
                            }
                        }
                    }
                    else{
                        msgAlert(
                            getString(R.string.pleasetryagain),
                            this@StripeConnectActivity
                        )
                    }
                }
            })
    }




}