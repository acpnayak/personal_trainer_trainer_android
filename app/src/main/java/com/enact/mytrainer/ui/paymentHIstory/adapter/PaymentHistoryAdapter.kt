package com.enact.mytrainer.ui.paymentHIstory.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.HistoryObj
import com.enact.mytrainer.model.SessionData
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class PaymentHistoryAdapter(
    var results: ArrayList<SessionData>,
    private val context: Context

    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        if (viewType == 1) {
            return HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.month_header,
                    parent,
                    false
                )
            )

        } else {
            return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.payment_history_adapter,
                    parent,
                    false
                )
            )

        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (results[position].type == 1) {
            1
        } else {
            2
        }


    }


    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {

        if (holder is PaymentHistoryAdapter.ViewHolder) {
            holder.day.text = results[position].time.day
            holder.time.text = convert(results[position].time.from)
            holder.name.text = "Training with " + results[position].name
            if (results[position].payment_type.equals("0")) {
                holder.payment_type.text = "Per Session"

            } else if (results[position].payment_type.equals("1")) {
                holder.payment_type.text = "Punch Card"

            } else {
                holder.payment_type.text = "Monthly"

            }
            holder.price.text = "$" + results[position].price+"/hr"

        }
        else{
            (holder as PaymentHistoryAdapter.HeaderViewHolder).header.text =getMonthName(results[position].header.toInt())

        }
    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }


    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var day: TextView
        var time: TextView
        var name: TextView
        var location: TextView
        var payment_type: TextView
        var price: TextView


        init {
            day = itemView.findViewById(R.id.datetext)
            time = itemView.findViewById(R.id.time)
            name = itemView.findViewById(R.id.gym_name)
            location = itemView.findViewById(R.id.tvLocation)
            payment_type = itemView.findViewById(R.id.punchcard)
            price = itemView.findViewById(R.id.price)



        }
    }

    inner class HeaderViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var header: TextView

        init {
            header = itemView.findViewById(R.id.header)

        }
    }


    fun getMonthName(monthNumber: Int): String {
        val cal = Calendar.getInstance()
        val month_date = SimpleDateFormat("MMMM")
        cal[Calendar.MONTH] = monthNumber - 1
        val month_name: String = month_date.format(cal.time)
        return month_name
    }


    @Throws(Exception::class)
    fun convert(time: String?):String {
        try {
            val t24 = SimpleDateFormat("HH:mm")
            val t12 = SimpleDateFormat("hh:mm a")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }


}