package com.enact.mytrainer.ui.clientDetail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.loadUrl
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityChangePassBinding
import com.enact.mytrainer.databinding.ActivityClientDetailBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.chat.ChatActivity
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class ClientDetailActivity : BaseActivity() {
    private lateinit var binding: ActivityClientDetailBinding
    lateinit var loginUserData: LoginUserData
    var clientId = ""
    var selectedClient:ClientObj?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityClientDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        clientId = intent.getStringExtra("clientID").toString()
        setListeners()
        clientDetailCall()
    }

    fun setListeners() {
        binding.backbtn.setOnClickListener() {
            finish()
        }
        binding.sendmsg.setOnClickListener {
            val intent = Intent(context, ChatActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(Constants.selectedClient, selectedClient)
            intent.putExtras(bundle)
            (context as Activity).startActivityForResult(intent, 201)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
    }

    fun clientDetailCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchClientDetail(
                loginUserData.id, clientId
            )
            ?.enqueue(object : Callback<ClientDeatilsResponse> {
                override fun onFailure(call: Call<ClientDeatilsResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<ClientDeatilsResponse>?,
                    response: Response<ClientDeatilsResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {
                            if (userpojo.success) {
                                setData(response.body().data)
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok") {

                            }
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    }
                }
            })
    }


    fun setData(data: ClientObj) {
        selectedClient=data
        binding.profile.loadUrl(Constants.IMAGE_BASE_URL + data.image)
        binding.tvName.text = data.first_name + " " + data.last_name

        if (data.goal.equals("")) {
            binding.goal.text = "N/A"

        } else {
            binding.goal.text = data.goal

        }
        if (data.address.equals("")) {
            binding.tvLocation.text = "N/A"

        } else {
            binding.tvLocation.text = data.address

        }

        try {
            if (data.last_session.date.equals("") && data.last_session.time.equals("")) {
                binding.pDate.text = "N/A"

            } else {
                binding.pDate.text = data.last_session.date
                binding.pTime.text = data.last_session.time
            }
        } catch (e:Exception) {
            binding.pDate.text = "N/A"

        }

        try {
            if (data.next_session.date.equals("") && data.next_session.time.equals("")) {
                binding.nDate.text = "N/A"

            } else {
                binding.nDate.text = data.next_session.date
                binding.nTime.text = data.next_session.date
            }
        } catch (e:Exception) {
            binding.nDate.text = "N/A"

        }


    }


}