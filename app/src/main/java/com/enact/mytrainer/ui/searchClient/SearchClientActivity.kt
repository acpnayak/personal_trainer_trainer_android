package com.enact.mytrainer.ui.searchClient

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.R
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityPunchCardsBinding
import com.enact.mytrainer.databinding.ActivitySearchClientBinding
import com.enact.mytrainer.ui.clients.adapter.ClientsAdapter

class SearchClientActivity : BaseActivity() {
    private lateinit var binding: ActivitySearchClientBinding
    lateinit var list: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchClientBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        list = ArrayList()
        list.add("")
        list.add("")
        list.add("")
        setListeners()

    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.stay, R.anim.slide_down)
    }

    fun setListeners() {
        binding.cancel.setOnClickListener() {
            onBackPressed()
        }
    }

}