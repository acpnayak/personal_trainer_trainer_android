package com.enact.mytrainer.ui.addGym

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityAddGymBinding
import com.enact.mytrainer.databinding.ActivityEditProfileBinding
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginResponse
import com.enact.mytrainer.model.LoginUserData
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.paperdb.Paper
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.File

class AddGymActivity : BaseActivity() {
    var file: File? = null
    private lateinit var binding: ActivityAddGymBinding
    private val AUTOCOMPLETE_REQUEST_CODE = 1001
    var lat = ""
    var lng = ""
    lateinit var loginUserData: LoginUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddGymBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        setListeners()
    }

    fun setListeners() {

        binding.back.setOnClickListener {
            finish()
        }
        binding.editAddress.setOnClickListener() {
            searchPlace()
        }

        binding.submitBtn.setOnClickListener() {
            if (binding.editName.text.isNullOrBlank()) {
                msgAlert("Please enter Gym name", this)
            } else if (binding.editAddress.text.isNullOrBlank()) {
                msgAlert("Please enter Gym address", this)

            } else if (file == null) {
                msgAlert("Please select gym image", this)

            } else {
                addGymCall()
            }
        }

        binding.gymImg.setOnClickListener() {
            ImagePicker.with(this)
                .cropSquare()          //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        binding.editAddress.setText(place.name.toString() + ", " + place.address.toString())
                        lat = place.latLng!!.latitude.toString()
                        lng = place.latLng!!.longitude.toString()


                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("ghfg", status.statusMessage.toString())
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The demoprofile canceled the operation.
                }
            }
            return
        } else {
            if (resultCode == Activity.RESULT_OK) {

                file = ImagePicker.getFile(data)
                if (file != null) {
                    binding.gymImg.setImageURI(Uri.fromFile(file))
                }
                //You can also get File Path from intent
                val filePath: String? = ImagePicker.getFilePath(data)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }


    fun searchPlace() {

        if (!Places.isInitialized()) {
            Places.initialize(this, Constants.googlekey)
        }

        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)

    }


    fun addGymCall() {
        showProgressDialog()

        val trainerId_R = RetrofitClientInstance.toRequestBody(loginUserData.id)
        val name_R = RetrofitClientInstance.toRequestBody(binding.editName.text.toString())
        val address_R = RetrofitClientInstance.toRequestBody(binding.editAddress.text.toString())
        val lat_R = RetrofitClientInstance.toRequestBody(lat)
        val lng_R = RetrofitClientInstance.toRequestBody(lng)

        val data = HashMap<String, RequestBody>()
        data["trainer_id"] = trainerId_R
        data["name"] = name_R
        data["address"] = address_R
        data["lat"] = lat_R
        data["long"] = lng_R
        if (file !== null) {
            //To put your image file you have to do
            val fileBody = RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
            data["image\"; filename=\"${file!!.name}"] = fileBody

        }

        RetrofitClientInstance.getAPI()
            ?.addGym(data)
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok", {
                                    finish()
                                })
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


}