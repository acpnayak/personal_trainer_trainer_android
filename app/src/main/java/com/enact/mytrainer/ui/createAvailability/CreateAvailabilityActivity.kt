package com.enact.mytrainer.ui.createAvailability

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import com.enact.mytrainer.R
import com.enact.mytrainer.base.BaseActivity
import android.view.View.OnFocusChangeListener
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.ActivityCreateAvailabilityBinding
import com.enact.mytrainer.model.AvailabilityData
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.ui.addAvailability.adapter.AvailabilityAdapter
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.paperdb.Paper
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateAvailabilityActivity : BaseActivity() {
    private lateinit var binding: ActivityCreateAvailabilityBinding
    var adapter: AvailabilityAdapter? = null
    lateinit var loginUserData: LoginUserData
    var selectedDate=""
    lateinit var list: ArrayList<AvailabilityData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateAvailabilityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.valDate.text = intent.getStringExtra("date").toString()
        selectedDate=intent.getStringExtra("date").toString()
        list = ArrayList()
        setAdapter(list)
        loginUserData = Paper.book().read(Constants.USERDATA)
        setListeners()
        binding.add.setOnClickListener() {
            addDialig()
        }
    }


    fun addDialig() {
        var focus = "from"
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.create_availability_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val window: Window = dialog.getWindow()!!
        window.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog.setTitle("Title...");
        dialog.setCancelable(true)
        val done = dialog.findViewById(R.id.done) as TextView
        val from = dialog.findViewById(R.id.edtFrom) as TextView
        val to = dialog.findViewById(R.id.edtTo) as TextView
        val session = dialog.findViewById(R.id.edtSessionLength) as EditText
        val timepicker = dialog.findViewById(R.id.timePicker) as TimePicker

        from.setOnClickListener() {
            focus = "from"
            from.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            from.setTextColor(resources.getColor(R.color.red))

            to.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            to.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        to.setOnClickListener() {
            focus = "to"
            to.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            to.setTextColor(resources.getColor(R.color.red))

            from.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            from.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        timepicker.setOnTimeChangedListener { _, hour, minute ->
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }
            val hr = if (hour < 10) "0" + hour else hour
            val min = if (minute < 10) "0" + minute else minute
            // display format of time
            val msg = "Time is: $hr : $min $am_pm"

            if (focus.equals("from")) {
                from.text = "$hr:$min $am_pm"

            } else {
                to.text = "$hr:$min $am_pm"

            }

        }


        done.setOnClickListener {
            if (from.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter from time", "Ok")

            } else if (to.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to time", "Ok")

            } else if (session.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to session length", "Ok")

            } else {
                val availabilityObj = AvailabilityData(
                    from = from.text.toString(),
                    to = to.text.toString(),
                    length = session.text.toString(),
                    isConver = false
                )
                adapter!!.addItem(availabilityObj)
                dialog.dismiss()
            }

        }
        dialog.show()
    }

    fun setAdapter(list: ArrayList<AvailabilityData>) {
        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
        adapter = AvailabilityAdapter(list, this, object : AvailabilityAdapter.ItemListener {
            override fun onItemClickListener(
                selectedItem: AvailabilityData,type:Int,pos:Int

            ) {
                if (type==1){
                    adapter!!.removeItem(pos)
                }
                else{
                    updateDialog(selectedItem,pos)
                }

            }
        })
        binding.rv!!.adapter = adapter
    }


    fun setListeners() {
        binding.cancel.setOnClickListener() {
            finish()
        }

        binding.done.setOnClickListener() {
           if (adapter!!.getList().size>0){
               saveAvailability()
           }
        }
    }


    fun saveAvailability() {

        val jsonObject = JsonObject()
        jsonObject.addProperty("trainer_id", loginUserData.id)
        val array = JsonArray()
        for (i in 0..adapter!!.getList().size - 1) {
            val obj = JsonObject()
            obj.addProperty("from", convert(adapter!!.getList()[i].from))
            obj.addProperty("to", convert(adapter!!.getList()[i].to))
            obj.addProperty("day", getDay(selectedDate))
            obj.addProperty("month", getMonth(selectedDate))
            obj.addProperty("year", getYear(selectedDate))
            obj.addProperty("to_utc", milliseconds(getDateInUTC(stringToDate(selectedDate+" "+adapter!!.getList()[i].to))))
            obj.addProperty("from_utc", milliseconds(getDateInUTC(stringToDate(selectedDate+" "+adapter!!.getList()[i].from))))
            obj.addProperty("date_utc", "0")
            obj.addProperty("length", adapter!!.getList()[i].length)
            array.add(obj)
        }
        jsonObject.addProperty("availability", array.toString())

        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.setAvailability(jsonObject)

            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                finish()
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }

    @Throws(Exception::class)
    fun convert(time: String?): String {
        try {
            val t24 = SimpleDateFormat("hh:mm a")
            val t12 = SimpleDateFormat("HH:mm")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    @Throws(Exception::class)
    fun convertTo12(time: String?): String {
        try {
            val t24 = SimpleDateFormat("HH:mm")
            val t12 = SimpleDateFormat("hh:mm a")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }



    fun getDay(date: String): String {
        val separated = date.split("-").toTypedArray()
        return separated[2]

    }

     fun getMonth(date: String): String {
        val separated = date.split("-").toTypedArray()
        return separated[1]

    }


     fun getYear(date: String): String {
        val separated = date.split("-").toTypedArray()
        return separated[0]

    }


    fun getDateInUTC(date: Date?): String? {
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val dateAsString = sdf.format(date)
        println("UTC$dateAsString")
        return dateAsString
    }

    fun stringToDate(str: String): Date {

        val format =
            SimpleDateFormat("yyyy-MM-dd hh:mm a")
        return format.parse(str)

    }


    fun milliseconds(date: String?): Long {
        //String date_ = date;
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        try {
            val mDate = sdf.parse(date)
            val timeInMilliseconds = mDate.time
            println("Date in milli :: $timeInMilliseconds")
            return timeInMilliseconds
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return 0
    }


    fun updateDialog(data:AvailabilityData,pos:Int) {
        var focus = "from"
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.create_availability_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val window: Window = dialog.getWindow()!!
        window.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog.setTitle("Title...");
        dialog.setCancelable(true)
        val done = dialog.findViewById(R.id.done) as TextView
        val from = dialog.findViewById(R.id.edtFrom) as TextView
        val to = dialog.findViewById(R.id.edtTo) as TextView
        val session = dialog.findViewById(R.id.edtSessionLength) as EditText
        val timepicker = dialog.findViewById(R.id.timePicker) as TimePicker

        from.setText(convertTo12(data.from))
        to.setText(convertTo12(data.to))
        session.setText(data.length)

        from.setOnClickListener() {
            focus = "from"
            from.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            from.setTextColor(resources.getColor(R.color.red))

            to.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            to.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        to.setOnClickListener() {
            focus = "to"
            to.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            to.setTextColor(resources.getColor(R.color.red))

            from.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            from.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        timepicker.setOnTimeChangedListener { _, hour, minute ->
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }
            val hr = if (hour < 10) "0" + hour else hour
            val min = if (minute < 10) "0" + minute else minute
            // display format of time
            val msg = "Time is: $hr : $min $am_pm"

            if (focus.equals("from")) {
                from.text = "$hr:$min $am_pm"

            } else {
                to.text = "$hr:$min $am_pm"

            }

        }


        done.setOnClickListener {
            if (from.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter from time", "Ok")

            } else if (to.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to time", "Ok")

            } else if (session.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to session length", "Ok")

            } else {
                val availabilityObj = AvailabilityData(
                    from = from.text.toString(),
                    to = to.text.toString(),
                    length = session.text.toString(),
                    isConver = false
                )
                adapter!!.updateItem(availabilityObj,pos)
                dialog.dismiss()
            }

        }
        dialog.show()
    }




}