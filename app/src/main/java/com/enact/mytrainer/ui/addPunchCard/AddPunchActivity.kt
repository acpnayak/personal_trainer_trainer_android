package com.enact.mytrainer.ui.addPunchCard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityAddPunchBinding
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginUserData
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddPunchActivity : BaseActivity() {
    private lateinit var binding: ActivityAddPunchBinding
    lateinit var loginUserData: LoginUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddPunchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData= Paper.book().read(Constants.USERDATA)
        setListeners()

    }

    fun setListeners(){
        binding.back.setOnClickListener(){
            finish()
        }

        binding.submitBtn.setOnClickListener(){
            if (binding.editSessions.text.isNullOrBlank()){
                msgAlert(getString(R.string.sessionvalidation),this)
            }
            else if (binding.editPrice.text.isNullOrBlank()){
                msgAlert(getString(R.string.pricevalidation),this)
            }
            else{
                addPunchCardCall()
            }
        }
    }

    fun addPunchCardCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.addPunchCard(
                loginUserData.id,
                binding.editSessions.text.toString(),
                binding.editPrice.text.toString(),
                "Avail "+binding.editSessions.text.toString().trim()+" for $"+binding.editPrice.text.toString()
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok", {
                                    finish()
                                })
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


}