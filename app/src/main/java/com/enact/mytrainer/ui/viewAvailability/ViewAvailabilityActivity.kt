package com.enact.mytrainer.ui.viewAvailability

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.daysOfWeekFromLocale
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityAddAvailabilityBinding
import com.enact.mytrainer.databinding.ActivityViewAvailabilityBinding
import com.enact.mytrainer.ui.punchCard.adapter.PunchCardsAdapter
import com.enact.mytrainer.ui.viewAvailability.adapter.ViewAvailabilityAdapter
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.WeekFields
import java.util.*
import kotlin.collections.ArrayList

class ViewAvailabilityActivity : BaseActivity() {
    private lateinit var binding: ActivityViewAvailabilityBinding
    private val titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy")
    private val yearFormatter = DateTimeFormatter.ofPattern("yyyy")
    private val monthFormatter = DateTimeFormatter.ofPattern("MM")
    var nwevents: ArrayList<LocalDate>? = ArrayList<LocalDate>()
    var backupcurrentMonth = YearMonth.now()
    val today = LocalDate.now()
    var selectedDate: LocalDate? = LocalDate.now()
    val daysOfWeek = daysOfWeekFromLocale()
    var showType = 1
    var weekDays: List<List<CalendarDay>>? = null
    var currentMonth = 0
    var currentYear = 0
    var onLaunch = true;
    lateinit var list:ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewAvailabilityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setUpCalendar()
        list= ArrayList()
        list.add("")
        list.add("")
        list.add("")
        setAdapter(list)
    }

    fun setUpCalendar() {
        binding.calendar.visibility = View.VISIBLE
        binding.calheader.visibility = View.VISIBLE
        binding.calendar.setHasFixedSize(true)


        class DayViewContainer(view: View) : ViewContainer(view) {

            // Without the kotlin android extensions plugin

            val pp = view.findViewById<RelativeLayout>(R.id.pp)
            val textView = view.findViewById<TextView>(R.id.calendarDayText)
            val greendot = view.findViewById<AppCompatImageView>(R.id.available)
        }

        binding.calendar.dayBinder = object : DayBinder<DayViewContainer> {
            // Called only when a new container is needed.
            override fun create(view: View) = DayViewContainer(view)

            // Called every time we need to reuse a container.
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.textView.text = day.date.dayOfMonth.toString()
                if (day.owner == DayOwner.THIS_MONTH) {
                    container.textView.setTextColor(Color.BLACK)
                    if (day.date == selectedDate) {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                            container.textView.setTextColor(Color.WHITE)
                        } else {
//                            container.textView.setTextColor(Color.WHITE)
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar_border)
                        }

                        if (nwevents!!.contains(day.date)) {
//                           container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            container.greendot.visibility = View.GONE
                        }
                    } else {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                        } else
                            container.pp.setBackgroundResource(0)

                        if (nwevents!!.contains(day.date)) {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                                container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                            // container.textView.setTextColor(Color.GRAY)
                                container.greendot.visibility = View.GONE
                        }
                    }
                } else {
                    container.textView.setTextColor(Color.GRAY)
                    container.greendot.visibility = View.GONE
                }
                container.textView.setOnClickListener {
                    selectedDate = day.date
                    binding.calendar!!.notifyCalendarChanged()


                }
            }
        }


        val currentMonth1 = YearMonth.now()
        val firstMonth = currentMonth1.minusMonths(10)
        val lastMonth = currentMonth1.plusMonths(10)
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        binding.calendar.setup(firstMonth, lastMonth, firstDayOfWeek)
        binding.calendar.scrollToMonth(currentMonth1)
        binding.monthTv.text = titleFormatter.format(currentMonth1)
        class MonthViewContainer(view: View) : ViewContainer(view) {

        }
        binding.calendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.

            }
        }
        binding.calendar.monthScrollListener = {
            binding.monthTv.text = titleFormatter.format(it.yearMonth)
            backupcurrentMonth = it.yearMonth

            if (!onLaunch) {
                selectedDate = null
            }
            onLaunch = false
            var month = monthFormatter.format(it.yearMonth)
            var year = yearFormatter.format(it.yearMonth)

        }

        binding. calendar.findFirstVisibleMonth()?.let {
            currentMonth = it.month
            currentYear = it.year
            weekDays = it.weekDays.toList()

        }


    }
    fun setAdapter(list: ArrayList<String>) {
        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
        val adapter = ViewAvailabilityAdapter(list, this)
        binding.rv!!.adapter = adapter
    }


}