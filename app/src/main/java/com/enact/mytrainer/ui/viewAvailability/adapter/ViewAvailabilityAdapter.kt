package com.enact.mytrainer.ui.viewAvailability.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.GymOffer
import com.google.android.material.button.MaterialButton

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ViewAvailabilityAdapter(
    var results: ArrayList<String>,
    private val context: Context

) : RecyclerView.Adapter<ViewAvailabilityAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_availability_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {



    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }



    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var date: TextView

        init {
            date = itemView.findViewById(R.id.date)
        }
    }

}