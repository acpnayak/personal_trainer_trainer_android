package com.enact.mytrainer.ui.searchBookings

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.enact.mytrainer.R
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivitySearchBookingsBinding
import com.enact.mytrainer.databinding.ActivitySearchClientBinding

class SearchBookingsActivity : BaseActivity() {

    private lateinit var binding: ActivitySearchBookingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBookingsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.backbtn.setOnClickListener {
            finish()
        }

    }
}