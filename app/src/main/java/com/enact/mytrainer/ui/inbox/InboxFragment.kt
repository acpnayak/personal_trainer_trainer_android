package com.enact.mytrainer.ui.inbox

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.FireUtils
import com.enact.mytrainer.TrainerUtills.FireUtils.getDataUsingQuery
import com.enact.mytrainer.TrainerUtills.makeInvisible
import com.enact.mytrainer.TrainerUtills.makeVisible
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.FragmentInboxBinding
import com.enact.mytrainer.model.ClientObj
import com.enact.mytrainer.model.ClientResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.model.chat.ChatModel
import com.enact.mytrainer.ui.clients.adapter.ClientsAdapter
import com.enact.mytrainer.ui.gyms.adapter.GymsAdapter
import com.enact.mytrainer.ui.inbox.adapter.InboxAdapter
import com.enact.mytrainer.ui.searchClient.SearchClientActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.toObject
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InboxFragment : Fragment() {

    private lateinit var notificationsViewModel: InboxViewModel
    private var _binding: FragmentInboxBinding? = null

    lateinit var clientlist: ArrayList<ClientObj>
    lateinit var loginUserData: LoginUserData
    lateinit var adapter:InboxAdapter

    var layoutManager: RecyclerView.LayoutManager? = null
    var chats: ArrayList<ChatModel>? = ArrayList()
    private var firestoreDB: FirebaseFirestore? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProvider(this).get(InboxViewModel::class.java)

        _binding = FragmentInboxBinding.inflate(inflater, container, false)
        val root: View = binding.root
        loginUserData = Paper.book().read(Constants.USERDATA)

        notificationsViewModel.text.observe(viewLifecycleOwner, Observer {

        })

        clientlist = ArrayList()

        fetchAllClients()
        getChats()
        binding.add.setOnClickListener() {
            bottomsheet()

        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }






    fun  getChats()
    {
        getDataUsingQuery(FireUtils.CHAT, "trainer_id", loginUserData.id){
            when(it)
            {
                is QuerySnapshot -> setAdapter(it)
                else -> {
                    Toast.makeText(context, "error", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun setAdapter(result: QuerySnapshot)
    {
        var chatsLocal: java.util.ArrayList<ChatModel>? = java.util.ArrayList()
        for (document in result) {

            val chatModel: ChatModel = document.toObject(ChatModel::class.java)
            // var chatModel = document.toObject<ChatModel>()

            chatModel.roomId=document.id
            chatsLocal!!.add(chatModel)

        }
        if(chatsLocal!!.size!=chats!!.size)
        {

            if(FireUtils.refrance!!.size>0)
            {
                var index = 0;
                FireUtils.refrance!!.forEach { it ->
                    it.remove();

                }
            }

            FireUtils.refrance!!.clear()
            chatsLocal.forEach {

                startListen(it)
            }



            chats!!.clear()
            chats!!.addAll(chatsLocal)


            try {
                layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?
                binding.inboxList.setLayoutManager(layoutManager)
                adapter = InboxAdapter(chats, requireContext(), this)
                binding.inboxList.adapter = adapter
            } catch (e: Exception) {
            }

            try {
                if(chats!!.size>0)
                {
                    binding.inboxList.makeVisible()
                    binding.labelno.makeInvisible()
                }
                else
                {
                    binding.inboxList.makeInvisible()
                    binding.labelno.makeVisible()
                }
            } catch (e: Exception) {
            }
        }


    }

    fun setAdapteragain( )
    {
        adapter = InboxAdapter(chats, requireContext(), this)
        binding.inboxList.setAdapter(adapter)
    }



    private fun inilizefirestoreDB() {
        firestoreDB = FirebaseFirestore.getInstance()
    }
    fun startListen(groupInfo: ChatModel) {
        if(firestoreDB==null)
        {
            inilizefirestoreDB()
        }
        groupInfo.unreadCount=("0")
        val docRef = firestoreDB!!.collection(FireUtils.CHAT).document(groupInfo.roomId)
        var docReflistner=  docRef.addSnapshotListener(EventListener { snapshot, e ->
            if (e != null) {
                Log.w("", "Listen failed.", e)
                return@EventListener
            }
            val source =
                if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
            if (snapshot != null && snapshot.exists()) {
                try {
                    Log.e("", "" + snapshot);
                    val message: ChatModel? = snapshot.toObject(ChatModel::class.java)
                    groupInfo.message = (message!!.message)
                    groupInfo.timestamp = (message!!.timestamp)
                    groupInfo.userLastDeletedMsgId = (message!!.userLastDeletedMsgId)

                    var list = chats!!.sortByDescending {
                        it.timestamp.toDate().time
                    }
                    adapter!!.notifyDataSetChanged()

                } catch (e1: Exception) {
                    e1.printStackTrace()
                }
                try {
                    Handler().postDelayed({
                        getallNodes(groupInfo)
                    }, 1000)

                } catch (e: Exception) {
                    Log.e("",""+e.localizedMessage)
                }
            } else {

                // Log.d("", "$source data: null")
            }
        })

        FireUtils.refrance!!.add(docReflistner)


    }
    fun getallNodes(groupInfo: ChatModel) {
        val lastMessageID = Paper.book().read(loginUserData.id + "" + groupInfo.roomId, 0L)
        if (lastMessageID != 0L) {
            firestoreDB!!.collection("Chat").document(groupInfo.roomId)
                .collection("Messages")
                .whereGreaterThan("messageId", lastMessageID).orderBy("messageId")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val count = task.result!!.size().toLong()
                        if (count > 99) {
                            groupInfo.unreadCount=("99+")
                        } else {
                            groupInfo.unreadCount=("" + count)
                        }
                        if (adapter != null) {
                            adapter!!.notifyDataSetChanged()
                        }
                    } else {
                        // groupInfo.setUnreadCount("0")
                        if (adapter != null) {
                            adapter!!.notifyDataSetChanged()
                        }
                    }
                    setbottomBarMessageIocn()
                }
        } else {
            firestoreDB!!.collection(FireUtils.CHAT).document(groupInfo.roomId)
                .collection("Messages").orderBy("timestamp")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val count = task.result!!.size().toLong()
                        if (count > 99) {
                            groupInfo.unreadCount= ("99+")
                        } else {
                            groupInfo.unreadCount=("" + count)
                        }
                        if (adapter != null) {
                            adapter!!.notifyDataSetChanged()
                        }
                    } else {
                        groupInfo.unreadCount=("0")
                        if (adapter != null) {
                            adapter!!.notifyDataSetChanged()
                        }
                    }

                    try {
                        setbottomBarMessageIocn()
                    } catch (e: Exception) {
                    }
                }
        }
    }


    fun bottomsheet() {
        val dialog = context?.let { BottomSheetDialog(it) }
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.activity_search_client)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.window!!.setGravity(Gravity.BOTTOM)
        dialog!!.window!!.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        );

        val clientList = dialog.findViewById<RecyclerView>(R.id.clientList)
        val cancel = dialog.findViewById<TextView>(R.id.cancel)

        clientList?.let { setClientsAdapter(clientlist, it) }

        cancel!!.setOnClickListener() {
            dialog.dismiss()
        }
        dialog!!.show()
    }


    fun setClientsAdapter(list: ArrayList<ClientObj>, rv: RecyclerView) {
        val layoutManager = LinearLayoutManager(context)
        rv.layoutManager = layoutManager
        val adapter = ClientsAdapter(list, requireContext())
        rv.adapter = adapter
    }


    fun fetchAllClients() {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchClients(loginUserData.id)
            ?.enqueue(object : Callback<ClientResponse> {
                override fun onFailure(call: Call<ClientResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<ClientResponse>?,
                    response: Response<ClientResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity).hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                              clientlist=userpojo.data


                            } else {
                                (context as MainActivity).singleButtonDialog(
                                    userpojo.message,
                                    "Ok"
                                ) {

                                }
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok") {

                            }
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    }
                }
            })
    }



    fun setbottomBarMessageIocn()
    {
        var yes= false
        chats!!.forEach {

            try {
                if(it.unreadCount.toInt()>0)
                {
                    yes=true
                }
            } catch (e: Exception) {
                if(it.equals("99+"))
                {
                    yes=true
                }
            }

        }
        try {
            (activity as MainActivity).setImageDotIcon(yes)
        } catch (e: Exception) {
        }
    }

    fun openChatRoomAccToId(id:String){
        if(adapter!=null){
            adapter.openChatRoomFromToId(id)
        }
    }
}