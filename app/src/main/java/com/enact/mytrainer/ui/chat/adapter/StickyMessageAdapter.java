package com.enact.mytrainer.ui.chat.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.enact.mytrainer.R;
import com.enact.mytrainer.TrainerUtills.Constants;
import com.enact.mytrainer.TrainerUtills.FireUtils;
import com.enact.mytrainer.model.LoginUserData;
import com.enact.mytrainer.model.chat.ChatMessage;
import com.enact.mytrainer.ui.chat.ChatActivity;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ozaydin.serkan.com.image_zoom_view.ImageViewZoom;

public class StickyMessageAdapter extends UltimateViewAdapter {
    Context context;
    List<ChatMessage> results;
    LoginUserData loginUserData;

    public StickyMessageAdapter(ArrayList<ChatMessage> results, Context context, LoginUserData loginUserData ) {
        this.context=context;
        this. results= results;
        this.loginUserData=loginUserData;
    }


    @Override
    public int getItemViewType(int position) {
        if(results.get(position).getType().equalsIgnoreCase("1"))
        {
            return VIEW_TYPES.HEADER;
        }
        else
        {

            if (results.get(position).getFrom().equalsIgnoreCase(loginUserData.getId())) {
                if (results.get(position).getMessageType().equalsIgnoreCase(FireUtils.SessionRequest)) {
                    return 31;
                }

                return 21;
            }

            else {
                if (results.get(position).getMessageType().equalsIgnoreCase(FireUtils.SessionRequest)) {
                    return 31;
                }
                return 11;
            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 11) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.left_chat_item, parent, false));
        }
        else  if (viewType == 31){
            return new HeaderViewHolderForAcceptReject(LayoutInflater.from(parent.getContext()).inflate(R.layout.left_chat_item_accept_reject, parent, false));
        }
        else  if (viewType == 21){
            return new HeaderViewHolder1(LayoutInflater.from(parent.getContext()).inflate(R.layout.right_chat_item, parent, false));
        }
        else
        {
            if (viewType == VIEW_TYPES.HEADER) {
                return  newHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_date_header, parent, false));
            }
            return  new DateHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_date_header, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,final int position) {

        if (results.get(position).getId().equalsIgnoreCase("TSFjSdYovUHO8hihwJ3e")) {
            Log.e("",""+results.get(position));

        }
            if (holder instanceof HeaderViewHolder1) {

            if(results.get(position).getDeleted())
            {
                ((HeaderViewHolder1) holder).notification_layout.setVisibility(View.GONE);
                ((HeaderViewHolder1) holder).msg_layout.setVisibility(View.GONE);
                ((HeaderViewHolder1) holder).img_msg.setVisibility(View.GONE);
                ((HeaderViewHolder1) holder).delete_msg_layout.setVisibility(View.VISIBLE);
                DateFormat formatter = new SimpleDateFormat("hh:mm aa");
                String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
                System.out.println("Today : " + todayTime);
                ((HeaderViewHolder1) holder).time_deleteMessage.setText(todayTime);


            }else {
                ((HeaderViewHolder1) holder).delete_msg_layout.setVisibility(View.GONE);

                if(results.get(position).getMessageType().equals(FireUtils.Text))
                {
                    ((HeaderViewHolder1) holder).msg.setVisibility(View.VISIBLE);
                    ((HeaderViewHolder1) holder).notification_layout.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).msg_layout.setVisibility(View.VISIBLE);
                    ((HeaderViewHolder1) holder).img_msg.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).msg.setText(results.get(position).getMessage());



                }
                else if(results.get(position).getMessageType().equals(FireUtils.Image))
                {
                    ((HeaderViewHolder1) holder).msg.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).img_msg.setVisibility(View.VISIBLE);
                    ((HeaderViewHolder1) holder).notification_layout.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).msg_layout.setVisibility(View.VISIBLE);
                    if(!results.get(position).getLocalImage().isEmpty())
                    {
                        ((HeaderViewHolder1) holder).img_msg.setImageURI(Uri.parse(results.get(position).getLocalImage()));
                    }
                    else
                    {
                        Picasso.with(context).load(results.get(position).getMessage()).networkPolicy(NetworkPolicy.OFFLINE).into(((HeaderViewHolder1) holder).img_msg, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(results.get(position).getMessage())

                                        .into(((HeaderViewHolder1) holder).img_msg, new Callback() {
                                            @Override
                                            public void onSuccess() {

                                            }

                                            @Override
                                            public void onError() {
                                                Log.v("Picasso", "Could not fetch image");

                                            }
                                        });
                            }
                        });
                    }


                }
                else {
                    ((HeaderViewHolder1) holder).msg.setVisibility(View.VISIBLE);
                    ((HeaderViewHolder1) holder).notification_layout.setVisibility(View.VISIBLE);
                    ((HeaderViewHolder1) holder).msg_layout.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).img_msg.setVisibility(View.GONE);
                    ((HeaderViewHolder1) holder).notification_msg.setText(results.get(position).getMessage());
                }
            }


            ((HeaderViewHolder1) holder).deleteChat.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Toast.makeText(context,"DELETED",Toast.LENGTH_SHORT).show();
                    results.get(position).setDeleted(true);
                    notifyItemChanged(position);

                    boolean isLastMesage= false;
                    if(position==results.size()-1)
                    {
                        isLastMesage=true;
                    }
                    ((ChatActivity) context).updateMyChatGroup(results.get(position), isLastMesage);


                    ((ChatActivity) context).updateDeleteMessage(results.get(position));


                }
            });



            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
            System.out.println("Today : " + todayTime);
            ((HeaderViewHolder1) holder).time.setText(todayTime);



        }

        else  if (holder instanceof ViewHolder) {

            if(results.get(position).getDeleted())
            {

                ((ViewHolder) holder).delete_msg_layout.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).msg.setVisibility(View.GONE);
                ((ViewHolder) holder).notification_layout.setVisibility(View.GONE);
                ((ViewHolder) holder).msg_layout.setVisibility(View.GONE);
                ((ViewHolder) holder).img_msg.setVisibility(View.GONE);
                ((ViewHolder) holder).notification_msg.setText(results.get(position).getMessage());



                DateFormat formatter = new SimpleDateFormat("hh:mm aa");
                String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
                System.out.println("Today : " + todayTime);
                ((ViewHolder) holder).time_deleteMessage.setText(todayTime);



            }

            else{

                ((ViewHolder) holder).delete_msg_layout.setVisibility(View.GONE);

                if(results.get(position).getMessageType().equals(FireUtils.Text))
                {
                    ((ViewHolder) holder).msg.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).img_msg.setVisibility(View.GONE);
                    ((ViewHolder) holder).notification_layout.setVisibility(View.GONE);
                    ((ViewHolder) holder).msg_layout.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).msg.setText(results.get(position).getMessage());
                    ((ViewHolder) holder).user_img.setVisibility(View.VISIBLE);



                }
                else if(results.get(position).getMessageType().equals(FireUtils.Image))
                {
                    ((ViewHolder) holder).msg.setVisibility(View.GONE);
                    ((ViewHolder) holder).img_msg.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).notification_layout.setVisibility(View.GONE);
                    ((ViewHolder) holder).msg_layout.setVisibility(View.VISIBLE);


                    ((ViewHolder) holder).user_img.setVisibility(View.VISIBLE);


                    if(!results.get(position).getLocalImage().isEmpty())
                    {
                        ((ViewHolder) holder).img_msg.setImageURI(Uri.parse(results.get(position).getLocalImage()));
                    }else
                    {
                        Picasso.with(context).load(results.get(position).getMessage()).networkPolicy(NetworkPolicy.OFFLINE).into(((ViewHolder) holder).img_msg, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(results.get(position).getMessage())

                                        .into(((ViewHolder) holder).img_msg, new Callback() {
                                            @Override
                                            public void onSuccess() {

                                            }

                                            @Override
                                            public void onError() {
                                                Log.v("Picasso", "Could not fetch image");

                                            }
                                        });
                            }
                        });
                    }


                }


                else {
                    ((ViewHolder) holder).msg.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).notification_layout.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).msg_layout.setVisibility(View.GONE);
                    ((ViewHolder) holder).img_msg.setVisibility(View.GONE);
                    ((ViewHolder) holder).user_img.setVisibility(View.GONE);


                    ((ViewHolder) holder).notification_msg.setText(results.get(position).getMessage());
                }
            }



            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
            System.out.println("Today : " + todayTime);
            ((ViewHolder) holder).time.setText(todayTime);



            try {
                if( ((ChatActivity)context).getCurrentChatRoom()!=null  &&  !((ChatActivity)context).getCurrentChatRoom().getClient_imageUrl().isEmpty())
                {
                    Glide.with(context).load(((ChatActivity)context).getCurrentChatRoom().getClient_imageUrl()).placeholder(R.drawable.demoprofile).error(R.drawable.demoprofile).into(((ViewHolder) holder).user_img);
                }
            else
                {
                    ((ViewHolder) holder).user_img.setImageResource(R.drawable.demoprofile);

                }
            } catch ( Exception e) {
                ((ViewHolder) holder).user_img.setImageResource(R.drawable.demoprofile);

            }

        }


        else  if (holder instanceof HeaderViewHolderForAcceptReject) {





                ((HeaderViewHolderForAcceptReject) holder).msg.setVisibility(View.VISIBLE);
                ((HeaderViewHolderForAcceptReject) holder).notification_layout.setVisibility(View.VISIBLE);
                ((HeaderViewHolderForAcceptReject) holder).msg_layout.setVisibility(View.GONE);
                ((HeaderViewHolderForAcceptReject) holder).img_msg.setVisibility(View.GONE);
                //((HeaderViewHolderForAcceptReject) holder).user_img.setVisibility(View.GONE);


                ((HeaderViewHolderForAcceptReject) holder).notification_msg.setText(results.get(position).getMessage());




            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
            System.out.println("Today : " + todayTime);
            ((HeaderViewHolderForAcceptReject) holder).time.setText(todayTime);

            ((HeaderViewHolderForAcceptReject) holder).btn_accept.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v)
                {
                    String clientId=  ((ChatActivity)context ).getCurrentClient().getUser_id();
                    String sessionID= results.get(position).getSessionId();

                    ((ChatActivity)context ).session_update_status( loginUserData.getId(),clientId,sessionID,""+ Constants.Accepted,results.get(position));



                }
            });
            ((HeaderViewHolderForAcceptReject) holder).btn_reject.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v)
                {



                   String clientId=  ((ChatActivity)context ).getCurrentClient().getUser_id();
                   String sessionID= results.get(position).getSessionId();

                    ((ChatActivity)context ).session_update_status(loginUserData.getId(),clientId,sessionID,""+ Constants.Rejected,results.get(position));



                }
            });

        }
        else
        {
            if (DateUtils.isToday(results.get(position).getTimestamp().toDate().getTime())) {
                // SimpleDateFormat sdf = SimpleDateFormat("h:mm a", Locale.getDefault());

                DateFormat formatter = new SimpleDateFormat("hh:mm aa");
                String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
                System.out.println("Today : " + todayTime);
//                ((DateHeaderViewHolder) holder).time.setText("Today");

            } else {
                DateFormat formatter = new SimpleDateFormat("d MMMM,yyyy");
                String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
                System.out.println("Today : " + todayTime);
//                ((DateHeaderViewHolder) holder).time.setText(todayTime);

            }
        }
    }



    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView msg,time,notification_msg,time_deleteMessage;
        ImageView user_img;
        ImageViewZoom img_msg;
        ConstraintLayout msg_layout,notification_layout,delete_msg_layout;

        View line;
        public ViewHolder(View itemView) {
            super(itemView);

            msg = itemView.findViewById(R.id.msg);
            img_msg= itemView.findViewById(R.id.img_msg);
            user_img= itemView.findViewById(R.id.user_img);
            time= itemView.findViewById(R.id.time);
            notification_msg=itemView.findViewById(R.id.notification_msg);
            msg_layout=itemView.findViewById(R.id.msg_layout);
            notification_layout=itemView.findViewById(R.id.noti_layout);
            delete_msg_layout=itemView.findViewById(R.id.delete_msg_layout);
            time_deleteMessage=itemView.findViewById(R.id.time_deleteMessage);



        }
    }

    public class HeaderViewHolder1 extends RecyclerView.ViewHolder {
        TextView msg,time,notification_msg,time_deleteMessage;
        ImageViewZoom img_msg;
        ConstraintLayout notification_layout, delete_msg_layout;
        LinearLayout deleteChat,msg_layout ;


        public HeaderViewHolder1(View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.msg);
            img_msg = itemView.findViewById(R.id.img_msg);
            time= itemView.findViewById(R.id.time);
            notification_msg=itemView.findViewById(R.id.notification_msg);
            msg_layout=itemView.findViewById(R.id.mainMessageLayout);
            notification_layout=itemView.findViewById(R.id.noti_layout);
            deleteChat=itemView.findViewById(R.id.deleteChat);

            time_deleteMessage=itemView.findViewById(R.id.time_deleteMessage);
            delete_msg_layout=itemView.findViewById(R.id.delete_msg_layout);


        }
    }




    public class HeaderViewHolderForAcceptReject extends RecyclerView.ViewHolder {
        TextView msg,time,notification_msg;
        ImageView img_msg;
        ConstraintLayout msg_layout,notification_layout;
        AppCompatTextView btn_accept,btn_reject;

        public HeaderViewHolderForAcceptReject(View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.msg);
            img_msg = itemView.findViewById(R.id.img_msg);
            time= itemView.findViewById(R.id.time);
            notification_msg=itemView.findViewById(R.id.notification_msg);
            msg_layout=itemView.findViewById(R.id.msg_layout);
            notification_layout=itemView.findViewById(R.id.noti_layout);

            btn_accept=itemView.findViewById(R.id.btn_accept);
            btn_reject=itemView.findViewById(R.id.btn_reject);

        }
    }

    public class DateHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView time;


        public DateHeaderViewHolder(View itemView) {
            super(itemView);
            time= itemView.findViewById(R.id.datenm);

        }
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0) {
            return -1;
        } else {
            return getItem(position).charAt(0);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_date_header, parent, false);
        return new DateHeaderViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        TextView textView = (TextView) holder.itemView.findViewById(R.id.datenm);
        textView.setText(getItemToShow(position));

//        holder.itemView.setBackgroundColor(getRandomColor());
    }

    public String getItem(int position) {
        if (DateUtils.isToday(results.get(position).getTimestamp().toDate().getTime())) {
            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
            System.out.println("Today : " + todayTime);
            return "Today";
        } else {
            DateFormat format = new SimpleDateFormat("d");
            format =new  SimpleDateFormat("d MMMM,yyyy");
            String todayTime = format.format(results.get(position).getTimestamp().toDate());


            return todayTime;

        }
    }

    public String getItemToShow(int position) {
        if (DateUtils.isToday(results.get(position).getTimestamp().toDate().getTime())) {
            // SimpleDateFormat sdf = SimpleDateFormat("h:mm a", Locale.getDefault());

            DateFormat formatter = new SimpleDateFormat("hh:mm aa");
            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
            System.out.println("Today : " + todayTime);
            return "Today";
        } else {
//            DateFormat formatter = new SimpleDateFormat("d MMMM,yyyy");
//            String todayTime = formatter.format(results.get(position).getTimestamp().toDate());
//            System.out.println("Today : " + todayTime);



            DateFormat format = new SimpleDateFormat("d");
            String date = format.format(results.get(position).getTimestamp().toDate());


            if (date.endsWith("1") && !date.endsWith("11"))
            {
                format =new SimpleDateFormat("MMMM d'st', yyyy");
            } else if (date.endsWith(
                    "2"
            ) && !date.endsWith("12")
            ) {
                format= new SimpleDateFormat("MMMM d'nd', yyyy");
            } else if (date.endsWith("3") && !date.endsWith(
                    "13"
            )
            ) {
                format= new SimpleDateFormat("MMMM d'rd', yyyy");
            } else {
                format= new  SimpleDateFormat("MMMM d'th', yyyy");
            }

            String todayTime = format.format(results.get(position).getTimestamp().toDate());



            return todayTime;

        }
    }

    @Override
    public RecyclerView.ViewHolder newFooterHolder(View view) {
        return null;
    }


    @Override
    public RecyclerView.ViewHolder newHeaderHolder(View view) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.chat_date_header, parent, false);
        return new UltimateRecyclerviewViewHolder<>(view);
//        return new DateHeaderViewHolder(view) {
//        };
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }


    @Override
    public int getAdapterItemCount() {
        return results.size();
    }

    @Override
    public long generateHeaderId(int position) {
        if (position == 0) {
            return -1;
        } else {
            return getItem(position).codePointCount(0,1);
        }
    }





}