package com.enact.mytrainer.ui.inbox.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.setImage
import com.enact.mytrainer.model.chat.ChatModel
import com.enact.mytrainer.ui.chat.ChatActivity
import com.enact.mytrainer.ui.inbox.InboxFragment
import de.hdodenhof.circleimageview.CircleImageView
import java.text.DateFormat
import java.text.SimpleDateFormat


class InboxAdapter(
    var results: ArrayList<ChatModel>?,
    private val context: Context,
    private val inboxFragment: InboxFragment,
) : RecyclerView.Adapter<InboxAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.inbox_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {


        holder.name.text = results!![position].client_name



        if(results!![position].message.toString().trim().toLowerCase().equals("### this message deleted ###"))
        {
            holder.msg.text = "This message was deleted"
        }else
        {
            holder.msg.text = results!![position].message
        }


        try {
            if(!results!!.get(position).client_imageUrl.isNullOrEmpty())
            {
                holder.chat_img.setImage(results!!.get(position).client_imageUrl)
            }
            else
            {
                holder.chat_img.setImageResource(R.drawable.dummy_user)

            }
        } catch (e: Exception) {
            holder.chat_img.setImageResource(R.drawable.dummy_user)

        }


        /*  Glide.with(context)
              .load(results!![position].imageUrl.getWithBaseURL())
              .into(holder.chat_img)*/

        if(results!![position].unreadCount.equals("0"))
        {
            holder.unread_layout.visibility= View.INVISIBLE
        }else
        {
            holder.unread_layout.visibility= View.VISIBLE
        }
        holder.unreadText.text = results!![position].unreadCount
        // holder.unreadText.text =""
        // holder.unread_layout.visibility= View.INVISIBLE
        holder.time.setVisibility(View.VISIBLE)
        if (DateUtils.isToday(results!![position].timestamp.toDate().time)) { // SimpleDateFormat sdf = SimpleDateFormat("h:mm a", Locale.getDefault());
            val formatter: DateFormat = SimpleDateFormat("hh:mm aa")
            val todayTime =
                formatter.format(results!![position].timestamp.toDate())
            println("Today : $todayTime")
            holder.time.text = todayTime
            if(results!![position].message.isNullOrEmpty())
            {
                holder.time.setVisibility(View.INVISIBLE)
            }else
            {
                holder.time.setVisibility(View.VISIBLE)
            }
        } else {


            var format = SimpleDateFormat("d")
            val date = format.format(results!![position].timestamp.toDate())

            format =
                if (date.endsWith("1") && !date.endsWith("11")) SimpleDateFormat("MMMM d'st', yyyy") else if (date.endsWith(
                        "2"
                    ) && !date.endsWith("12")
                ) SimpleDateFormat("MMMM d'nd', yyyy") else if (date.endsWith("3") && !date.endsWith(
                        "13"
                    )
                ) SimpleDateFormat("MMMM d'rd', yyyy") else SimpleDateFormat("MMMM d'th', yyyy")

            val todayTime = format.format(results!![position].timestamp.toDate())

            holder.time.text = todayTime
            if(results!![position].message.isNullOrEmpty())
            {
                holder.time.setVisibility(View.INVISIBLE)
            }else
            {
                holder.time.setVisibility(View.VISIBLE)
            }
        }



        holder.itemView.setOnClickListener{

            results!![position].unreadCount="0";
            notifyDataSetChanged()



            if(inboxFragment!=null){
                inboxFragment. setbottomBarMessageIocn()
            }

            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra("chatroom", results!![position])
            intent.putExtra("sid", results!![position].client_id)
            (context as MainActivity).startActivityForResult(intent, 202)
            (context as MainActivity).overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
        }
    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results!!.size
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var time: TextView
        var name: TextView
        var msg: TextView
        var unreadText: TextView
        var unread_layout: LinearLayout

        var chat_img: CircleImageView

        init {
            time = itemView.findViewById(R.id.time)
            name = itemView.findViewById(R.id.name)
            msg = itemView.findViewById(R.id.msg)
            unreadText = itemView.findViewById(R.id.unreadText)
            unread_layout = itemView.findViewById(R.id.unread_layout)
            chat_img = itemView.findViewById(R.id.chat_img)
        }
    }


    fun openChatRoomFromToId(id:String){
        if (results!!.size>0){
            for (i in 0..results!!.size-1){
                if(results!![i].trainer_id.equals(id)){
                    results!![i].unreadCount="0";
                    notifyDataSetChanged()


                    if(inboxFragment!=null){
                        inboxFragment. setbottomBarMessageIocn()
                    }

                    val intent = Intent(context, ChatActivity::class.java)
                    intent.putExtra("chatroom", results!![i])
                    intent.putExtra("sid", results!![i].client_id)
                    (context as MainActivity).startActivityForResult(intent, 202)
//                    (context as MainActivity).overridePendingTransition(
//                        R.anim.slide_in_right,
//                        R.anim.slide_out_left
//                    )
                }
            }
        }

    }

}