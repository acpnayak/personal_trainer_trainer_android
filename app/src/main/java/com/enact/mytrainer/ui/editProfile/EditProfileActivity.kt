package com.enact.mytrainer.ui.editProfile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.BaseAdapter
import android.widget.Toast
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.loadUrl
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityEditProfileBinding
import com.enact.mytrainer.databinding.ActivityTrainingOffersBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.trainingoffer.adapter.HashTagAdapter
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.paperdb.Paper
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditProfileActivity : BaseActivity() {
    private lateinit var binding: ActivityEditProfileBinding
    var file: File? = null
    lateinit var loginUserData: LoginUserData
    var hashadapter: HashTagAdapter? = null
    private val AUTOCOMPLETE_REQUEST_CODE = 1001
    var lat = ""
    var lng = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        setListeners()
        fetchAllTrainings()
        setData()
    }

    fun setHashAdapter(list: ArrayList<TrainingObj>) {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        binding.gymtypeList.layoutManager = layoutManager
        hashadapter = HashTagAdapter(list, this, false)
        binding.gymtypeList!!.adapter = hashadapter
    }


    fun setListeners() {


        binding.back.setOnClickListener {
            finish()
        }

        binding.edit.setOnClickListener() {
            ImagePicker.with(this)
                .cropSquare()          //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }


        binding.editAddress.setOnClickListener() {
            searchPlace()
        }

        binding.submitBtn.setOnClickListener() {
            if (binding.editFname.text.isNullOrBlank()) {
                msgAlert(getString(R.string.fnamevalidation), this)
            } else if (binding.editLastName.text.isNullOrBlank()) {
                msgAlert(getString(R.string.lnamevalidation), this)

            } else if (binding.editPrice.text.isNullOrBlank()) {
                msgAlert(getString(R.string.pricevalidation), this)

            } else if (binding.editAddress.text.isNullOrBlank()) {
                msgAlert(getString(R.string.addressvalidation), this)

            } else {
                updateProfile()
            }
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        binding.editAddress.setText(place.name.toString() + ", " + place.address.toString())
                        lat = place.latLng!!.latitude.toString()
                        lng = place.latLng!!.longitude.toString()


                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("ghfg", status.statusMessage.toString())
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The demoprofile canceled the operation.
                }
            }
            return
        } else {

            if (resultCode == Activity.RESULT_OK) {

                file = ImagePicker.getFile(data)
                if (file != null) {
                    binding.profile.setImageURI(Uri.fromFile(file))
                }
                //You can also get File Path from intent
                val filePath: String? = ImagePicker.getFilePath(data)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }


    fun fetchAllTrainings() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchTrainings(

            )
            ?.enqueue(object : Callback<TrainingResponse> {
                override fun onFailure(call: Call<TrainingResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<TrainingResponse>?,
                    response: Response<TrainingResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                for (i in 0..userpojo.data.size - 1) {
                                    for (j in 0..loginUserData.trainings.size - 1) {
                                        if (userpojo.data[i]._id.equals(loginUserData.trainings[j]._id)) {
                                            userpojo.data[i].isSelected = true
                                        }
                                    }

                                }

                                setHashAdapter(response.body().data)

                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    fun setData() {
        binding.profile.loadUrl(Constants.IMAGE_BASE_URL + loginUserData.image)
        binding.editFname.setText(loginUserData.first_name)
        binding.editLastName.setText(loginUserData.last_name)
        binding.editEmail.setText(loginUserData.email)
        binding.editPrice.setText(loginUserData.price)
        binding.editAddress.setText(loginUserData.address)
    }


    fun updateProfile() {

        val ids: ArrayList<String>
        ids = ArrayList()
        for (i in 0..hashadapter!!.getList().size - 1) {
            if (hashadapter!!.getList()[i].isSelected) {
                ids.add(hashadapter!!.getList()[i]._id)
            }
        }
        val csv = android.text.TextUtils.join(",", ids);
        showProgressDialog()
        val trainerId_R = RetrofitClientInstance.toRequestBody(loginUserData.id)
        val fname_R = RetrofitClientInstance.toRequestBody(binding.editFname.text.toString())
        val lname_R = RetrofitClientInstance.toRequestBody(binding.editLastName.text.toString())
        val bio_R = RetrofitClientInstance.toRequestBody(loginUserData.bio)
        val email_R = RetrofitClientInstance.toRequestBody(binding.editEmail.text.toString())
        val price_R = RetrofitClientInstance.toRequestBody(binding.editPrice.text.toString())
        val training_R = RetrofitClientInstance.toRequestBody(csv)
        val socialId_R = RetrofitClientInstance.toRequestBody(loginUserData.social_id)
        val address_R = RetrofitClientInstance.toRequestBody(binding.editAddress.text.toString())
        val lat_R = RetrofitClientInstance.toRequestBody(lat)
        val lng_R = RetrofitClientInstance.toRequestBody(lng)
        val data = HashMap<String, RequestBody>()
        data["trainer_id"] = trainerId_R
        data["first_name"] = fname_R
        data["last_name"] = lname_R
        data["bio"] = bio_R
        data["email"] = email_R
        data["price"] = price_R
        data["training_id"] = training_R
        data["social_id"] = socialId_R
        data["latitude"] = lat_R
        data["longitude"] = lng_R
        data["formatted_address"] = address_R
        if (file !== null) {
            //To put your image file you have to do
            val fileBody = RequestBody.create("image/*".toMediaTypeOrNull(), file!!)
            data["image\"; filename=\"${file!!.name}"] = fileBody

        }

        RetrofitClientInstance.getAPI()
            ?.updateProfile(data)
            ?.enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<LoginResponse>?,
                    response: Response<LoginResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                Paper.book().write(Constants.USERDATA, response.body().data)
                                singleButtonDialog(userpojo.message, "Ok", {
                                    finish()
                                })
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    fun searchPlace() {

        if (!Places.isInitialized()) {
            Places.initialize(this, Constants.googlekey)
        }

        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)

    }


}