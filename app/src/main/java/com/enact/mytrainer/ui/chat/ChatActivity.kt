package com.enact.mytrainer.ui.chat

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.FireUtils
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityChatBinding
import com.enact.mytrainer.databinding.ActivitySearchClientBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.model.chat.ChatMessage
import com.enact.mytrainer.model.chat.ChatModel
import com.enact.mytrainer.ui.chat.adapter.StickyMessageAdapter
import com.enact.mytrainer.ui.clientDetail.ClientDetailActivity

import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.Timestamp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonElement
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView
import com.marshalchen.ultimaterecyclerview.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import io.paperdb.Paper
 import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatActivity : BaseActivity() {

    var layoutManager: RecyclerView.LayoutManager? = null
    private lateinit var binding: ActivityChatBinding
    //    var adapter: MessageAdapter? = null
    var adapter: StickyMessageAdapter? = null
    var msgList: ArrayList<ChatMessage>? = null
    var currentChatRoom: ChatModel? = null

    var registration: ListenerRegistration? = null
    private var firestoreDB: FirebaseFirestore? = null
    var selectedSession: SessionData? = null
    var selectedClient: ClientObj? = null
    var lastMessageId: String? = ""
    var lastMessageIdForLong: Long? = 0
    var dates = ArrayList<String>()
    var lastChatObject: ChatMessage? = null
    var timestampDeleteMessage = Timestamp.now()
//    var sid = "sid"
    var isBlock = ""
    lateinit var push_tokens: java.util.ArrayList<String>
    lateinit var currentClient: ClientObj
    var client_id = ""
    var lastOnlineRef: DatabaseReference? = null
    var myConnectionsRef: DatabaseReference? = null
    var trainer_name = ""
    var copymsg = ChatMessage()
    lateinit var loginUserData: LoginUserData
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        binding = ActivityChatBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        loginUserData = Paper.book().read(Constants.USERDATA)
        push_tokens = java.util.ArrayList()

        if (intent.hasExtra("chatroom")) {
            currentChatRoom = intent.getParcelableExtra("chatroom")

            getClientDetails(loginUserData.id, currentChatRoom!!.client_id)
            trainer_name = currentChatRoom!!.name


        } else if (intent.hasExtra(Constants.selectedSession)) {

            selectedSession = intent.getSerializableExtra(Constants.selectedSession) as? SessionData
            currentChatRoom = ChatModel()

            getClientDetails(loginUserData.id, selectedSession!!.client_id)
            trainer_name = selectedSession!!.first_name


        } else if (intent.hasExtra(Constants.selectedClient)) {

            selectedClient = intent.getSerializableExtra(Constants.selectedClient) as? ClientObj
            currentChatRoom = ChatModel()
            getClientDetails(loginUserData.id, selectedClient!!.user_id)
            trainer_name = selectedClient!!.first_name

            // isBlock = selectedClient!!.isBlocked
        } else {
            currentChatRoom = ChatModel()
            getClientDetails(loginUserData.id, currentChatRoom!!.client_id)
            // currentChatRoom= ChatModel("","","","", Timestamp.now(),"","","","","","",false)
            trainer_name = currentChatRoom!!.name

        }





        binding.msgList!!.setHasFixedSize(false)
        msgList = ArrayList()

        binding.backBtn.setOnClickListener() {
            onBackPressed()
        }


        binding.camera.setOnClickListener {
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }


        binding.sendmsg.setOnClickListener {
            if (binding.messageArea.getText().toString().trim().length == 0) {
            } else {

                val msg = ChatMessage()

                if (currentClient != null) {
                    msg.to = currentClient.user_id
                    msg.from = loginUserData.id
                }


                val tsLong = System.currentTimeMillis()
                val ts = tsLong.toString()

                msg.message = binding.messageArea.getText().toString().trim()
                msg.senderName = loginUserData.first_name + " " + loginUserData.last_name
                msg.messageType = FireUtils.Text
                msg.deleted = false
                msg.messageId = System.currentTimeMillis()
                msg.timestamp = Timestamp.now()


                var roomID=getRoomId()
                val myMessageRef= Firebase.firestore.collection("${FireUtils.CHAT}/${roomID}/${FireUtils.MESSAGES}").document();



                msg.id=myMessageRef.id.toString()
                // myMessageRef.key
                addMsg(msg)



                var fullName = loginUserData.first_name
                if (!loginUserData.last_name.isNullOrEmpty()) {
                    fullName = fullName + " " + loginUserData.last_name
                }
                fullName = fullName + " sent you a message"



                binding. messageArea.setText("")
                binding.msgList!!.scrollVerticallyToPosition(msgList!!.size - 1); // yourList is the ArrayList that you are passing to your RecyclerView Adapter.

                val iOSData: HashMap<String, Any> = HashMap<String, Any>()

                if (currentChatRoom!!.roomId.isNullOrEmpty()) {
                    setMyCurrentChatRoom(msg, null, myMessageRef)
                } else {

                    if (push_tokens.size > 0) {
                        for (i in 0..push_tokens.size - 1) {
                            sentToNotification(
                                fullName,
                                "",
                                push_tokens[i], iOSData, "1", Constants.ChatMessage.toString(),
                                loginUserData.id,
                                "chat"

                            )
                        }
                    }
                    updateMyChatGroup(msg, false)
                    FireUtils.saveMessageUsingRef(
                        myMessageRef,
                        msg
                    ) {
                        Log.e("", "" + it.toString())
                    }
                }
            }
        }


        binding.imgArrow.rotation = 180f
        binding.clName.setOnClickListener() {
            if (binding.topSheet.isVisible) {
                toggle(false)
                binding.imgArrow.rotation = 180f
            } else {
                toggle(true)
                binding.imgArrow.rotation = 0f
            }
        }


        binding.tvDeleteChat.setOnClickListener() {
            // deleteChatAction()
            val intent= Intent(context, ClientDetailActivity::class.java)
            intent.putExtra("clientID",currentClient.user_id)
            context.startActivity(intent)
        }

        binding.tvBlockUser.setOnClickListener() {
            if (isBlock.equals("0")) {

                doubleButtonDialog(
                    "Are you sure you want to block this " + trainer_name + "?",
                    "Cancel",
                    "Block",
                    button2Click = {

                        toggle(false)
                        binding.imgArrow.rotation = 180f

                        try {
                            block_unblocl(currentClient!!.user_id, loginUserData.id, "1")
                            isBlock = "1"
                            binding. tvBlockUserText.text = "Unblock User"
                        } catch (e: Exception) {
                        }

                    })


            } else {

                doubleButtonDialog(
                    "Are you sure you want to unblock this " + trainer_name + "?",
                    "Cancel",
                    "Unblock",
                    button2Click = {
                        toggle(false)
                        binding.imgArrow.rotation = 180f

                        try {
                            block_unblocl(currentClient!!.user_id, loginUserData.id, "1")
                            isBlock = "0"
                            binding.tvBlockUserText.text = "Block User"
                        } catch (e: Exception) {
                        }
                    })

            }
        }

        binding. tvReportUser.setOnClickListener() {
            doubleButtonDialog(
                "Are you sure you want to report this " + trainer_name + "?",
                "Cancel",
                "Report",
                button2Click = {
                    toggle(false)
                    binding. imgArrow.rotation = 180f

                    if (currentChatRoom!!.client_id.isNullOrEmpty() || currentChatRoom!!.trainer_id.isNullOrEmpty()) {
                        if (selectedSession != null) {
                            report_client(selectedSession!!.client_id, loginUserData.id,"0")
                        } else {

                            report_client(selectedClient!!.user_id, loginUserData.id,"0")

                        }

                    } else {
                        report_client(currentChatRoom!!.client_id, loginUserData.id,"0")

                    }
                })
            // blockAndReportUser()


        }

        showProgressDialog()
        if (!currentChatRoom!!.client_id.isNullOrEmpty()) {
            getChats()
        } else {
            //  chat_name.text=selectedSession!!.name
            getRoomFromFirebase();
        }


    }


    fun updateMyChatGroupOnOpenChat() {

        if (loginUserData.image.isNotEmpty()) {
            currentChatRoom!!.imageUrl = Constants.IMAGE_BASE_URL + loginUserData.image

        }
        try {
            if (currentClient.image.isNotEmpty()) {
                currentChatRoom!!.client_imageUrl = Constants.IMAGE_BASE_URL + currentClient.image

            }
        } catch (e: Exception) {
        }


        try {
            currentChatRoom!!.client_name =
                selectedClient!!.first_name + " " + selectedClient!!.last_name
        } catch (e: Exception) {
        }

        if(!currentChatRoom!!.roomId.isNullOrEmpty())
        {
            FireUtils.updateMessageGroup(
                currentChatRoom!!.roomId,
                currentChatRoom!!
            )
            {

            }
        }


    }

    override fun onBackPressed() {

        Paper.book()
            .write(loginUserData.id + "" + currentChatRoom!!.roomId, lastMessageIdForLong)


        val intent = Intent()
        setResult(Activity.RESULT_OK, intent);
        finish() //finishing activityZZ

    }

    private fun toggle(show: Boolean) {
        val redLayout = findViewById<View>(com.enact.mytrainer.R.id.top_sheet)
        val parent = findViewById<ViewGroup>(com.enact.mytrainer.R.id.parent)
        val transition: Transition = Slide(Gravity.TOP)
        transition.setDuration(300)
        transition.addTarget(com.enact.mytrainer.R.id.top_sheet)
        TransitionManager.beginDelayedTransition(parent, transition)
        redLayout.visibility = if (show) View.VISIBLE else View.GONE
    }


/*
    fun setAdapter(){
        val data1=ChatMessage()
        data1.type="2"
        val data2=ChatMessage()
        data1.type="3"
        val data3=ChatMessage()
        data1.type="2"
        val data4=ChatMessage()
        data1.type="3"
        val data5=ChatMessage()
        data1.type="2"
        msgList!!.add(data1)
        msgList!!.add(data2)
        msgList!!.add(data3)
        msgList!!.add(data4)
        msgList!!.add(data5)
        layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        recyclerView!!.setLayoutManager(layoutManager)
        adapter = StickyMessageAdapter(msgList, this)
        recyclerView!!.setAdapter(adapter)

        val headersDecor =
            StickyRecyclerHeadersDecoration(adapter!!)
        recyclerView!!.addItemDecoration(headersDecor)
        adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                headersDecor.invalidateHeaders()
            }
        })
    }
*/


    fun getRoomId(): String {


        try {// Trainer Id
            var uid: String = loginUserData.id
            /// Client Id
            var gid: String = currentClient.user_id
            return uid + "_" + gid
        } catch (e: Exception) {


            if (selectedSession != null) {
                return loginUserData.id + "_" + selectedSession!!.client_id;
            } else {
                return loginUserData.id + "_" + selectedClient!!.user_id;
            }
        }


    }

    fun getRoomFromFirebase() {
        var roomID = getRoomId()
        FireUtils.getChatRoomAccToID(FireUtils.CHAT, roomID)
        {
            when (it) {
                is DocumentSnapshot -> setAdapter(it)
                else -> {
                    // Toast.makeText(this,"error",Toast.LENGTH_LONG).show()
                }
            }
            Log.d("", "");
            hideProgress()
        }

    }

    fun setAdapter(result: DocumentSnapshot) {
        if (result.exists()) {
            var ChatModel = result.toObject<ChatModel>()
            ChatModel!!.roomId = result.id
            currentChatRoom = ChatModel
            getChats()
        } else {
            layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
            binding.msgList!!.setLayoutManager(layoutManager)
            adapter = StickyMessageAdapter(msgList, this,loginUserData)
            binding.msgList!!.setAdapter(adapter)

            val headersDecor =
                StickyRecyclerHeadersDecoration(adapter!!)
            binding.msgList!!.addItemDecoration(headersDecor)
            adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onChanged() {
                    headersDecor.invalidateHeaders()
                }
            })
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            var file = ImagePicker.getFile(data)
            if (file != null) {

                val msg = ChatMessage()
                if (currentChatRoom!!.client_id.isNullOrEmpty() || currentChatRoom!!.trainer_id.isNullOrEmpty()) {
                    if (selectedSession != null) {
                        msg.to = selectedSession!!.client_id

                    } else {
                        msg.to = selectedClient!!.user_id

                    }


                } else {
                    msg.to = currentChatRoom!!.client_id

                }



                msg.from = loginUserData.id
                msg.message = ""
                msg.senderName = loginUserData.first_name + " " + loginUserData.last_name
                msg.messageType = FireUtils.Image
                msg.messageId = System.currentTimeMillis()
                msg.localImage = file.absolutePath
                msg.timestamp = Timestamp.now()

                var roomID=getRoomId()
                val myMessageRef= Firebase.firestore.collection("${FireUtils.CHAT}/${roomID}/${FireUtils.MESSAGES}").document();



                msg.id=myMessageRef.id.toString()

                msgList!!.add(msg)
                adapter!!.notifyDataSetChanged()
                binding.messageArea.setText("")


                var fullName = loginUserData.first_name
                if (!loginUserData.last_name.isNullOrEmpty()) {
                    fullName = fullName + " " + loginUserData.last_name
                }
                fullName = fullName + " sent you an image"


                binding.msgList!!.scrollVerticallyToPosition(msgList!!.size - 1); // yourList is the ArrayList that you are passing to your RecyclerView Adapter.

                if (currentChatRoom!!.roomId.isNullOrEmpty()) {
                    setMyCurrentChatRoom(msg, fileUri, myMessageRef)
                } else {
                    updateMyChatGroup(msg, false)
                    FireUtils.uploadImgaeOnFireStrore(
                        msg,
                        fileUri,
                        myMessageRef
                    ) {
                        Log.e("", "" + it.toString())
                    }
                }

                val iOSData: HashMap<String, Any> = HashMap<String, Any>()

                if (push_tokens.size > 0) {
                    for (i in 0..push_tokens.size - 1) {
                        sentToNotification(
                            fullName,
                            "",
                            push_tokens[i], iOSData, "1", Constants.ChatMessage.toString(),
                            loginUserData.id,
                            "chat"

                        )
                    }
                }


            }

            //uploadImgaeOnFireStrore

            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            //  Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun inilizefirestoreDB() {
        firestoreDB = FirebaseFirestore.getInstance()
    }


    fun getMessages(collectionname: String) {
        if (firestoreDB == null) {
            inilizefirestoreDB()
        }
        if (registration != null) {
            registration!!.remove()
        }

        registration = firestoreDB!!.collection(collectionname).orderBy("timestamp")
            .addSnapshotListener { snapshots, e ->
                if (e != null) {

                } else {
                    for (dc in snapshots!!.documentChanges) {
                        when (dc.type) {
                            DocumentChange.Type.ADDED -> {
                                var message = dc.document.toObject(ChatMessage::class.java)

                                if (message.id.isNullOrEmpty()) {
                                    lastMessageId = dc.document.id
                                } else {
                                    lastMessageId = message.id
                                }

                                // copymsg.id=message.id
                                lastMessageIdForLong = message.messageId


                                Paper.book()
                                    .write(
                                        loginUserData.id + "" + currentChatRoom!!.roomId,
                                        lastMessageIdForLong
                                    )


                                if (lastChatObject != null) {
                                    try {
                                        if (lastChatObject!!.timestamp.seconds == message.timestamp.seconds) {
                                            currentChatRoom!!.userLastDeletedMsgId =
                                                lastMessageId as String
                                            FireUtils.updateDeleteMessageGroup(
                                                currentChatRoom!!.roomId,
                                                currentChatRoom!!
                                            )
                                            {

                                            }
                                        }
                                    } catch (e: Exception) {
                                    }
                                }





                                if (!message.from.equals(loginUserData.id)) {
                                    message.roomId = dc.document.id
                                    var monthList: List<ChatMessage> =
                                        msgList!!.filter { s -> s.id == message.roomId }
                                    if (monthList.size == 0) {
                                        if (message.timestamp.seconds > timestampDeleteMessage.seconds) {
                                            addMsg(message)
                                        }

                                    }
                                }

                            }
                            DocumentChange.Type.MODIFIED -> {
                                var message = dc.document.toObject(ChatMessage::class.java)

                                if (message.deleted) {
                                    if (!message.id.isNullOrBlank()) {
                                        msgList!!.forEach {
                                            if (it.id.equals(message.id)) {
                                                if (message.deleted) {
                                                    try {
                                                        it.deleted = true
                                                        adapter!!.notifyDataSetChanged()

                                                    } catch (e: Exception) {
                                                    }
                                                    return@forEach
                                                }
                                            }
                                        }
                                    }
                                } else if (message.messageType.equals(
                                        FireUtils.NoAdded,
                                        ignoreCase = true
                                    )
                                ) {
                                    if (!message.id.isNullOrBlank()) {
                                        msgList!!.forEach {
                                            if (it.id.equals(message.id)) {
                                                //if (message.deleted) {
                                                try {
                                                    it.messageType = FireUtils.NoAdded
                                                    adapter!!.notifyDataSetChanged()

                                                } catch (e: Exception) {
                                                }
                                                return@forEach
                                                // }
                                            }
                                        }
                                    }
                                }
                            }
                            DocumentChange.Type.REMOVED -> Log.d(
                                "TAG", "Removed Msg: " + dc.document.toObject(
                                    // Message::class.java
                                )
                            )
                        }
                    }
                }
                // Log.d("TAG", "Current users born before 1900: $snapshots")
            }
    }

    fun removeListen() {
        if (registration != null) {
            registration!!.remove()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        removeListen()
    }

    fun getChats() {

        FireUtils.getDataUsingCollectionName("${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}") {
            when (it) {
                is QuerySnapshot -> setAdapter(it)
                else -> {
                    setAdapter(null)
                    Toast.makeText(this@ChatActivity, "error", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    fun setAdapter(result: QuerySnapshot?) {
        if (result != null) {
            for (document in result) {
                var city = document.toObject<ChatMessage>()
                if (currentChatRoom!!.userLastDeletedMsgId.equals(city.id)) {
                    timestampDeleteMessage = city.timestamp
                    Log.e("", "" + city.id);
                    break;
                }
                lastMessageId = city.id
                city.roomId = document.id
                msgList!!.add(city)
            }
        }

        if (msgList!!.size > 0) {
            // msgList!!.reverse();
            //var messageList=    msgList!!.get(msgList!!.size-1)
            //lastMessageId=messageList.id

            lastMessageIdForLong = msgList!!.get(0).messageId
            Paper.book()
                .write(
                    loginUserData.id + "" + currentChatRoom!!.roomId,
                    msgList!!.get(0).messageId
                )
            msgList!!.reverse()
        }
        grouping(msgList!!)
        layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        binding.msgList!!.setLayoutManager(layoutManager)
        adapter = StickyMessageAdapter(msgList, this,loginUserData)
        binding.msgList!!.setAdapter(adapter)
        val headersDecor =
            StickyRecyclerHeadersDecoration(adapter!!)
        binding.msgList!!.addItemDecoration(headersDecor)
        adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                headersDecor.invalidateHeaders()
            }
        })
        try {
            binding.msgList!!.scrollVerticallyToPosition(msgList!!.size - 1); // yourList is the ArrayList that you are passing to your RecyclerView Adapter.
        } catch (e: Exception) {
        }
        getMessages("${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}")

        hideProgress()

    }

    fun addMsg(list: ChatMessage) {

        val formatter: DateFormat = SimpleDateFormat("d MMMM,yyyy")
        val todayTime = formatter.format(list.timestamp.toDate())
        if (dates.contains(todayTime)) {
            msgList!!.add(list)
        } else {
            dates.add(todayTime)
            list.type = "1"
            msgList!!.add(list)
            copymsg = ChatMessage()
            copymsg.type = ""
            copymsg.roomId = list.roomId
            copymsg.id = list.id
            copymsg.messageId = list.messageId
            copymsg.from = list.from
            copymsg.to = list.to
            copymsg.message = list.message
            copymsg.messageType = list.messageType
            copymsg.timestamp = list.timestamp
            copymsg.senderName = list.senderName
            copymsg.localImage = list.localImage
            // copymsg. timestamp = Timestamp.now()
            msgList!!.add(copymsg)
        }

        adapter!!.notifyDataSetChanged()
        binding.msgList!!.scrollVerticallyToPosition(msgList!!.size - 1); // yourList is the ArrayList that you are passing to your RecyclerView Adapter.

    }

    fun grouping(list: ArrayList<ChatMessage>) {
        var msg = ArrayList<ChatMessage>()



        list.forEach {
            val formatter: DateFormat = SimpleDateFormat("d MMMM,yyyy")
            val todayTime = formatter.format(it.timestamp.toDate())
            if (dates.contains(todayTime)) {
                msg.add(it)
            } else {
                dates.add(todayTime)
                it.type = "1"
                msg.add(it)
                var copymsg = ChatMessage()
                copymsg.type = ""
                copymsg.roomId = it.roomId
                copymsg.id = it.id
                copymsg.messageId = it.messageId
                copymsg.from = it.from
                copymsg.to = it.to
                copymsg.message = it.message
                copymsg.messageType = it.messageType
                copymsg.timestamp = it.timestamp
                copymsg.senderName = it.senderName
                copymsg.localImage = it.localImage
                msg.add(copymsg)
            }
        }
        msgList!!.clear()
        msgList!!.addAll(msg)
    }

    fun updateMyChatGroup(msg: ChatMessage, deleted: Boolean) {
        var text = "";
        if (msg.messageType.equals(FireUtils.Image)) {
            // text = "#pic#"
            text = "Photo"
        } else {
            text = msg.message
        }

        if(deleted)
        {
            text= "### This message deleted ###"
        }

        try {
            currentChatRoom!!.client_name =
                selectedClient!!.first_name + " " + selectedClient!!.last_name

            if (loginUserData.image.isNotEmpty()) {
                currentChatRoom!!.imageUrl = Constants.IMAGE_BASE_URL + loginUserData.image

            }


            var fullName=loginUserData.first_name
            if(!loginUserData.last_name.isNullOrEmpty())
            {
                fullName= fullName+" "+loginUserData.last_name
            }

            currentChatRoom!!.name=fullName

            try {
                if (currentClient.image.isNotEmpty()) {
                    currentChatRoom!!.client_imageUrl =
                        Constants.IMAGE_BASE_URL + currentClient.image

                }
            } catch (e: Exception) {
            }
        } catch (e: Exception) {
        }


        currentChatRoom!!.message = text
        currentChatRoom!!.timestamp = msg.timestamp
        currentChatRoom!!.unreadMessage = true
        FireUtils.updateMessageGroup(
            currentChatRoom!!.roomId,
            currentChatRoom!!
        )
        {

        }
    }


    fun setMyCurrentChatRoom(msg: ChatMessage, file: Uri?, myMessageRef: DocumentReference) {

        currentChatRoom!!.unreadCount = "0";
        currentChatRoom!!.roomId = getRoomId();

        if (selectedSession != null) {
            currentChatRoom!!.name =
                selectedSession!!.first_name + " " + selectedSession!!.last_name;

            currentChatRoom!!.client_id = selectedSession!!.client_id;
        } else {
            currentChatRoom!!.name = selectedClient!!.first_name + " " + selectedClient!!.last_name;

            currentChatRoom!!.client_id = selectedClient!!.user_id;
        }



        if (loginUserData.image.isNotEmpty()) {
            currentChatRoom!!.imageUrl = Constants.IMAGE_BASE_URL + loginUserData.image

        }
        try {
            if (currentClient.image.isNotEmpty()) {
                currentChatRoom!!.client_imageUrl = Constants.IMAGE_BASE_URL + currentClient.image

            }
        } catch (e: Exception) {
        }



        currentChatRoom!!.message = msg!!.message;
        currentChatRoom!!.timestamp = msg!!.timestamp;
        currentChatRoom!!.userLastDeletedMsgId = "";
        currentChatRoom!!.gymLastDeletedMsgId = "";
        currentChatRoom!!.uid = loginUserData.id;
        currentChatRoom!!.trainer_id = loginUserData.id;
        currentChatRoom!!.client_id = selectedClient!!.user_id;

        //currentChatRoom!!.trainer_id = sid;
        currentChatRoom!!.unreadMessage = true;

        FireUtils.saveMessageGroup(
            getRoomId(),
            currentChatRoom!!
        )
        {
            Log.e("", "" + it);

            if (file != null) {
                FireUtils.uploadImgaeOnFireStrore(
                    msg,
                    file,
                    myMessageRef
                ) {
                    Log.e("", "" + it.toString())
                }
            } else {
                FireUtils.saveMessageUsingRef(
                    myMessageRef,
                    msg
                ) {
                    Log.e("", "" + it.toString())
                }
            }
            getMessages("${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}")
        }
    }


    fun deleteChatAction() {

        if (msgList!!.size == 0) {
            //self.toggleHeaderOptions()
            return
        }
        iosAlertDialog("Are you sure you want to delete the chat history?")
    }

    internal fun showDialog() {
        val alert = AlertDialog.Builder(this@ChatActivity)
        alert.setMessage("Are you sure you want to delete the chat history?")
        alert.setPositiveButton(
            "Delete"
        ) { dialog, whichButton ->
            //What ever you want to do with the value
            dialog.dismiss()


            if (msgList!!.size > 0) {
                try {
                    lastChatObject = (msgList!!.get(msgList!!.size - 1))
                } catch (e: Exception) {
                }
                if (!lastMessageId.isNullOrEmpty() && !currentChatRoom!!.trainer_id.isNullOrEmpty()) {

                    currentChatRoom!!.userLastDeletedMsgId = lastChatObject!!.id as String
                    FireUtils.updateDeleteMessageGroup(
                        currentChatRoom!!.roomId,
                        currentChatRoom!!
                    )
                    {

                    }
                }
            }


            try {
                msgList!!.clear();
                adapter!!.notifyDataSetChanged()
            } catch (e: Exception) {

                layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
                binding.msgList!!.setLayoutManager(layoutManager)
                adapter = StickyMessageAdapter(msgList, this,loginUserData)
                binding.msgList!!.setAdapter(adapter)

                val headersDecor =
                    StickyRecyclerHeadersDecoration(adapter!!)
                binding.msgList!!.addItemDecoration(headersDecor)
                adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                    override fun onChanged() {
                        headersDecor.invalidateHeaders()
                    }
                })
                Log.e("", "" + e.message)
            }


        }
        alert.setNegativeButton("Cancel") { dialog, whichButton ->
            dialog.dismiss()
        }



        alert.show()
    }


    fun blockAndReportUser() {

    }


    fun block_unblocl(client_id: String, trainer_id: String, type: String) {
        showProgressDialog()
        RetrofitClientInstance.getAPI()?.report_client( trainer_id, type,client_id)
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok") {

                                    if (type.equals("0")) {
                                        binding.tvBlockUserText.text = "Block User"
                                        isBlock = "0"

                                    } else {
                                        binding.tvBlockUserText.text = "Unblock User"
                                        isBlock = "1"
                                    }


                                }
                            } else {
                                singleButtonDialog(userpojo.message, "Ok") {

                                }
                            }

                        } else {
                            iosAlert(
                                getString(com.enact.mytrainer.R.string.pleasetryagain),
                                this@ChatActivity
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            iosAlert(userpojo.message, this@ChatActivity)
                        } else {
                            iosAlert(
                                getString(com.enact.mytrainer.R.string.pleasetryagain),
                                this@ChatActivity
                            )
                        }
                    }
                }


            })
    }


    fun report_client(client_id: String, trainer_id: String, type:String) {
        showProgressDialog()
        RetrofitClientInstance.getAPI()?.report_client(trainer_id, type,client_id)
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok") {

                                }
                            } else {
                                singleButtonDialog(userpojo.message, "Ok") {

                                }
                            }

                        } else {
                            iosAlert(
                                getString(com.enact.mytrainer.R.string.pleasetryagain),
                                this@ChatActivity
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            iosAlert(userpojo.message, this@ChatActivity)
                        } else {
                            iosAlert(
                                getString(com.enact.mytrainer.R.string.pleasetryagain),
                                this@ChatActivity
                            )
                        }
                    }
                }


            })
    }


    fun iosAlertDialog(message: String) {
        val dialog = Dialog(context!!)
        dialog.setContentView(com.enact.mytrainer.R.layout.logout_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false)
        val ok = dialog.findViewById(com.enact.mytrainer.R.id.ios_logout) as TextView
        val ios_cancel = dialog.findViewById(com.enact.mytrainer.R.id.ios_cancel) as TextView
        val text = dialog.findViewById(com.enact.mytrainer.R.id.ios_text_logout) as TextView
        text.text = message

        ok.setOnClickListener {
            dialog.dismiss()
            if (msgList!!.size > 0) {
                try {
                    lastChatObject = (msgList!!.get(msgList!!.size - 1))
                } catch (e: Exception) {
                }
                if (!lastMessageId.isNullOrEmpty() && !currentChatRoom!!.trainer_id.isNullOrEmpty()) {

                    currentChatRoom!!.userLastDeletedMsgId = lastChatObject!!.id as String
                    FireUtils.updateDeleteMessageGroup(
                        currentChatRoom!!.roomId,
                        currentChatRoom!!
                    )
                    {

                    }
                }
            }


            try {
                msgList!!.clear();
                adapter!!.notifyDataSetChanged()
            } catch (e: Exception) {

                layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
                binding.msgList!!.setLayoutManager(layoutManager)
                adapter = StickyMessageAdapter(msgList, this,loginUserData)
                binding.msgList!!.setAdapter(adapter)

                val headersDecor =
                    StickyRecyclerHeadersDecoration(adapter!!)
                binding.msgList!!.addItemDecoration(headersDecor)
                adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                    override fun onChanged() {
                        headersDecor.invalidateHeaders()
                    }
                })
                Log.e("", "" + e.message)
            }


        }

        ios_cancel.setOnClickListener() {
            dialog.dismiss()
        }

        dialog.show()


    }


    fun getClientDetails(userId: String, client_id: String) {
        showProgressDialog()
        RetrofitClientInstance.getAPI()?.fetchClientDetail(userId, client_id)
            ?.enqueue(object : Callback<ClientDeatilsResponse> {
                override fun onFailure(call: Call<ClientDeatilsResponse>?, t: Throwable?) {
                    hideProgress()
                    /// var clientData: ClientDataDeatils
                    // setView(clientData)
                }

                override fun onResponse(
                    call: Call<ClientDeatilsResponse>?,
                    response: Response<ClientDeatilsResponse>?
                ) {
                   // val clientDetails = response?.body()
                    hideProgress()

                    if (response != null && response.body()?.success!!) {
                        //push_tokens = response!!.body()!!.data.push_token
                        currentClient = response!!.body()!!.data

                        binding.chatName.text =
                            currentClient!!.first_name + " " + currentClient!!.last_name

                        startListeningOtherPersonOnlineStatus()

                       // isBlock = currentClient.isBlocked.toString()
                        if (isBlock.equals("0")) {
                            binding.tvBlockUserText.text = "Block User"
                        } else {
                            binding.tvBlockUserText.text = "Unblock User"
                        }

                        try {
                            if (!currentClient.image.equals("")) {
                                val into = Glide.with(context)
                                    .load(Constants.IMAGE_BASE_URL + currentClient.image)
                                    .placeholder(com.enact.mytrainer.R.drawable.demoprofile)
                                    .error(com.enact.mytrainer.R.drawable.demoprofile).into(binding.trainerImage)
                            }
                        } catch (e: Exception) {

                        }

                    }
                }
            })
    }





    fun session_update_status(
        trainer_id: String,
        client_id: String,
        session_id: String,
        status: String,
        chatMessage: ChatMessage
    ) {
//        showProgressDialog()
//        RetrofitClientInstance.getAPI()?.session_update_status(
//            trainer_id,
//            client_id,
//            session_id,
//            status
//        )
//            ?.enqueue(object : Callback<CommonResponse> {
//                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
//                    hideProgress()
//                    /// var clientData: ClientDataDeatils
//                    // setView(clientData)
//                }
//
//                override fun onResponse(
//                    call: Call<CommonResponse>?,
//                    response: Response<CommonResponse>?
//                ) {
//                    val res = response?.body()
//
//                    if (res!!.success) {
//
//
//                        val formatS = SimpleDateFormat("dd MMMM, yyyy")
//                        val formatT = SimpleDateFormat("hh:mma")
//
//
//                        var dateStr = ""
//                        var timeStr = ""
//
//
//                        try {
//                            dateStr = formatS.format(chatMessage.sessionStartTime.toDate())
//                            timeStr = formatT.format(chatMessage.sessionStartTime.toDate())
//                        } catch (e: Exception) {
//                            Log.e("", "" + e)
//                        }
//
//                        var isAccepted = false
//
//                        if (status.equals(Constants.Accepted.toString())) {
//                            isAccepted = true
//                            if (Paper.book().read(Constants.SYNCCALENDAR, true)) {
//                                getSessionDetails(session_id)
//                            }
//
//                        }
//
//
//                        performSessionAction(
//                            isAccepted,
//                            session_id,
//                            client_id,
//                            dateStr + " @ " + timeStr,
//                            chatMessage
//                        )
//
//
//                    } else {
//                        iosAlertWithCallback(res?.message ?: "",
//                            this@ChatActivity,
//                            object : AlertCallBack {
//                                override fun onClickOk() {
//
//                                }
//                            })
//                    }
//
//                    hideProgress()
//                }
//            })
    }


    fun performSessionAction(
        isSessionAccepted: Boolean,
        sessionId: String,
        clientId: String,
        dateTime: String, chatMessage: ChatMessage
    ) {
        var message = ""
        var fullName = loginUserData.first_name
        if (!loginUserData.last_name.isNullOrEmpty()) {
            fullName = fullName + " " + loginUserData.last_name
        }
        if (isSessionAccepted) {
            message = fullName + " confirmed your session request scheduled on " + dateTime
        } else {
            message = fullName + " rejected your session request scheduled on " + dateTime
        }

        val iOSData: HashMap<String, Any> = HashMap<String, Any>()

        var notificationType = "1"

        if (isSessionAccepted) {
            notificationType = Constants.SessionAccepted.toString()
        } else {
            notificationType = Constants.SessionRejected.toString()
        }

        iOSData.put("trainer_id", loginUserData.id)

        if (push_tokens.size > 0) {
            for (i in 0..push_tokens.size - 1) {
                sentToNotification(
                    message,
                    "",
                    push_tokens[i], iOSData, sessionId, notificationType,
                    loginUserData.id,
                    "chat"

                )
            }
        }
        sendMessage(message, chatMessage)
    }


    fun sendMessage(message: String, chatMessage: ChatMessage) {

        val msg = ChatMessage()
        if (currentChatRoom!!.client_id.isNullOrEmpty() || currentChatRoom!!.uid.isNullOrEmpty()) {
            if (selectedSession != null) {
                msg.to = selectedSession!!.client_id

            } else {
                msg.to = selectedClient!!.user_id

            }


        } else {
            msg.to = loginUserData.id

        }

        if (currentClient != null) {
            msg.to = currentClient.user_id
            //msg.from = loginUserData.id
        }
        msg.from = loginUserData.id


        if (loginUserData.image.isNotEmpty()) {
            currentChatRoom!!.imageUrl = Constants.IMAGE_BASE_URL + loginUserData.image

        }
        try {
            if (currentClient.image.isNotEmpty()) {
                currentChatRoom!!.client_imageUrl = Constants.IMAGE_BASE_URL + currentClient.image

            }
        } catch (e: Exception) {
        }


        val tsLong = System.currentTimeMillis()
        val ts = tsLong.toString()

        msg.message = message
        msg.senderName = loginUserData.first_name + " " + loginUserData.last_name
        msg.messageType = FireUtils.NoAdded
        msg.messageId = System.currentTimeMillis()
        msg.timestamp = Timestamp.now()
        addMsg(msg)
        binding.msgList!!.scrollVerticallyToPosition(msgList!!.size - 1); // yourList is the ArrayList that you are passing to your RecyclerView Adapter.
        val iOSData: HashMap<String, Any> = HashMap<String, Any>()


        var roomID=getRoomId()
        val myMessageRef= Firebase.firestore.collection("${FireUtils.CHAT}/${roomID}/${FireUtils.MESSAGES}").document();
        msg.id=myMessageRef.id.toString()
        if (currentChatRoom!!.roomId.isNullOrEmpty()) {
            setMyCurrentChatRoom(msg, null, myMessageRef)
            chatMessage.messageType = FireUtils.NoAdded

            adapter!!.notifyDataSetChanged()
            FireUtils.updateMessage(
                "${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}/${chatMessage.id}",
                chatMessage
            ) {


            }

        } else {
            updateMyChatGroup(msg, false)
            FireUtils.saveMessageUsingRef(
                myMessageRef,
                msg
            ) {


                chatMessage.messageType = FireUtils.NoAdded

                adapter!!.notifyDataSetChanged()
                FireUtils.updateMessage(
                    "${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}/${chatMessage.id}",
                    chatMessage
                ) {


                }


            }
        }
    }




    fun updateDeleteMessage(chatMessage: ChatMessage) {


        chatMessage.deleted= true
        // adapter!!.notifyDataSetChanged()
        FireUtils.updateDelete(
            "${FireUtils.CHAT}/${currentChatRoom!!.roomId}/${FireUtils.MESSAGES}/${chatMessage.id}",
            chatMessage
        ) {


        }
    }



    fun getSessionDetails(
        session_id: String
    ) {


//        RetrofitClientInstance.getAPI()?.session_details(
//            loginUserData.id,
//            loginUserData.user_type,
//            session_id
//        )
//            ?.enqueue(object : Callback<SingleSessionsResponse> {
//                override fun onFailure(call: Call<SingleSessionsResponse>?, t: Throwable?) {
//
//
//                }
//
//                override fun onResponse(
//                    call: Call<SingleSessionsResponse>?,
//                    response: Response<SingleSessionsResponse>?
//                )  {
//                    Log.e("",""+response);
//
//
//                    var userpojo= response!!.body()
//                    if (Paper.book().read(Constants.SYNCCALENDAR, true)) {
//
//
//                        var map =
//                            java.util.HashMap<String, Long>() //Creating HashMap
//                        try {
//                            map = Paper.book().read(Constants.CALENDARHASHMAP)
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }
//
//                        if (map == null) {
//                            map = HashMap<String, Long>() //Creating HashMap
//                        }
//
//                        try {
//                            if(userpojo!!.data.status==Constants.Auto_Accepted ||  userpojo.data.status==Constants.Accepted
//                                ||  userpojo.data.status==Constants.Completed )
//                            {
//                                addEventToCalendar( userpojo.data.repeat,  userpojo.data.session_end_date,userpojo!!.data.duration,
//                                    (userpojo!!.data.utc.toLong() * 1000),
//                                    "Session with " + userpojo!!.data.first_name+" "+userpojo!!.data.last_name,
//                                    System.currentTimeMillis(),
//                                    userpojo.data.id.toString(),
//                                    map
//                                )
//                            }
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }
//                    }
//
//                }
//            })
    }



    fun startListeningOtherPersonOnlineStatus() {
        // stopListeningOtherPersonOnlineStatus()

        updateMyChatGroupOnOpenChat()


        lastOnlineRef =
            Firebase.database.getReference("users/" + currentClient.user_id!! + "/lastOnline")
        myConnectionsRef =
            Firebase.database.getReference("users/" + currentClient.user_id + "/connections")

        myConnectionsRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.value

                if (value is HashMap<*, *>) {


                    if (value.values.contains("online")) {
                        binding.onlinetext.setText("Online")
                    } else {
                        binding.onlinetext.setText("")

                        lastOnlineRef!!.addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val numChildren = dataSnapshot.getValue()
                                if (numChildren is Double || numChildren is Long) {
                                    if (numChildren is Double) {
                                        updateLastSeen(numChildren.toLong())
                                    } else {
                                        updateLastSeen(numChildren.toString().toLong())
                                    }
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })

                    }
                } else {
                    binding.onlinetext.setText("")

                    lastOnlineRef!!.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val numChildren = dataSnapshot.getValue()
                            if (numChildren is Double || numChildren is Long) {
                                if (numChildren is Double) {
                                    updateLastSeen(numChildren.toLong())
                                } else {
                                    updateLastSeen(numChildren.toString().toLong())
                                }

                            }


                        }

                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
                }

                Log.e("", "" + value);


            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    fun updateLastSeen(time: Long) {
        try {
            if (DateUtils.isToday(time)) {
                // SimpleDateFormat sdf = SimpleDateFormat("h:mm a", Locale.getDefault());
                val formatter: DateFormat = SimpleDateFormat("hh:mm aa")
                val todayTime = formatter.format(Date(time))
                binding.onlinetext.setText("last seen " + todayTime)
                //                ((DateHeaderViewHolder) holder).time.setText("Today");
            } else {
                val formatter: DateFormat = SimpleDateFormat("d MMMM,yyyy")
                val todayTime = formatter.format(Date(time))
                binding.onlinetext.setText("last seen " + todayTime)
            }
        } catch (e: Exception) {

        }
    }


}