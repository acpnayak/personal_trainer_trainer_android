package com.enact.mytrainer.ui.clients

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.ClientsFragmentBinding
import com.enact.mytrainer.databinding.FragmentGymsBinding
import com.enact.mytrainer.model.ClientObj
import com.enact.mytrainer.model.ClientResponse
import com.enact.mytrainer.model.GymsResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.ui.clients.adapter.ClientsAdapter
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClientsFragment : Fragment() {
    private lateinit var viewModel: ClientsViewModel
    private var _binding:ClientsFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var list:ArrayList<String>
    lateinit var loginUserData: LoginUserData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(ClientsViewModel::class.java)
        _binding = ClientsFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        loginUserData = Paper.book().read(Constants.USERDATA)
        fetchAllClients()


        return root
    }



    fun setAdapter(list: ArrayList<ClientObj>) {
        val layoutManager = LinearLayoutManager(context)
        binding.clientList.layoutManager = layoutManager
        val adapter = ClientsAdapter(list, requireContext())
        binding.clientList!!.adapter = adapter
    }



    fun fetchAllClients() {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchClients(loginUserData.id)
            ?.enqueue(object : Callback<ClientResponse> {
                override fun onFailure(call: Call<ClientResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<ClientResponse>?,
                    response: Response<ClientResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity).hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                if (userpojo.data.size > 0) {
                                    binding.labelno.visibility = View.GONE
                                    setAdapter(userpojo.data)

                                } else {
                                    binding.labelno.visibility = View.VISIBLE

                                }


                            } else {
                                (context as MainActivity).singleButtonDialog(
                                    userpojo.message,
                                    "Ok",
                                    {

                                    })
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }




}