package com.enact.mytrainer.ui.addAvailability.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.AvailabilityData
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.GymOffer
import com.google.android.material.button.MaterialButton
import java.lang.Exception

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AvailabilityAdapter(
    var results: ArrayList<AvailabilityData>,
    private val context: Context,
    val listener: ItemListener

) : RecyclerView.Adapter<AvailabilityAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.availability_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        if (results[position].isConver) {
            holder.fromTo.text =
                convert(results[position].from) + " - " + convert(results[position].to)

        } else {
            holder.fromTo.text = results[position].from + " - " + results[position].to

        }
        holder.sessionLength.text = results[position].length+" min"


//        holder.del.setOnClickListener(){
//            listener.onItemClickListener(results[position],1,position)
//        }
//
//        holder.update.setOnClickListener(){
//            listener.onItemClickListener(results[position],2,position)
//        }



    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }

    fun addItem(item: AvailabilityData) {
        results.add(item)
        notifyDataSetChanged()
    }

    fun removeItem(pos:Int) {
        results.removeAt(pos)
        notifyDataSetChanged()
    }

    fun updateItem(item: AvailabilityData,pos:Int) {
        results.removeAt(pos)
        results.add(pos,item)
        notifyDataSetChanged()
    }

    fun getList():ArrayList<AvailabilityData>{
        return results;
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var fromTo: TextView
        var sessionLength: TextView
       // var del: LinearLayout
      //  var update: LinearLayout
        var mainLayout: ConstraintLayout

        init {
            fromTo = itemView.findViewById(R.id.fromTo)
            sessionLength = itemView.findViewById(R.id.val_time)
          //  del = itemView.findViewById(R.id.deletelay)
            //update = itemView.findViewById(R.id.updateLay)
            mainLayout = itemView.findViewById(R.id.mainLayout)
        }
    }

    @Throws(Exception::class)
    fun convert(time: String?): String {
        try {
            val t24 = SimpleDateFormat("HH:mm")
            val t12 = SimpleDateFormat("hh:mm a")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }
    interface ItemListener{
        fun onItemClickListener(data: AvailabilityData,Type:Int,pos:Int)
    }


}