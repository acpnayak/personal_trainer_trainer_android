package com.enact.mytrainer.ui.gyms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GymViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Gym Fragment"
    }
    val text: LiveData<String> = _text
}