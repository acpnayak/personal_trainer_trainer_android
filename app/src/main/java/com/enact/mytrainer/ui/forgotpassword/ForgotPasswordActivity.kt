package com.enact.mytrainer.ui.forgotpassword

import android.os.Bundle
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityForgotpasswordBinding
import com.enact.mytrainer.databinding.ActivityLoginBinding
import com.enact.mytrainer.model.CommonResponse
import com.enact.mytrainer.model.LoginResponse
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity: BaseActivity() {
    private lateinit var binding: ActivityForgotpasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotpasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
       setListeners()
    }

    fun forgotPassCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.resetPass(
                binding.editText.text.toString()
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                singleButtonDialog(userpojo.message, "Ok", {
                                    finish()
                                })
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    fun setListeners(){
        binding.backbtn.setOnClickListener {
            onBackPressed()
        }

        binding.submit.setOnClickListener(){
            if (binding.editText.text.toString().trim().isBlank()) {
                binding.editText.error = getString(R.string.please_enter_email)
                binding.editText.requestFocus()
            } else if (!isValidEmail(binding.editText.text.toString().trim())) {
                binding.editText.error = getString(R.string.please_enter_valid_email)
                binding.editText.requestFocus()
            }
            else{
                if (isNetworkAvailable()){
                    forgotPassCall()
                }
                else{
                    singleButtonDialog(
                        getString(R.string.netError),
                        "Ok", {

                        }
                    )

                }
            }
        }


    }


}