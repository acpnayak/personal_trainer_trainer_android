package com.enact.mytrainer.ui.terms

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.enact.mytrainer.R
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityPaymentHistoryBinding
import com.enact.mytrainer.databinding.ActivityTermsBinding

class TermsActivity : BaseActivity() {
    private lateinit var binding: ActivityTermsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTermsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.webview.settings.javaScriptEnabled=true
        binding.webview.loadUrl("https://www.google.com/")
        setListeners()

    }

    fun setListeners(){
        binding.backbtn.setOnClickListener(){
            finish()
        }
    }

}