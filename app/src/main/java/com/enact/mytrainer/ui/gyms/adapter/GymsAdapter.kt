package com.enact.mytrainer.ui.gyms.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.loadUrl
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.model.*
import com.google.android.material.button.MaterialButton
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GymsAdapter(
    var results: ArrayList<GymObject>,
    private val context: Context

) : RecyclerView.Adapter<GymsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.gyms_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        var loginUserData:LoginUserData
        loginUserData=Paper.book().read(Constants.USERDATA)


        holder.name.text=results[position].name
        holder.location.text=results[position].address
        holder.img.loadUrl(Constants.IMAGE_BASE_URL+results[position].image)

        holder.deleteLay.setOnClickListener(){
            deleteGym(loginUserData.id,results[position].id,position)
        }

        holder.main_layout.setOnClickListener(){

        }

    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }


    fun deleteItem(pos:Int){
        results.removeAt(pos)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var location: TextView
        var img: ImageView
        var deleteLay: LinearLayout
        var main_layout: ConstraintLayout

        init {
            name = itemView.findViewById(R.id.tv_username)
            location = itemView.findViewById(R.id.tvLocation)
            img = itemView.findViewById(R.id.img)
            deleteLay = itemView.findViewById(R.id.deletelay)
            main_layout = itemView.findViewById(R.id.main_layout)
        }
    }


    fun deleteGym(trainerId:String,gymID:String,pod:Int) {
        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.deleteGym(
                trainerId,
                gymID
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    (context as MainActivity). hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity). hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                               deleteItem(pod)

                            } else {
                                (context as MainActivity).singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                context.getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                context.getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }





}