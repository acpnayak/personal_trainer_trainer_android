package com.enact.mytrainer.ui.signup

import android.os.Bundle
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityLoginBinding
import com.enact.mytrainer.databinding.ActivitySignupBinding
import com.enact.mytrainer.model.LoginResponse
import com.enact.mytrainer.ui.trainingoffer.TrainingOfferActivity
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupActivity: BaseActivity() {

    private lateinit var binding: ActivitySignupBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        binding.signupBtn.setOnClickListener(){

            when {
                binding.editText.text.isNullOrBlank() -> {
                    binding.editText.error =getString(R.string.fnamevalidation)
                    binding.editText.requestFocus()
                }
                binding.editLastName.text.isNullOrBlank() -> {
                    binding.editLastName.error =getString(R.string.lnamevalidation)
                    binding.editLastName.requestFocus()

                }
                binding.editEmail.text.isNullOrBlank() -> {
                    binding.editEmail.error =getString(R.string.please_enter_email)
                    binding.editEmail.requestFocus()

                }
                !android.util.Patterns.EMAIL_ADDRESS.matcher(binding.editEmail.text.toString())
                    .matches() -> {
                    binding.editEmail.error =getString(R.string.valid_email)
                    binding.editEmail.requestFocus()

                }
                binding.editPassword.text.isNullOrBlank() -> {
                    binding.editPassword.error =getString(R.string.passwordvalidation)
                    binding.editPassword.requestFocus()

                }
                binding.editPassword.text.toString().length < 6 -> {
                    binding.editPassword.error =getString(R.string.password_length_validation)
                    binding.editPassword.requestFocus()

                }
                binding.editCpass.text.isNullOrBlank() -> {
                    binding.editCpass.error =getString(R.string.confirmpasswordvalidation)
                    binding.editCpass.requestFocus()

                }
                !binding.editPassword.text.toString()
                    .equals(binding.editCpass.text.toString()) -> {
                    binding.editCpass.error =getString(R.string.passwordnotmatch)
                    binding.editCpass.requestFocus()

                }

                else -> {
                    if (isNetworkAvailable()) {
                    registerCall()
                    }
                    else {
                        singleButtonDialog(getString(R.string.netError), "Ok", {})

                    }
                }

            }




        }



        binding.againLogin.setOnClickListener {
            onBackPressed()
        }
    }

    fun registerCall() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.registerUser(
                binding.editText.text.toString(),
                binding.editLastName.text.toString(),
                binding.editEmail.text.toString().trim(),
                binding.editPassword.text.toString(),
                "",
                "0"
            )
            ?.enqueue(object : Callback<LoginResponse> {
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<LoginResponse>?,
                    response: Response<LoginResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                Paper.book().write(Constants.USERDATA,response.body().data)
                                Paper.book().write(Constants.ISLOGIN,true)
                                finishAll(TrainingOfferActivity::class.java)

                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }



}