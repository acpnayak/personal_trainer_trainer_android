package com.enact.mytrainer.ui.punchSessions.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.GymOffer
import com.enact.mytrainer.model.PunchCardHistoryObj
import com.google.android.material.button.MaterialButton

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PunchSessionsAdapter(
    var results: ArrayList<PunchCardHistoryObj>,
    private val context: Context

) : RecyclerView.Adapter<PunchSessionsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.punch_session_adapter,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        holder.name.text=results[position].client_name
        holder.sessions.text=results[position].sessions_available+"/"+results[position].total_sessions
        holder.time.text="Purchased: "+millisToDate(results[position].purchased_on.toLong()*1000)

    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }



    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var name: TextView
        var sessions: TextView
        var time: TextView

        init {
            name = itemView.findViewById(R.id.name)
            sessions = itemView.findViewById(R.id.sessions)
            time = itemView.findViewById(R.id.time)
        }
    }

    fun millisToDate(millis: Long) : String {
        return SimpleDateFormat("dd MMM,yyyy", Locale.US).format(Date(millis))
    }

}