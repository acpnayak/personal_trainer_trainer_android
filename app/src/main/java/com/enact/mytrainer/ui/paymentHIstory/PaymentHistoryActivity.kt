package com.enact.mytrainer.ui.paymentHIstory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityChangePassBinding
import com.enact.mytrainer.databinding.ActivityPaymentHistoryBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.paymentHIstory.adapter.PaymentHistoryAdapter
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentHistoryActivity : BaseActivity() {
    private lateinit var binding: ActivityPaymentHistoryBinding
    lateinit var loginUserData: LoginUserData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentHistoryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        fetchAllData()
        setListeners()

    }

    fun setAdapter(list: ArrayList<SessionData>) {
        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
        val adapter = PaymentHistoryAdapter(list, this)
        binding.rv!!.adapter = adapter
    }

    fun setListeners(){
        binding.backbtn.setOnClickListener(){
            finish()
        }
    }


    fun fetchAllData() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchPaymentHistory(loginUserData.id)
            ?.enqueue(object : Callback<SessionResponse> {
                override fun onFailure(call: Call<SessionResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<SessionResponse>?,
                    response: Response<SessionResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                if (userpojo.data.size > 0) {
                                    binding.nodata.visibility = View.GONE
                                    binding.rv.visibility= View.VISIBLE
                                    setAdapter(addAlphabets(response.body().data))

                                } else {
                                    binding.nodata.visibility = View.VISIBLE
                                    binding.rv.visibility= View.GONE

                                }


                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    internal fun addAlphabets(list: MutableList<SessionData>): ArrayList<SessionData> {
        var i = 0
        val customList = ArrayList<SessionData>()
        val timeObj= TimeObj("","","","","","","","")
        val firstMember = SessionData(time = timeObj,header = list[0].time.month,type = 1)
        customList.add(firstMember)
        i = 0
        while (i < list.size - 1) {
            val teamMember = SessionData(time=timeObj)
            val name1 = list[i].time.month
            val name2 = list[i + 1].time.month
            if (name1.equals(name2)) {
                list[i].type = 2
                customList.add(list[i])
            } else {
                list[i].type = 2
                customList.add(list[i])
                teamMember.header=name2
                teamMember.type = 1
                customList.add(teamMember)
            }
            i++
        }
        list[i].type = 2
        customList.add(list[i])
        return customList
    }


}