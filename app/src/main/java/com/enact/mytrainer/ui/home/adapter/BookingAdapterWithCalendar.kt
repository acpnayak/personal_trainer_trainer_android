package com.enact.mytrainer.ui.home.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enact.mytrainer.R
import com.enact.mytrainer.model.AvailabilityObj
import com.enact.mytrainer.model.GymOffer
import com.enact.mytrainer.model.SessionData
import com.google.android.material.button.MaterialButton
import java.lang.Exception

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class BookingAdapterWithCalendar(
    var results: ArrayList<SessionData>,
    private val context: Context

) : RecyclerView.Adapter<BookingAdapterWithCalendar.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.session_adapter_with_calendar,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        holder.time.text = convert(results[position].time.from)
        holder.name.text = "Training with " + results[position].name
        if (results[position].payment_type.equals("0")) {
            holder.payment_type.text = "Per Session"

        } else if (results[position].payment_type.equals("1")) {
            holder.payment_type.text = "Punch Card"

        } else {
            holder.payment_type.text = "Monthly"

        }
        holder.price.text = "$" + results[position].price+"/hr"

    }

    override fun getItemCount(): Int {
        return if (results == null) 0 else results.size
    }



    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var time: TextView
        var name: TextView
        var location: TextView
        var payment_type: TextView
        var price: TextView

        init {
            time = itemView.findViewById(R.id.time)
            name = itemView.findViewById(R.id.gym_name)
            location = itemView.findViewById(R.id.tvLocation)
            payment_type = itemView.findViewById(R.id.punchcard)
            price = itemView.findViewById(R.id.price)
        }
    }

    @Throws(Exception::class)
    fun convert(time: String?):String {
        try {
            val t24 = SimpleDateFormat("HH:mm")
            val t12 = SimpleDateFormat("hh:mm a")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

}