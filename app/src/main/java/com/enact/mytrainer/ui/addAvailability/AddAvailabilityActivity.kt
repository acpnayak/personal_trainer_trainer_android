package com.enact.mytrainer.ui.addAvailability

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.daysOfWeekFromLocale
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityAddAvailabilityBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.addAvailability.adapter.AvailabilityAdapter
import com.enact.mytrainer.ui.createAvailability.CreateAvailabilityActivity
import com.enact.mytrainer.ui.trainingoffer.adapter.HashTagAdapter
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import io.paperdb.Paper
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.WeekFields
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddAvailabilityActivity : BaseActivity() {
    private lateinit var binding: ActivityAddAvailabilityBinding
    lateinit var loginUserData: LoginUserData
    var adapter :AvailabilityAdapter?=null
    private val titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy")
    private val yearFormatter = DateTimeFormatter.ofPattern("yyyy")
    private val monthFormatter = DateTimeFormatter.ofPattern("MM")
    var nwevents: ArrayList<LocalDate>? = ArrayList<LocalDate>()
    var backupcurrentMonth = YearMonth.now()
    val today = LocalDate.now()
    var selectedDate: LocalDate? = LocalDate.now()
    val daysOfWeek = daysOfWeekFromLocale()
    var showType = 1
    var weekDays: List<List<CalendarDay>>? = null
    var currentMonth = 0
    var currentYear = 0
    var onLaunch = true;

    lateinit var list:ArrayList<AvailabilityObj>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddAvailabilityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData= Paper.book().read(Constants.USERDATA)


        setListeners()



    }

    fun setUpCalendar(binding: ActivityAddAvailabilityBinding) {
        binding.calendar.visibility = View.VISIBLE
        binding.calheader.visibility = View.VISIBLE
        binding.calendar.setHasFixedSize(true)


        class DayViewContainer(view: View) : ViewContainer(view) {

            // Without the kotlin android extensions plugin

            val pp = view.findViewById<RelativeLayout>(R.id.pp)
            val textView = view.findViewById<TextView>(R.id.calendarDayText)
            val greendot = view.findViewById<AppCompatImageView>(R.id.available)
        }

        binding.calendar.dayBinder = object : DayBinder<DayViewContainer> {
            // Called only when a new container is needed.
            override fun create(view: View) = DayViewContainer(view)

            // Called every time we need to reuse a container.
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.textView.text = day.date.dayOfMonth.toString()

                if (nwevents!!.contains(day.date)) {
                      Log.e("YES ADDEd","YES ADDEd")
                }

                if (day.owner == DayOwner.THIS_MONTH) {
                    container.textView.setTextColor(Color.BLACK)
                    if (day.date == selectedDate) {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                            container.textView.setTextColor(Color.WHITE)
                        } else {
//                            container.textView.setTextColor(Color.WHITE)
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar_border)
                        }

                        if (nwevents!!.contains(day.date)) {
//                           container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            container.greendot.visibility = View.GONE
                        }
                    } else {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                        } else
                            container.pp.setBackgroundResource(0)

                        if (nwevents!!.contains(day.date)) {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                                container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                            // container.textView.setTextColor(Color.GRAY)
                                container.greendot.visibility = View.GONE
                        }
                    }
                } else {
                    container.textView.setTextColor(Color.GRAY)
                    container.greendot.visibility = View.GONE
                }
                container.textView.setOnClickListener {
                    selectedDate = day.date
                    binding.calendar!!.notifyCalendarChanged()
                    fetchAllData(
                        backupcurrentMonth.year.toString(),
                        backupcurrentMonth.monthValue.toString(),
                        selectedDate!!.dayOfMonth.toString()
                    )


                }
            }
        }


        val currentMonth1 = YearMonth.now()
        val firstMonth = currentMonth1.minusMonths(10)
        val lastMonth = currentMonth1.plusMonths(10)
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        binding.calendar.setup(firstMonth, lastMonth, firstDayOfWeek)
        binding.calendar.scrollToMonth(currentMonth1)
        binding.monthTv.text = titleFormatter.format(currentMonth1)
        class MonthViewContainer(view: View) : ViewContainer(view) {

        }
        binding.calendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.

            }
        }
        binding.calendar.monthScrollListener = {
            binding.monthTv.text = titleFormatter.format(it.yearMonth)
            backupcurrentMonth = it.yearMonth

            if (!onLaunch) {
                selectedDate = null
            }
            onLaunch = false
            var month = monthFormatter.format(it.yearMonth)
            var year = yearFormatter.format(it.yearMonth)
            fetchAllData(
                backupcurrentMonth.year.toString(),
                backupcurrentMonth.monthValue.toString(),
                ""
            )

        }

        binding. calendar.findFirstVisibleMonth()?.let {
            currentMonth = it.month
            currentYear = it.year
            weekDays = it.weekDays.toList()

        }


    }


    fun setAdapter(list: ArrayList<AvailabilityData>) {


        list!!.forEach {


            try {
                var cal = Calendar.getInstance()
                cal.time = Date(it.from_utc.toLong())

                var sessionMonth = cal.get(Calendar.MONTH) + 1
                var sessionYear = cal.get(Calendar.YEAR)
                var weekDay = cal.get(Calendar.DAY_OF_WEEK)
                var sessionVar = Instant.ofEpochMilli(it!!.from_utc.toLong() * 1000)
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

                var date = LocalDate.of(
                    cal.get(Calendar.YEAR),
                    (cal.get(Calendar.MONTH) + 1),
                    cal.get(
                        Calendar.DAY_OF_MONTH
                    )
                )
                nwevents!!.add(
                    date

                )
            } catch (e: Exception) {
                Log.e("Exception", "" + e.localizedMessage)
            }
        }
        if (nwevents!!.size > 0) {
            try {
                if (binding.calendar != null)
                    binding.calendar!!.notifyCalendarChanged()
            } catch (e: Exception) {
                Log.e("Exception", "" + e.localizedMessage)
            }
        }




        binding.calendar!!.notifyCalendarChanged()


        val layoutManager = LinearLayoutManager(this)
        binding.rv.layoutManager = layoutManager
         adapter = AvailabilityAdapter(list, this, object : AvailabilityAdapter.ItemListener {
            override fun onItemClickListener(
                selectedItem: AvailabilityData,type:Int,pos:Int

            ) {
                if (type==1){
                    deleteAvailability(pos,selectedItem._id)
                }
                else{
                    updateDialog(selectedItem,pos)
                }


            }
        })
        binding.rv!!.adapter = adapter
    }


    fun setListeners(){


        binding.back.setOnClickListener {
            finish()
        }
        binding.button.setOnClickListener(){
            if (selectedDate.toString().equals("null")){
                msgAlert("Please select a date",this)
            }
            else{
                val intent=Intent(this,CreateAvailabilityActivity::class.java)
                intent.putExtra("date",selectedDate.toString())
                startActivity(intent)
            }

        }
    }


    fun fetchAllData(year: String, month: String, day: String) {
        var m = ""
        var d = ""

        if (month.length > 1) {
            m = month
        } else {
            m = "0" + month
        }
        if (day.length == 0) {
            d = ""
        } else if (day.length == 1) {
            d = "0" + day
        } else {
            d = day
        }

       showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchAvailabilities(loginUserData.id, year, m, d)
            ?.enqueue(object : Callback<AvailabilityResponse> {
                override fun onFailure(call: Call<AvailabilityResponse>?, t: Throwable?) {
                   hideProgress()
                }

                override fun onResponse(
                    call: Call<AvailabilityResponse>?,
                    response: Response<AvailabilityResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                if (userpojo.data.size > 0) {
                                    binding.rv.visibility = View.VISIBLE
                                    setAdapter(response.body().data)

                                } else {
                                    binding.rv.visibility = View.GONE

                                }


                            } else {
                               singleButtonDialog(
                                    userpojo.message,
                                    "Ok",
                                    {

                                    })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                          singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                          singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        setUpCalendar(binding)
    }

    fun deleteAvailability(pos:Int,availabilityId:String) {
      showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.deleteAvailability(
                loginUserData.id,
                availabilityId
            )
            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                   hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                adapter!!.removeItem(pos)

                            } else {
                             singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                           singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                           singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                           singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    fun updateDialog(data:AvailabilityData,pos:Int) {
        var focus = "from"
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.create_availability_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val window: Window = dialog.getWindow()!!
        window.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog.setTitle("Title...");
        dialog.setCancelable(true)
        val done = dialog.findViewById(R.id.done) as TextView
        val from = dialog.findViewById(R.id.edtFrom) as TextView
        val to = dialog.findViewById(R.id.edtTo) as TextView
        val session = dialog.findViewById(R.id.edtSessionLength) as EditText
        val timepicker = dialog.findViewById(R.id.timePicker) as TimePicker

        from.setText(convert(data.from))
        to.setText(convert(data.to))
        session.setText(data.length)

        from.setOnClickListener() {
            focus = "from"
            from.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            from.setTextColor(resources.getColor(R.color.red))

            to.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            to.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        to.setOnClickListener() {
            focus = "to"
            to.setBackgroundDrawable(resources.getDrawable(R.drawable.red_empty))
            to.setTextColor(resources.getColor(R.color.red))

            from.setBackgroundDrawable(resources.getDrawable(R.drawable.empty_rectangle))
            from.setTextColor(resources.getColor(R.color.litledarkgrey))

        }

        timepicker.setOnTimeChangedListener { _, hour, minute ->
            var hour = hour
            var am_pm = ""
            // AM_PM decider logic
            when {
                hour == 0 -> {
                    hour += 12
                    am_pm = "AM"
                }
                hour == 12 -> am_pm = "PM"
                hour > 12 -> {
                    hour -= 12
                    am_pm = "PM"
                }
                else -> am_pm = "AM"
            }
            val hr = if (hour < 10) "0" + hour else hour
            val min = if (minute < 10) "0" + minute else minute
            // display format of time
            val msg = "Time is: $hr : $min $am_pm"

            if (focus.equals("from")) {
                from.text = "$hr:$min $am_pm"

            } else {
                to.text = "$hr:$min $am_pm"

            }

        }


        done.setOnClickListener {
            if (from.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter from time", "Ok")

            } else if (to.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to time", "Ok")

            } else if (session.text.toString().equals("")) {
                oneButtonDialog(this, "", "Please enter to session length", "Ok")

            } else {
                updateAvailability(data,from.text.toString(),to.text.toString(),session.text.toString(),pos)
                dialog.dismiss()
            }

        }
        dialog.show()
    }


    @Throws(Exception::class)
    fun convert(time: String?): String {
        try {
            val t24 = SimpleDateFormat("HH:mm")
            val t12 = SimpleDateFormat("hh:mm a")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    @Throws(Exception::class)
    fun convert24(time: String?): String {
        try {
            val t24 = SimpleDateFormat("hh:mm a")
            val t12 = SimpleDateFormat("HH:mm")
            val date = t24.parse(time)
            println(t12.format(date))
            return t12.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    fun updateAvailability(data:AvailabilityData,from:String,to:String,session:String,pos:Int) {

        val jsonObject = JsonObject()
        jsonObject.addProperty("trainer_id", loginUserData.id)
        jsonObject.addProperty("availability_id",data._id)
        val obj = JsonObject()
            obj.addProperty("id", data._id)
            obj.addProperty("from", convert24(from))
            obj.addProperty("to", convert24(to))
            obj.addProperty("day", data.day)
            obj.addProperty("month",data.month)
            obj.addProperty("year", data.year)
            obj.addProperty("to_utc", data.to_utc)
            obj.addProperty("from_utc",data.from_utc)
            obj.addProperty("date_utc", data.date_utc)
            obj.addProperty("length", session)
        jsonObject.addProperty("availability",obj.toString())

        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.updateAvailability(jsonObject)

            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                               adapter!!.updateItem(data,pos)
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }



}