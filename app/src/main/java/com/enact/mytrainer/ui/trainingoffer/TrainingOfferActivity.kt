package com.enact.mytrainer.ui.trainingoffer


import android.os.Bundle
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.base.BaseActivity
import com.enact.mytrainer.databinding.ActivityTrainingOffersBinding
import com.enact.mytrainer.model.*
import com.enact.mytrainer.ui.trainingoffer.adapter.HashTagAdapter
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class TrainingOfferActivity : BaseActivity() {
    lateinit var loginUserData: LoginUserData
    var hashadapter: HashTagAdapter? = null
    private lateinit var binding: ActivityTrainingOffersBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrainingOffersBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        loginUserData = Paper.book().read(Constants.USERDATA)
        fetchAllTrainings()
        binding.skip.setOnClickListener {
            finishAll(MainActivity::class.java)
        }
        binding.submitBtn.setOnClickListener {
            val jsonObject = JsonObject()
            jsonObject.addProperty("user_id", loginUserData.id)
            val trainingArray = JsonArray()
            hashadapter!!.getList().forEach {
                if (it.isSelected) {
                    trainingArray.add(it._id)
                }
            }
            jsonObject.add("training_id", trainingArray)
            if (trainingArray.size() > 0) {
                if (isNetworkAvailable()) {
                    saveTrainings(jsonObject)
                } else {
                    singleButtonDialog(
                        getString(R.string.netError),
                        "Ok", {

                        }
                    )


                }


            } else {
                singleButtonDialog("Please select atleast 1 training", "Ok", {

                })
            }

        }


    }

    fun setHashAdapter(list: ArrayList<TrainingObj>) {
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        binding.gymtypeList.layoutManager = layoutManager
        hashadapter = HashTagAdapter(list, this, false)
        binding.gymtypeList!!.adapter = hashadapter
    }

    fun fetchAllTrainings() {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchTrainings(

            )
            ?.enqueue(object : Callback<TrainingResponse> {
                override fun onFailure(call: Call<TrainingResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<TrainingResponse>?,
                    response: Response<TrainingResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                setHashAdapter(response.body().data)

                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


    fun saveTrainings(jsonObject: JsonObject) {
        showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.saveTrainings(jsonObject)

            ?.enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    hideProgress()
                }

                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    val userpojo = response?.body()
                    hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {
                                finishAll(MainActivity::class.java)
                            } else {
                                singleButtonDialog(userpojo.message, "Ok", {

                                })
                            }

                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    } else {
                        if (userpojo != null) {
                            singleButtonDialog(userpojo.message, "Ok", {

                            })
                        } else {
                            singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok", {

                                }
                            )
                        }
                    }
                }
            })
    }


}