package com.enact.mytrainer.ui.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.enact.mytrainer.MainActivity
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.TrainerUtills.daysOfWeekFromLocale
import com.enact.mytrainer.apiServices.RetrofitClientInstance
import com.enact.mytrainer.databinding.FragmentHomeBinding
import com.enact.mytrainer.model.GymsResponse
import com.enact.mytrainer.model.LoginUserData
import com.enact.mytrainer.model.SessionData
import com.enact.mytrainer.model.SessionResponse
import com.enact.mytrainer.ui.addAvailability.AddAvailabilityActivity
import com.enact.mytrainer.ui.addGym.AddGymActivity
import com.enact.mytrainer.ui.bookinglist.BookingListActivity
import com.enact.mytrainer.ui.home.adapter.BookingAdapterWithCalendar
import com.enact.mytrainer.ui.inbox.adapter.InboxAdapter
import com.enact.mytrainer.ui.searchBookings.SearchBookingsActivity
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import io.paperdb.Paper
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.WeekFields
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    lateinit var list: ArrayList<String>
    lateinit var loginUserData: LoginUserData

    private val titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy")
    private val yearFormatter = DateTimeFormatter.ofPattern("yyyy")
    private val monthFormatter = DateTimeFormatter.ofPattern("MM")
    var nwevents: ArrayList<LocalDate>? = ArrayList<LocalDate>()
    var backupcurrentMonth = YearMonth.now()
    val today = LocalDate.now()
    var selectedDate: LocalDate? = LocalDate.now()
    val daysOfWeek = daysOfWeekFromLocale()
    var showType = 1
    var weekDays: List<List<CalendarDay>>? = null
    var currentMonth = 0
    var currentYear = 0
    var onLaunch = true;

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        loginUserData = Paper.book().read(Constants.USERDATA)


        homeViewModel.text.observe(viewLifecycleOwner, Observer {

        })
        setUpCalendar()

        setListeners()



        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    fun setUpCalendar() {
        binding.calendar.visibility = View.VISIBLE
        binding.calheader.visibility = View.VISIBLE
        binding.calendar.setHasFixedSize(true)


        class DayViewContainer(view: View) : ViewContainer(view) {

            // Without the kotlin android extensions plugin

            val pp = view.findViewById<RelativeLayout>(R.id.pp)
            val textView = view.findViewById<TextView>(R.id.calendarDayText)
            val greendot = view.findViewById<AppCompatImageView>(R.id.available)
        }

        binding.calendar.dayBinder = object : DayBinder<DayViewContainer> {
            // Called only when a new container is needed.
            override fun create(view: View) = DayViewContainer(view)

            // Called every time we need to reuse a container.
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.textView.text = day.date.dayOfMonth.toString()
                if (day.owner == DayOwner.THIS_MONTH) {
                    container.textView.setTextColor(Color.BLACK)
                    if (day.date == selectedDate) {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                            container.textView.setTextColor(Color.WHITE)
                        } else {
//                            container.textView.setTextColor(Color.WHITE)
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar_border)
                        }

                        if (nwevents!!.contains(day.date)) {
//                           container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            container.greendot.visibility = View.GONE
                        }
                    } else {
                        if (day.date == today) {
                            container.pp.setBackgroundResource(R.drawable.btn_bg_orange_for_calendar)
                        } else
                            container.pp.setBackgroundResource(0)

                        if (nwevents!!.contains(day.date)) {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                                container.textView.setTextColor(Color.BLACK)
                            container.greendot.visibility = View.VISIBLE
                        } else {
                            if (day.date == today) {
                                container.textView.setTextColor(Color.WHITE)
                            } else
                            // container.textView.setTextColor(Color.GRAY)
                                container.greendot.visibility = View.GONE
                        }
                    }
                } else {
                    container.textView.setTextColor(Color.GRAY)
                    container.greendot.visibility = View.GONE
                }
                container.textView.setOnClickListener {


//                    sessionArrayAccToDate!!.forEach {
//                        var cal = Calendar.getInstance()
//                        cal.time = Date(it.utc.toLong() * 1000)
//                        if (day.date == it.localUtc)
//                         {
//                            filterArrayAccToDate!!.add(it)
//                         }
//                    }


                    selectedDate = day.date
                    binding.calendar!!.notifyCalendarChanged()
                    fetchAllSessions(
                        backupcurrentMonth.year.toString(),
                        backupcurrentMonth.monthValue.toString(),
                        selectedDate!!.dayOfMonth.toString()
                    )


                }
            }
        }


        val currentMonth1 = YearMonth.now()
        val firstMonth = currentMonth1.minusMonths(10)
        val lastMonth = currentMonth1.plusMonths(10)
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        binding.calendar.setup(firstMonth, lastMonth, firstDayOfWeek)
        binding.calendar.scrollToMonth(currentMonth1)
        binding.monthTv.text = titleFormatter.format(currentMonth1)
        class MonthViewContainer(view: View) : ViewContainer(view) {

        }
        binding.calendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.

            }
        }
        binding.calendar.monthScrollListener = {
            binding.monthTv.text = titleFormatter.format(it.yearMonth)
            backupcurrentMonth = it.yearMonth

            if (!onLaunch) {
                selectedDate = null
            }
            onLaunch = false
            var month = monthFormatter.format(it.yearMonth)
            var year = yearFormatter.format(it.yearMonth)
            fetchAllSessions(
                backupcurrentMonth.year.toString(),
                backupcurrentMonth.monthValue.toString(),
                ""
            )

        }

        binding.calendar.findFirstVisibleMonth()?.let {
            currentMonth = it.month
            currentYear = it.year
            weekDays = it.weekDays.toList()

        }


    }


    fun setAdapter(list: ArrayList<SessionData>) {


        nwevents!!.clear()



        list!!.forEach {


            try {
                var cal = Calendar.getInstance()
                cal.time = Date(it.schedule_time.toLong() * 1000)

                var sessionMonth = cal.get(Calendar.MONTH) + 1
                var sessionYear = cal.get(Calendar.YEAR)
                var weekDay = cal.get(Calendar.DAY_OF_WEEK)
                var sessionVar = Instant.ofEpochMilli(it!!.schedule_time.toLong() * 1000)
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

                var date = LocalDate.of(
                    cal.get(Calendar.YEAR),
                    (cal.get(Calendar.MONTH) + 1),
                    cal.get(
                        Calendar.DAY_OF_MONTH
                    )
                )
                nwevents!!.add(
                    date

                )
            } catch (e: Exception) {
                Log.e("Exception", "" + e.localizedMessage)
            }
        }
        if (nwevents!!.size > 0) {
            try {
                if (binding.calendar != null)
                    binding.calendar!!.notifyCalendarChanged()
            } catch (e: Exception) {
                Log.e("Exception", "" + e.localizedMessage)
            }
        }




        binding.calendar!!.notifyCalendarChanged()



        val layoutManager = LinearLayoutManager(context)
        binding.bookingList.layoutManager = layoutManager
        val adapter = BookingAdapterWithCalendar(list, requireContext())
        binding.bookingList!!.adapter = adapter
    }


    fun setListeners() {
        binding.search.setOnClickListener() {
            val intent = Intent(context, SearchBookingsActivity::class.java)
            startActivity(intent)
        }

        binding.createSession.setOnClickListener() {
            val intent = Intent(context, AddAvailabilityActivity::class.java)
            startActivity(intent)
        }

        binding.calendarView.setOnClickListener() {
            val intent = Intent(context, BookingListActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        //  fetchAllSessions(backupcurrentMonth.year.toString(),backupcurrentMonth.monthValue.toString(),"")

    }


    fun fetchAllSessions(year: String, month: String, day: String) {
        var m = ""
        var d = ""

        if (month.length > 1) {
            m = month
        } else {
            m = "0" + month
        }
        if (day.length == 0) {
            d = ""
        } else if (day.length == 1) {
            d = "0" + day
        } else {
            d = day
        }

        (context as MainActivity).showProgressDialog()
        RetrofitClientInstance.getAPI()
            ?.fetchDashboardSessions(loginUserData.id, year, m, d)
            ?.enqueue(object : Callback<SessionResponse> {
                override fun onFailure(call: Call<SessionResponse>?, t: Throwable?) {
                    (context as MainActivity).hideProgress()
                }

                override fun onResponse(
                    call: Call<SessionResponse>?,
                    response: Response<SessionResponse>?
                ) {
                    val userpojo = response?.body()
                    (context as MainActivity).hideProgress()
                    if (response != null && response.body()?.success!!) {
                        if (userpojo != null) {

                            if (userpojo.success) {

                                if (userpojo.data.size > 0) {
                                    binding.bookingList.visibility = View.VISIBLE
                                    setAdapter(response.body().data)

                                } else {
                                    binding.bookingList.visibility = View.GONE

                                }


                            } else {
                                (context as MainActivity).singleButtonDialog(
                                    userpojo.message,
                                    "Ok"
                                ) {

                                }
                            }

                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    } else {
                        if (userpojo != null) {
                            (context as MainActivity).singleButtonDialog(userpojo.message, "Ok") {

                            }
                        } else {
                            (context as MainActivity).singleButtonDialog(
                                getString(R.string.pleasetryagain),
                                "Ok"
                            ) {

                            }
                        }
                    }
                }
            })
    }


}