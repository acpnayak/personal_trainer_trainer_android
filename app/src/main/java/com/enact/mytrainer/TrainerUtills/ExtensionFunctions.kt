package com.enact.mytrainer.TrainerUtills

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.enact.mytrainer.R
import org.threeten.bp.DayOfWeek
import org.threeten.bp.temporal.WeekFields
import java.util.*

fun daysOfWeekFromLocale(): Array<DayOfWeek> {
    val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
    var daysOfWeek = DayOfWeek.values()
    // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
    if (firstDayOfWeek != DayOfWeek.MONDAY) {
        val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
        val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
        daysOfWeek = rhs + lhs
    }
    return daysOfWeek
}

fun ImageView.loadUrl(url:String){
    Glide.with(context).load(url).placeholder(R.color.lightgrey).error(R.color.lightgrey).into(this)
}


fun ImageView.setImage(url:String){
    Glide.with(context).load(url).placeholder(R.color.lightgrey).error(R.color.lightgrey).into(this)
}
fun View.makeInvisible()
{
    this.visibility= View.GONE
}
fun View.makeInvisibleNotGone()
{
    this.visibility= View.INVISIBLE
}

fun View.makeVisible()
{
    this.visibility= View.VISIBLE
}


