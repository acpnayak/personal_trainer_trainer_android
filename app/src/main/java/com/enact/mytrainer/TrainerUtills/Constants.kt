package com.enact.mytrainer.TrainerUtills

object Constants {

    const val USERID="userid"
    const val ISLOGIN="islogin"
    const val USERDATA="userdata"

    const val fromDashBoard="fromDashBoard"
    const val SYNCCALENDAR="synccalendar"
    const val CALENDARHASHMAP="calendarhashmap"
    // rememmber me
    const val REMEMBER_NAME="remembername"
    const val REMEMBER_PASS="rememberpass"

    const val selectedSession="SELECTEDSESSION"
    const val selectedClient="SELECTEDCLIENT"
    const val googlekey = "AIzaSyDKsePyE8iV4NzmUr6J4NbYIDFGdoXQTAY"

    const val IMAGE_BASE_URL = "http://54.183.206.219/uploads/images/"

    // search
    const val SEARCH_ADDRESS="searchaddress"
    const val SEARCH_LAT="searchlat"
    const val SEARCH_LNG="searchlng"
    const val SEARCH_CAT="searchcat"
    const val SEARCH_RADIUS="searchradius"

    const val ISHITUPDATEREQUEST="ishitupdaterequest"

    const val ISNUMBERVERIFIED="isnumberverified"

    const val OTHERUSERID="otheruserid"



    const val CHAT_ROOM="Chat"
    const val MESSAGES="Messages"


    const val CHAT_ROOM_SCREEN="chatroomscreen"
    const val CHATSCREENUSERID="chatscreenuserid"



    const val OTHERUSERNAME="otherusername"
    const val OTHERUSERIMAGE="otheruserimage"
    const val CONNECTID="connectid"
    const val ORDERID="orderid"
    const val TRANSACTIONID="transactionid"
    const val DRIVERID="driverid"
    const val OTHERTOKEN="othertoken"


    const val ME=0
    const val OTHER=1
    const val STRIPE_CHARGE_API = "https://api.stripe.com/v1/charges"
    const val StripeCharge : String = "https://api.stripe.com/"
    const  val StripeChargeTransferProcess : String = "https://connect.stripe.com/"

    const val STRIPE_SECRET_KEY : String= "sk_test_1tqRe2GJZqKbCChbIZvB4GKs"
    const  val SECRET_KEY_PUBLISHER : String = "pk_test_O1muKEXXSGWqCtGIWmwuhbJC"


    const val None =0
    const val SessionBooked=1
    const val SessionAccepted=2
    const val SessionRejected=3
    const val SessionCancelled=4
    const val ChatMessage=5


    const val Online = "online"
    const val Offile = "offilne"
    const val Away   = "away"

    const val Pending =0
    const val Accepted=1
    const val Auto_Accepted=2
    const val Cancelled_By_Trainer=3
    const val Rejected=4
    const val Cancelled_By_Client=5
    const val Completed=6

}