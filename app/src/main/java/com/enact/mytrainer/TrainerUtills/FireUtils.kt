package com.enact.mytrainer.TrainerUtills

import android.net.Uri
import android.os.Handler
import android.util.Log
import com.enact.mytrainer.model.chat.ChatMessage
import com.enact.mytrainer.model.chat.ChatModel

import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.Timestamp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


object FireUtils {
    private const val TAG="Fireutils"
    public const val PAYPAL_TOKEN: String = "sandbox_4x7wdqmx_8vth6dk6nd73mq4v"
    public const val STRIPE_PUBLIC_KEY_TEST: String = "pk_test_O1muKEXXSGWqCtGIWmwuhbJC"
    public const val STRIPE_SECRECT_KEY_TEST: String = "sk_test_1tqRe2GJZqKbCChbIZvB4GKs"
    private const val USER: String = "USER"
    private const val USER_ID: String = "USER_ID"
    public const val CHAT: String = "Chat"
    public const val Text: String = "Text"
    public const val Image: String = "image"
    public const val NoAdded: String = "NoAdded"
    public const val SessionRequest: String = "SessionRequest"




    public const val MESSAGES: String = "Messages"
     public const val FIREBASE_API_KEY: String = "AIzaSyAHEgWdvBNtC1JIMmbyJBweUGfODFq2Tn0"





    private var storage: FirebaseStorage? = null
    var refrance: ArrayList<ListenerRegistration>? = ArrayList()

    init {

    }





    val db= Firebase.firestore
    fun getDataUsingCollectionName(collectionname:String, callback : (Any)-> Unit)
    {
        db.collection(collectionname).orderBy("timestamp", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { result ->
//
                callback.invoke(result)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }

    fun getDataUsingCollectionNameONlyOneChat(collectionname:String,sessionID:String ,callback : (Any)-> Unit)
    {
        db.collection(collectionname)
            .whereEqualTo("sessionId",sessionID)
            .get()

            .addOnSuccessListener { result ->
//
                callback.invoke(result)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }

    fun getDataUsingQuery(collectionname:String,key:String,value:String, callback : (Any)-> Unit)
    {
        db.collection(collectionname)
            .whereEqualTo(key, value)
            .get()
            .addOnSuccessListener { result ->

                callback.invoke(result)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }
    fun getChatRoomAccToID(collectionname:String,groupID: String,callback : (Any)-> Unit)
    {
        db.collection(collectionname).document(groupID)
            .get()
            .addOnSuccessListener { result ->
                //

                callback.invoke(result)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }


    // Save And Update Message
    fun saveMessageIntoFireBase(collectionname:String, msg: ChatMessage, callback : (Any)-> Unit)
    {
        msg.deleted = false
        msg.localImage=""

        db.collection(collectionname).add(msg)
            .addOnSuccessListener { result ->
                msg.id = result.id

//                result.set(msg).addOnCompleteListener {
//                    callback.invoke(result)
//                }
                var col= collectionname+"/${result.id}"

                updateMesageID(
                    col,
                    msg
                ) {


                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }




    // Save And Update Message
    fun saveMessageUsingRef(collectionname:DocumentReference, msg:ChatMessage, callback : (Any)-> Unit)
    {
       msg.deleted = false
       msg.localImage=""
       var hashMap= HashMap<String,Any>()
//
//        var type: String=""
//        var roomId: String=""
//        var id: String =""
//        var messageId: Long = Long.MIN_VALUE
//        var from: String =""
//        var to: String =""
//        var message: String =""
//        var messageType: String =""// MessageType = MessageType.Text
//        var timestamp = Timestamp.now()
//        var senderName: String =""
//        var sessionId:String=""
//        var deleted:Boolean = false
//
//        var sessionStartTime=  Timestamp.now()
//
//        /// Only for local use: UIImage instance for Image message
//        var localImage: String =""
//

        hashMap["type"] = msg.type
        hashMap["roomId"] = msg.roomId
        hashMap["id"] = msg.id
        hashMap["messageId"] = msg.messageId
        hashMap["from"] = msg.from
        hashMap["to"] = msg.to
        hashMap["message"] = msg.message

        hashMap["messageType"] = msg.messageType
        hashMap["timestamp"] = msg.timestamp
        hashMap["senderName"] = msg.senderName
        hashMap["sessionId"] = msg.sessionId
        hashMap["deleted"] = msg.deleted
        hashMap["sessionStartTime"] = msg.sessionStartTime



        collectionname.set(hashMap)
            .addOnSuccessListener { result ->
                Log.w(TAG, "Error getting documents.")
                callback.invoke("result")
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
                callback.invoke(exception)
            }
    }
    fun updateMesageID(collectionname:String, msg:ChatMessage, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        val data = HashMap<String, Any>()
        data.put("id",msg.id)
        data.put("timestamp",msg.timestamp)
        Firebase.firestore
            //.collection(collectionname)
            .document(collectionname)
            .update(data).addOnFailureListener {
                Log.e("",""+it.localizedMessage)

                saveMessageIntoFireBase(collectionname, msg)
                {

                }
            }
    }

    fun updateMessage(collectionname:String, msg:ChatMessage, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        val data = HashMap<String, Any>()
        data.put("messageType",msg.messageType)
        data.put("timestamp",msg.timestamp)
        Firebase.firestore
            //.collection(collectionname)
            .document(collectionname)
            .update(data).addOnFailureListener {
                Log.e("",""+it.localizedMessage)

                saveMessageIntoFireBase(collectionname, msg)
                {

                }
            }
    }

    fun updateDelete(collectionname:String, msg:ChatMessage, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        val data = HashMap<String, Any>()
        data.put("deleted",true)
        data.put("deleteMsgTimestamp",Timestamp.now())
        Firebase.firestore
            //.collection(collectionname)
            .document(collectionname)
            .update(data).addOnFailureListener {
                Log.e("",""+it.localizedMessage)

                saveMessageIntoFireBase(collectionname, msg)
                {

                }
            }
    }


    fun updateMessageGroup(collectionname:String, msg: ChatModel, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        val data = HashMap<String, Any>()
        data.put("message",msg.message)
        data.put("timestamp",msg.timestamp)

        data.put("imageUrl",msg.imageUrl)
        data.put("client_imageUrl",msg.client_imageUrl)

        Firebase.firestore
            .collection(CHAT)
            .document(collectionname)
            .update(data).addOnSuccessListener {

            }.addOnFailureListener {
                saveMessageGroup(
                    msg.roomId,
                    msg!!
                )
                {

                }
            }
    }

    fun updateDeleteMessageGroup(collectionname:String, msg:ChatModel, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        val data = HashMap<String, Any>()
        data.put("userLastDeletedMsgId",msg.userLastDeletedMsgId)

        Firebase.firestore
            .collection(CHAT)
            .document(collectionname)
            .update(data)



        Handler().postDelayed({
            addEmptyMessaeg(collectionname,msg)
        }, 1000)
    }

    fun addEmptyMessaeg(collectionname:String, msg:ChatModel)
    {
        val data = HashMap<String, Any>()
        data.put("message","")

        Firebase.firestore
            .collection(CHAT)
            .document(collectionname)
            .update(data)
    }

    fun saveMessageGroup(collectionname:String, msg: ChatModel, callback : (Any)-> Unit)
    {

        // var chats: Map<String,Any>? = Map<String, Any>
        Firebase.firestore
            .collection(CHAT)
            .document(collectionname)
            .set(msg).addOnCompleteListener {
                callback.invoke(it)
                Log.e("",""+it);
            }



    }

    //    fileprivate func updateGroup(params: [String: Any]) {
//
//        kFirChatRef.document(self.chatRef.documentID).updateData(params) { (error) in
//                if let _error = error, (_error as NSError).code == 5 {
//            var newParams = self.chatGroup.getDictionary()
//            for (key, value) in params {
//            newParams[key] = value
//        }
//            kFirChatRef.document(self.chatRef.documentID).setData(newParams)
//        }
//        }
//
//
//    }
    private fun inilizefirestorestorage() {
        storage =
            FirebaseStorage.getInstance()
    }

    fun uploadImgaeOnFireStrore(
        message: ChatMessage?,
        selectedUri: Uri?,
        collectionname:DocumentReference?
        , callback : (Any)-> Unit ) {
        if (storage == null) {
            inilizefirestorestorage()
        }
        val storageReference: StorageReference =
            storage!!.getReferenceFromUrl(
                "gs://personaltrainer-ff5f8.appspot.com/"
            ).child("Images").child(System.currentTimeMillis().toString())
        val uploadTask: UploadTask = storageReference.putFile(selectedUri!!)
        uploadTask.addOnSuccessListener(OnSuccessListener<Any> { taskSnapshot ->

            getImageFromFirebase(message, storageReference, collectionname,callback)
        }).addOnFailureListener(OnFailureListener { e -> Log.e("failure ", "print$e") })
    }


    private fun getImageFromFirebase(
        message: ChatMessage?,
        reference: StorageReference,
        collectionname:DocumentReference?
        , callback : (Any)-> Unit
    ) {
        reference.downloadUrl
            .addOnSuccessListener { uri ->
                val fileUrl = uri.toString()
                message!!.message=fileUrl
                if (collectionname != null) {
                    saveMessageUsingRef(collectionname, message)
                    {
                        callback.invoke(it)
                    }
                }
            }.addOnFailureListener { }
    }





}

