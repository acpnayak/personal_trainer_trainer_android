package com.enact.mytrainer.base


import android.app.Application
import com.enact.mytrainer.R
import com.jakewharton.threetenabp.AndroidThreeTen
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import io.paperdb.Paper


class AppClass :Application() {

    override fun onCreate() {
        super.onCreate()
        Paper.init(this)
        AndroidThreeTen.init(this)

        //  ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("font/FontsFree-Net-SFProDisplay-Regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

    }



}