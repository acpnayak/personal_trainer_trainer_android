package com.enact.mytrainer.base

import android.app.Activity
import android.app.Dialog
import android.content.*
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.enact.mytrainer.R
import com.enact.mytrainer.TrainerUtills.Constants
import com.enact.mytrainer.apiServices.ApiInterface
import com.enact.mytrainer.model.SessionData
import com.enact.mytrainer.model.chat.Message
import com.enact.mytrainer.model.clientData

import com.gdacciaro.iOSDialog.iOSDialogBuilder
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import io.paperdb.Paper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import spencerstudios.com.bungeelib.Bungee
import java.util.*


open class BaseActivity : AppCompatActivity() {
    lateinit var context: Context
    var dialog: Dialog? = null
    var progressBar: Dialog? = null

    lateinit var builder: AlertDialog.Builder
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        supportActionBar?.hide()

    }





    fun twoButtonDialog(
        context: Context,
        title: String,
        subtitle: String,
        firstBtnText: String,
        secondBtnText: String
        , button1Click: () -> Unit
        , button2Click: () -> Unit
    ){
        iOSDialogBuilder(context)
            .setTitle(title)
            .setSubtitle(subtitle)
            .setBoldPositiveLabel(true)
            .setCancelable(false)
            .setPositiveListener(
                firstBtnText
            ) { dialog ->

                button1Click.invoke()
                dialog.dismiss()
            }
            .setNegativeListener(
                secondBtnText
            ) {
                    dialog -> dialog.dismiss()
                button2Click.invoke()
            }
            .build().show()
    }

    fun oneButtonDialog(context: Context, title: String, subtitle: String, firstBtnText: String){
        iOSDialogBuilder(context)
            .setTitle(title)
            .setSubtitle(subtitle)
            .setBoldPositiveLabel(true)
            .setCancelable(false)
            .setPositiveListener(
                firstBtnText
            ) { dialog ->
                //Toast.makeText(context, "Clicked!", Toast.LENGTH_LONG).show()
                dialog.dismiss()
            }
            .build().show()
    }
      fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun oneButtonDialog(context: Context, title: String, subtitle: String, firstBtnText: String
                        , button1Click: () -> Unit){
        iOSDialogBuilder(context)
            .setTitle(title)
            .setSubtitle(subtitle)
            .setBoldPositiveLabel(true)
            .setCancelable(false)
            .setPositiveListener(
                firstBtnText
            ) { dialog ->
                button1Click.invoke()
                dialog.dismiss()
            }
            .build().show()
    }






    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase?.let { ViewPumpContextWrapper.wrap(it) });
    }




    open fun goToNextScreen(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        startActivity(intent)
        Bungee.slideLeft(context);
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(context); //fire the slide left animation
    }



    interface AlertCallBack {
        fun onClickOk()
    }




    fun msgAlert(message: String, context: Context) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.ios_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false)
        val ok = dialog.findViewById(R.id.ios_ok) as TextView
        val text = dialog.findViewById(R.id.ios_text) as TextView
        text.text = message

        ok.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showAlertDialog(message: String, title: String? = "Alert!") {
        val dialogBuilder = AlertDialog.Builder(context)

        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(title)
        // show alert dialog
        if (!(context as Activity).isFinishing()) {
            alert.show()
        }

    }

    fun iosAlert(message: String, context: Context) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.ios_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Title...");
        dialog.setCancelable(false)
        val ok = dialog.findViewById(R.id.ios_ok) as TextView
        val text = dialog.findViewById(R.id.ios_text) as TextView
        text.text = message

        ok.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun iosAlertWithCallback(message: String, context: Context, alertCallBack: AlertCallBack) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.ios_dialog);
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Alert!");
        dialog.setCancelable(false)
        val ok = dialog.findViewById(R.id.ios_ok) as TextView
        val text = dialog.findViewById(R.id.ios_text) as TextView
        text.text = message

        ok.setOnClickListener {
            alertCallBack.onClickOk()
            dialog.dismiss()


        }
        dialog.show()
    }


    fun showAlertDialogWithCallBack(message: String, alertCallBack: () -> Unit) {
        val dialogBuilder = AlertDialog.Builder(context)

        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
                alertCallBack

            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Alert!")
        // show alert dialog
        if (!(context as Activity).isFinishing()) {
            alert.show()
        }

    }


    fun singleButtonDialog(msg: String?, button1Text: String = "OK", button1Click: () -> Unit) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.ios_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val text =
            dialog.findViewById<View>(R.id.ios_text) as TextView
        text.text = msg

        val button1 =
            dialog.findViewById<View>(R.id.ios_ok) as TextView

        button1.text = button1Text
        button1.setOnClickListener {
            button1Click()
            dialog.dismiss()

        }
        dialog.show()
    }


    fun doubleButtonDialog(
        msg: String?,
        button1Text: String = "Ok",
        button2Text: String = "Cancel",
        button1Click: () -> Unit={
            dialog!!.dismiss()
        },
        button2Click: () -> Unit
    ) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.ios_logout_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val text =
            dialog.findViewById<View>(R.id.ios_text_logout) as TextView
        text.text = msg
        val button2 =
            dialog.findViewById<View>(R.id.ios_cancel) as TextView
        button2.text = button2Text
        button2.setOnClickListener {
            button2Click()
            dialog.dismiss()

        }
        val button1 =
            dialog.findViewById<View>(R.id.ios_logout) as TextView

        button1.text = button1Text
        button1.setOnClickListener {
            button1Click()
            dialog.dismiss()

        }
        if (!this.isFinishing) dialog.show()
    }




    fun showProgressDialog() {

        if (dialog?.isShowing ?: false) {
            dialog?.dismiss()
        }
        dialog = Dialog(context)
        val inflate = LayoutInflater.from(context).inflate(R.layout.custom_progress, null)
        dialog?.setContentView(inflate)
        dialog?.setCancelable(false)
        dialog?.window!!.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT)
        )
        dialog?.show()
        // CallProgressWheel.showLoadingDialog(context)
    }


    fun hideProgress() {
        try {
            if (dialog != null && dialog?.isShowing ?: false) {
                dialog?.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun hideKeyboard(applicationContext: Context, mView: View?) {
        if (mView != null) {
            val imm =
                applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(mView.windowToken, 0)
        }
    }

    override fun onDestroy() {
        hideProgress()
        super.onDestroy()
    }

    open fun addActivityToStack(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }

    open fun addActivityToStackWithForResult(otherActivityClass: Class<*>?, intentCode: Int) {
        val intent = Intent(applicationContext, otherActivityClass)
        val bundle = Bundle()
        bundle.putBoolean(Constants.fromDashBoard, true)
        intent.putExtras(bundle)

        startActivityForResult(intent, intentCode)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }


    open fun addActivityToStackWithForResultSession(
        otherActivityClass: Class<*>?,
        intentCode: Int
    ) {
        val intent = Intent(applicationContext, otherActivityClass)
        val bundle = Bundle()
        bundle.putBoolean(Constants.fromDashBoard, true)
        intent.putExtras(bundle)
        startActivityForResult(intent, intentCode)
        overridePendingTransition(R.anim.slide_updetails, R.anim.stay);


    }


    open fun addActivityToStackWithClient(otherActivityClass: Class<*>?, selected: clientData) {
        val intent = Intent(applicationContext, otherActivityClass)
        val bundle = Bundle()
        bundle.putSerializable(Constants.selectedClient, selected)
        intent.putExtras(bundle)


        startActivityForResult(intent, 200)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }


    open fun addActivityToStackWithSession(otherActivityClass: Class<*>?, selected: SessionData) {
        val intent = Intent(applicationContext, otherActivityClass)
        //intent.putExtra(Constants.selectedClient, selected)

        val bundle = Bundle()
        bundle.putSerializable(Constants.selectedSession, selected)
        intent.putExtras(bundle)
        startActivityForResult(intent, 200)
        overridePendingTransition(R.anim.slide_updetails, R.anim.stay);

    }

    open fun addActivityToStackWithSessionWithstart(
        otherActivityClass: Class<*>?,
        selected: clientData,
        intentvalue: Int
    ) {
        val intent = Intent(applicationContext, otherActivityClass)
        //intent.putExtra(Constants.selectedClient, selected)

        val bundle = Bundle()
        bundle.putSerializable(Constants.selectedClient, selected)
        intent.putExtras(bundle)


        startActivityForResult(intent, intentvalue)
        overridePendingTransition(R.anim.slide_updetails, R.anim.stay);

        // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }

    open fun addActivityToStackbyRemovingall(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        // intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }

    open fun startNextActivityAnimation(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)

    }

    open fun startPreviousActivity(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }


    open fun finishAll(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    open fun addOrReplaceFragmentName(fragmentName: Fragment, container: Int) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            .replace(container, fragmentName)
            .addToBackStack(null)
            .commit()


    }

    fun isNetworkAvailable(): Boolean {
        try {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
            return if (connectivityManager is ConnectivityManager) {
                val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                networkInfo?.isConnected ?: false
            } else false
        } catch (e: Exception) {
            return false
        }
    }

    open fun replaceFrag(fragmentName: Fragment, container: Int) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
            .replace(container, fragmentName)
            .commit()


    }


    open fun addActivityToStackWithClientNEw(otherActivityClass: Class<*>?, selected: clientData) {
        val intent = Intent(applicationContext, otherActivityClass)
        val bundle = Bundle()
        bundle.putSerializable(Constants.selectedClient, selected)
        intent.putExtras(bundle)


        startActivityForResult(intent, 201)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

    }


    open fun addOFirstFragment(fragmentName: Fragment, container: Int) {
        supportFragmentManager
            .beginTransaction()
            .replace(container, fragmentName)
            .addToBackStack(null)
            .commit()
    }




    fun sentToNotification(
        body: String,
        title: String,
        token: String,
        customValue: HashMap<String, Any>,
        id: String,
        notificationType:String,
        sender_id:String="",
        type:String=""

    ) {
        val to = token
        val collapseKey = ""



        //var androidData = self.getDictionary() ?? [:]


        val iOSData: HashMap<String, Any>? = HashMap<String, Any>()
        val androidData: HashMap<String, Any>? = HashMap<String, Any>()


        iOSData!!.put("body", body.toString())
        iOSData!!.put("type", notificationType)
        iOSData!!.put("id", id)
        iOSData!!.put("badge", "1")

        iOSData!!.put("content_available", true)
        iOSData!!.put("sound", "")

        try {
            iOSData.putAll(customValue)
        } catch (e: Exception) {
        }


        androidData!!.put("body", body.toString())
        androidData!!.put("title", title)
        androidData!!.put("sender_id", sender_id)
        androidData!!.put("type", type)
        androidData!!.put("session_id","")

        try {
            androidData!!.putAll(customValue)
        } catch (e: Exception) {
        }

        val notificationTask = Message(to, collapseKey, iOSData, androidData)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://fcm.googleapis.com/") //url of FCM message server
            .addConverterFactory(GsonConverterFactory.create()) //use for convert JSON file into object
            .build()
        val api: ApiInterface = retrofit.create(ApiInterface::class.java)
        val call: Call<Message> = api.sendMessage(
            "key=AAAAafwXRxw:APA91bHJuzbIdl0a931E7Qgv5l9PlgMFVZhic7IdCnTmjEhwrb4Jlp4lAQQ9ZaDd3vjkOL_WgMcNaYOBb7GeqLcdTZm25W0V--qKAW1eBv1FBDyq1ruHlAo2cjeVtz5R_bzsatyQWrQS",
            notificationTask
        )
        call.enqueue(object : Callback<Message?> {
            override fun onFailure(call: Call<Message?>?, t: Throwable) {
                Log.e("TAG", t.message.toString())
            }

            override fun onResponse(call: Call<Message?>, response: Response<Message?>) {
                Log.e("TAG", "")
            }
        })
    }

    fun addEventToCalendar(repeatMode:String, sessionEndTime:String, duration:String ,start: Long, title: String, id: Long,session_id:String , map:java.util.HashMap<String, Long>) {
        val cr: ContentResolver = getContentResolver()
        val durationInt = duration.toInt()
        val eventValues = ContentValues()


        var FREQ=""
        if(repeatMode=="0")
        {
            FREQ=  "FREQ=DAILY";
        }
        if(repeatMode=="1")
        {
            FREQ=  "FREQ=WEEKLY";
        }
        if(repeatMode=="2")
        {
            FREQ=  "FREQ=MONTHLY";
        }

        eventValues.put(CalendarContract.Events._ID,id);
        eventValues.put(CalendarContract.Events.TITLE, title)
        eventValues.put(CalendarContract.Events.EVENT_LOCATION, "HOURFULLAPPTRAINER")
        eventValues.put(CalendarContract.Events.DTSTART, start)

        eventValues.put(CalendarContract.Events.DTEND, start + (durationInt * 60 * 1000))
        eventValues.put(CalendarContract.Events.CALENDAR_ID, "1") //Defaul calendar
        eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.SHORT)


        if(!sessionEndTime.isNullOrEmpty() && sessionEndTime.toLong()>0 )
        {
            eventValues.put(
                CalendarContract.Events.RRULE,
                FREQ + ";UNTIL=" + sessionEndTime.toLong()*1000);
        }



        cr.insert(CalendarContract.Events.CONTENT_URI, eventValues)





        map[session_id] = id //Put elements in Map
        Paper.book().write(Constants.CALENDARHASHMAP,map)

    }




    open fun deleteLast30days(context: Context, startTime: Calendar, endTime: Calendar)
    {
        try {
            val projection = arrayOf(
                "_id"
            )
            val selection =
                "(( " + CalendarContract.Events.DTSTART + " >= " + startTime.timeInMillis + " ) AND (eventLocation == ? ) AND  ( " + CalendarContract.Events.DTSTART + " <= " + endTime.timeInMillis + " ))"

            val selectionsArgs = arrayOf("HOURFULLAPPTRAINER")
            val cursor = this.baseContext.contentResolver.query(
                CalendarContract.Events.CONTENT_URI,
                projection,
                selection,
                selectionsArgs,
                null
            )
            cursor!!.moveToFirst()
            // fetching calendars name
            val CNames = arrayOfNulls<String>(cursor!!.getCount())
            for (i in CNames.indices) {
                val cr = context.contentResolver
                val eventUri: Uri = ContentUris.withAppendedId(
                    CalendarContract.Events.CONTENT_URI,
                    cursor!!.getString(0)!!.toLong()
                )
                cr.delete(eventUri, null, null)
                cursor.moveToNext()
            }


        } catch (e: Exception) {
            Log.e("",""+e.localizedMessage);

        }
    }

    open fun deleteAllEvent(context: Context) {

        try {
            val selectionClause = "(eventLocation == ?)"
            val selectionsArgs = arrayOf("HOURFULLAPPTRAINER")

            val cursor: Cursor? = context.contentResolver
                .query(
                    Uri.parse("content://com.android.calendar/events"), arrayOf(
                        "calendar_id", "title", "_id",
                        "dtstart", "dtend", "eventLocation"
                    ), selectionClause,
                    selectionsArgs, null
                )
            cursor!!.moveToFirst()
            // fetching calendars name
            val CNames = arrayOfNulls<String>(cursor!!.getCount())
            for (i in CNames.indices) {
                val cr = context.contentResolver
                val eventUri: Uri = ContentUris.withAppendedId(
                    CalendarContract.Events.CONTENT_URI,
                    cursor!!.getString(2)!!.toLong()
                )
                cr.delete(eventUri, null, null)
                cursor.moveToNext()
            }

            var map = java.util.HashMap<String, Long>() //Creating HashMap
            Paper.book().write(Constants.CALENDARHASHMAP,map)
        } catch (e: Exception) {
            Log.e("",""+e.localizedMessage)
        }



    }

    open fun updateEvent(repeatMode:String, sessionEndTime:String,duration: String, start: Long, eventID: String) {


        var durationInt =  duration.toInt()
        var map = HashMap<String, Long>() //Creating HashMap
        map= Paper.book().read(Constants.CALENDARHASHMAP)
        val event_id=map.get(eventID)

        val cr = context.contentResolver
        val eventUri: Uri = ContentUris.withAppendedId(
            CalendarContract.Events.CONTENT_URI,
            event_id!!.toLong()
        )

        val event = ContentValues()
        event.put(CalendarContract.Events.DTSTART, start)



        var FREQ=""
        if(repeatMode=="0")
        {
            FREQ=  "FREQ=DAILY";
        }
        if(repeatMode=="1")
        {
            FREQ=  "FREQ=WEEKLY";
        }
        if(repeatMode=="2")
        {
            FREQ=  "FREQ=MONTHLY";
        }
        if(!sessionEndTime.isNullOrEmpty() && sessionEndTime.toLong()>0 )
        {
            event.put(
                CalendarContract.Events.RRULE,
                FREQ + ";UNTIL=" + sessionEndTime.toLong()*1000);
        }



        event.put(CalendarContract.Events.DTEND, start + (durationInt * 60 * 1000))
        cr.update(eventUri, event, null, null)
    }

    open fun deleteEvent(eventID: String) {

        var map = HashMap<String, Long>() //Creating HashMap
        map= Paper.book().read(Constants.CALENDARHASHMAP)
        val event_id=map.get(eventID)

        val cr = context.contentResolver
        val eventUri: Uri = ContentUris.withAppendedId(
            CalendarContract.Events.CONTENT_URI,
            event_id!!.toLong()
        )

        cr.delete(eventUri, null, null)

        map.remove(eventID)
        Paper.book().write(Constants.CALENDARHASHMAP,map)


    }


}
